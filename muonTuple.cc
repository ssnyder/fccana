#include "FCCAnalyses/ReconstructedParticle.h"
#include "FCCAnalyses/MCParticle.h"
#include "edm4hep/EventHeader.h"

struct sel_absType {
  int m_id;
  sel_absType (int id) : m_id(id) {}
  ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData> operator() (ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData> in)
  {
    ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData> result;
    result.reserve(in.size());
    for (size_t i = 0; i < in.size(); ++i) {
      auto & p = in[i];
      if (std::abs(p.type) == m_id) {
        result.emplace_back(p);
      }
    }
    return result;
  }
};



struct MatchRel
{
  MatchRel (int mc, int rp) : mcndx(mc), rpndx(rp) {}
  int mcndx;
  int rpndx;
};


int matchOne (const edm4hep::MCParticleData& mc,
              const ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData>& rps)
{
  int matched = -1;
  float mindr = 0.1;
  TLorentzVector mc_tlv;
  mc_tlv.SetXYZM (mc.momentum.x, mc.momentum.y, mc.momentum.z, mc.mass);
  for (size_t i  = 0; i < rps.size(); i++) {
    const auto& rp = rps[i];
    TLorentzVector rp_tlv;
    rp_tlv.SetXYZM (rp.momentum.x, rp.momentum.y, rp.momentum.z, rp.mass);
    float dr = mc_tlv.DeltaR (rp_tlv);
    if (dr < mindr) {
      mindr = dr;
      matched = i;
    }
  }
  return matched;
}


std::vector<MatchRel>
match (const ROOT::VecOps::RVec<edm4hep::MCParticleData>& mcs,
       const ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData>& rps)
{
  std::vector<MatchRel> ret;
  ret.reserve (mcs.size());
  for (size_t i = 0; i < mcs.size(); i++) {
    const auto& mc = mcs[i];
    int matched = matchOne (mc, rps);
    if (matched >= 0)
      ret.emplace_back (i, matched);
  }
  return ret;
}


ROOT::VecOps::RVec<edm4hep::MCParticleData> 
sel_matched (const ROOT::VecOps::RVec<edm4hep::MCParticleData>& mcs,
             const std::vector<MatchRel>& matchrel)
{
  ROOT::VecOps::RVec<edm4hep::MCParticleData> out;
  for (const MatchRel& r : matchrel) {
    out.push_back (mcs[r.mcndx]);
  }
  return out;
}


ROOT::VecOps::RVec<edm4hep::MCParticleData> 
sel_unmatched (const ROOT::VecOps::RVec<edm4hep::MCParticleData>& mcs,
             const std::vector<MatchRel>& matchrel)
{
  ROOT::VecOps::RVec<edm4hep::MCParticleData> out;
  for (size_t i = 0; i < mcs.size(); i++) {
    bool found = false;
    for (const MatchRel& r : matchrel) {
      if (i == r.mcndx) {
        found = true;
        break;
      }
    }
    if (!found) {
      out.push_back (mcs[i]);
    }
  }
  return out;
}

struct calc_trues
{
  float operator() (ROOT::VecOps::RVec<edm4hep::MCParticleData> mcp)
  {
    if (mcp.size() < 2) return 0;
    return std::abs(mcp[0].momentum.z) + std::abs(mcp[1].momentum.z);
  }
};

struct calc_trues_recoil
{
  ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData> 
  operator() (float trues, ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData> in)
  {
    return FCCAnalyses::ReconstructedParticle::recoilBuilder(trues)(in);
  }
};


struct event_number
{
  int operator() (const ROOT::VecOps::RVec<edm4hep::EventHeaderData>& eh) const
  {
    return eh[0].eventNumber;
  }
};


struct run_number
{
  int operator() (const ROOT::VecOps::RVec<edm4hep::EventHeaderData>& eh) const
  {
    return eh[0].runNumber;
  }
};
