#include "FCCAnalyses/ReconstructedParticle.h"
#include "FCCAnalyses/MCParticle.h"
#include "FCCAnalyses/defines.h"
#include <cmath>
#include <atomic>
#include <mutex>
#include <algorithm>


enum PDG {
  PDG_ELE = 11,
  PDG_NUE = 12,
  PDG_MUO = 13,
  PDG_NUM = 14,
  PDG_TAU = 15,
  PDG_NUT = 16,

  PDG_B = 5,
  PDG_Z = 23,
  PDG_W = 24,
  PDG_H = 25,
  
};


enum Z_DecayType
{
  Z_NONE,
  Z_EE, // 1
  Z_MM, // 2
  Z_TT, // 3
  Z_NN, // 4
  Z_QQ  // 5
};

enum W_DecayType
{
  W_NONE,
  W_EN, // 1
  W_MN, // 2
  W_TN, // 3
  W_QQ  // 4
};


std::vector<int>
get_list_of_particles_from_decay (int i, const Vec_mc& mcp,
                                  const ROOT::VecOps::RVec<podio::ObjectID>& dindex)
{
  std::vector<int> res;
  if (i < 0 || i >= mcp.size()) return res;
  for (unsigned da = mcp[i].daughters_begin; da <= mcp[i].daughters_end; ++da) {
    res.push_back (dindex[da].index);
  }
  return res;
}


Z_DecayType classify_z (int pdg)
{
  int abspdg = std::abs (pdg);
  if (abspdg == PDG_ELE) {
    return Z_EE;
  }
  else if (abspdg == PDG_MUO) {
    return Z_MM;
  }
  else if (abspdg == PDG_TAU) {
    return Z_TT;
  }
  else if (abspdg == PDG_NUE || abspdg == PDG_NUM || abspdg == PDG_NUT) {
    return Z_NN;
  }
  else if (abspdg <= PDG_B) {
    return Z_QQ;
  }
  return Z_NONE;
}


std::pair<int, Z_DecayType> find_z (int beg, const Vec_mc& mc,
                                    const ROOT::VecOps::RVec<podio::ObjectID>& dindex)
{
  if (beg < 0)
    return std::make_pair (-1, Z_NONE);
  size_t imax = std::min (static_cast<size_t>(50), mc.size());
  for (size_t i = beg; i < imax; i++) {
    if (mc[i].PDG == PDG_Z) {
      std::vector<int> daughters =
        get_list_of_particles_from_decay (i, mc, dindex);
      int pdg1 = 0;
      for (int d : daughters) {
        int pdg = mc.at(d).PDG;
        if (pdg1 && pdg + pdg1 == 0) {
          return std::make_pair (i, classify_z (pdg1));
        }
        else if (pdg1 == 0) {
          if (classify_z (pdg) != Z_NONE) {
            pdg1 = pdg;
          }
        }
      }
    }
  }
  return std::make_pair (-1, Z_NONE);
}

W_DecayType classify_w (int pdg)
{
  int abspdg = std::abs (pdg);
  if (abspdg == PDG_ELE || abspdg == PDG_NUE) {
    return W_EN;
  }
  else if (abspdg == PDG_MUO || abspdg == PDG_NUM) {
    return W_MN;
  }
  else if (abspdg == PDG_TAU || abspdg == PDG_NUT) {
   return W_TN;
  }
  else if (abspdg <= PDG_B) {
    return W_QQ;
  }
  return W_NONE;
}


std::pair<int, W_DecayType> find_w (int beg, const Vec_mc& mc,
                                    const ROOT::VecOps::RVec<podio::ObjectID>& dindex)
{
  if (beg < 0)
    return std::make_pair (-1, W_NONE);
  size_t imax = std::min (static_cast<size_t>(50), mc.size());
  for (size_t i = beg; i < imax; i++) {
    if (std::abs (mc[i].PDG) == PDG_W) {
      std::vector<int> daughters =
        get_list_of_particles_from_decay (i, mc, dindex);
      int pdg1 = 0;
      for (int d : daughters) {
        int pdg = mc.at(d).PDG;
        if (pdg1) {
          W_DecayType typ = classify_w (pdg1);
          if (typ == classify_w (pdg) && pdg*pdg1 < 0 &&
              (pdg1 > PDG_B || (pdg1+pdg)%2 == 1))
          {
            return std::make_pair (i, typ);
          }
        }
        else if (pdg1 == 0) {
          if (classify_w (pdg) != W_NONE) {
            pdg1 = pdg;
          }
        }
      }
    }
  }
  return std::make_pair (-1, W_NONE);
}


enum H_DecayType
{
  H_OTHER,
  H_EENN,
  H_MMNN,
  H_QQNN,
  H_NNNN,
  H_QQ,
};


H_DecayType classify_h (int ih, const Vec_mc& mc,
                        const ROOT::VecOps::RVec<podio::ObjectID>& dindex)
{
  if (ih < 0) return H_OTHER;
  std::vector<int> daughters =
    get_list_of_particles_from_decay (ih, mc, dindex);
  if (daughters.size() < 2) return H_OTHER;
  int d1 = daughters[0];
  int d2 = daughters[1];
  if (mc.at(d1).PDG <= PDG_B) return H_QQ;
  if (mc.at(d1).PDG != PDG_Z) return H_OTHER;
  if (mc.at(d2).PDG != PDG_Z) return H_OTHER;
  auto [iz1, ztyp1] = find_z (d1, mc, dindex);
  if (iz1 != d1) return H_OTHER;
  auto [iz2, ztyp2] = find_z (d2, mc, dindex);
  if (iz2 != d2) return H_OTHER;

  if (ztyp1 > ztyp2) {
    std::swap (ztyp1, ztyp2);
  }
  if (ztyp1 == Z_EE && ztyp2 == Z_NN) return H_EENN;
  if (ztyp1 == Z_MM && ztyp2 == Z_NN) return H_MMNN;
  if (ztyp1 == Z_NN && ztyp2 == Z_QQ) return H_QQNN;
  if (ztyp1 == Z_NN && ztyp2 == Z_NN) return H_NNNN;
  return H_OTHER;
}


std::tuple<int, Z_DecayType, H_DecayType>
find_zh (const Vec_mc& mc,
         const ROOT::VecOps::RVec<podio::ObjectID>& dindex)
{
  if (mc.size() < 10 ||
      std::abs (mc.at(4).PDG) != PDG_ELE ||
      std::abs (mc.at(5).PDG) != PDG_ELE)
  {
    return std::make_tuple (-1, Z_NONE, H_OTHER);
  }

  std::vector<int> daughters =
    get_list_of_particles_from_decay (4, mc, dindex);
#if 0
  if (daughters !=
      get_list_of_particles_from_decay (5, mc, dindex))
  {
    return std::make_tuple (-1, Z_NONE, H_OTHER);
  }
#endif

  int pdg1 = 0;
  int ih = -1;
  Z_DecayType typ = Z_NONE;
  for (int d : daughters) {
    int pdg = mc.at(d).PDG;
    if (pdg == PDG_H)
      ih = d;
    else if (pdg1 && pdg + pdg1 == 0)
      typ = classify_z (pdg1);
    else if (pdg1 == 0 && classify_z (pdg) != Z_NONE)
      pdg1 = pdg;
  }

  H_DecayType htyp = classify_h (ih, mc, dindex);
  return std::make_tuple (ih, typ, htyp);
}


enum Channel
{
  CHAN_OTHER,
  CHAN_ZZ_EENN, // 1
  CHAN_ZZ_MMNN, // 2
  CHAN_ZZ_QQNN, // 3
  CHAN_ZZ_EEQQ, // 4
  CHAN_ZZ_MMQQ, // 5
  CHAN_ZZ_QQQQ, // 6

  CHAN_ZH_EENNNN, // 7
  CHAN_ZH_MMNNNN, // 8
  CHAN_ZH_QQNNNN, // 9
  CHAN_ZH_NNEENN, // 10
  CHAN_ZH_NNMMNN, // 11
  CHAN_ZH_NNQQNN, // 12
  CHAN_ZH_NNQQ, // 13

  CHAN_WW_ENEN, // 14
  CHAN_WW_MNMN, // 15
  CHAN_WW_TNTN, // 16
  CHAN_WW_ENMN, // 17
  CHAN_WW_ENTN, // 18
  CHAN_WW_MNTN, // 19
  CHAN_WW_ENQQ, // 20
  CHAN_WW_MNQQ, // 21
  CHAN_WW_TNQQ, // 22
  
};




Channel classify_event (const Vec_mc& mc,
                        const ROOT::VecOps::RVec<podio::ObjectID>& dindex)
{
  auto [ih, ztyp0, htyp] = find_zh (mc, dindex);
  if (ih >= 0) {
    if (ztyp0 == Z_EE && htyp == H_NNNN) return CHAN_ZH_EENNNN;
    if (ztyp0 == Z_MM && htyp == H_NNNN) return CHAN_ZH_MMNNNN;
    if (ztyp0 == Z_QQ && htyp == H_NNNN) return CHAN_ZH_QQNNNN;
    if (ztyp0 == Z_NN) {
      if (htyp == H_EENN) return CHAN_ZH_NNEENN;
      if (htyp == H_MMNN) return CHAN_ZH_NNMMNN;
      if (htyp == H_QQNN) return CHAN_ZH_NNQQNN;
      if (htyp == H_QQ) return CHAN_ZH_NNQQ;
    }
    return CHAN_OTHER;
  }

  auto [iz1, ztyp1] = find_z (0, mc, dindex);
  if (iz1 != -1) {
    auto [iz2, ztyp2] = find_z (iz1+1, mc, dindex);
    if (iz2 != -1) {
      if (ztyp1 > ztyp2) {
        std::swap (ztyp1, ztyp2);
      }
      if (ztyp1 == Z_EE) {
        if (ztyp2 == Z_NN) return CHAN_ZZ_EENN;
        if (ztyp2 == Z_QQ) return CHAN_ZZ_EEQQ;
      }
      else if (ztyp1 == Z_MM) {
        if (ztyp2 == Z_NN) return CHAN_ZZ_MMNN;
        if (ztyp2 == Z_QQ) return CHAN_ZZ_MMQQ;
      }
      else if (ztyp1 == Z_NN) {
        if (ztyp2 == Z_QQ) return CHAN_ZZ_QQNN;
      }
      else if (ztyp1 == Z_QQ) {
        if (ztyp2 == Z_QQ) return CHAN_ZZ_QQQQ;
      }
    }
  }

  auto [iw1, wtyp1] = find_w (0, mc, dindex);
  if (iw1 != -1) {
    auto [iw2, wtyp2] = find_w (iw1+1, mc, dindex);
    if (iw2 != -1) {
      if (wtyp1 > wtyp2) {
        std::swap (wtyp1, wtyp2);
      }
      if (wtyp1 == W_EN) {
        if (wtyp2 == W_EN) return CHAN_WW_ENEN;
        if (wtyp2 == W_MN) return CHAN_WW_ENMN;
	if (wtyp2 == W_TN) return CHAN_WW_ENTN;
	if (wtyp2 == W_QQ) return CHAN_WW_ENQQ;
      }
      else if (wtyp1 == W_MN) {
        if (wtyp2 == W_EN) return CHAN_WW_ENMN;
        if (wtyp2 == W_MN) return CHAN_WW_MNMN;
	if (wtyp2 == W_TN) return CHAN_WW_MNTN;
	if (wtyp2 == W_QQ) return CHAN_WW_MNQQ;
      }
       else if (wtyp1 == W_TN) {
        if (wtyp2 == W_EN) return CHAN_WW_ENTN;
        if (wtyp2 == W_MN) return CHAN_WW_MNTN;
	if (wtyp2 == W_TN) return CHAN_WW_TNTN;
	if (wtyp2 == W_QQ) return CHAN_WW_TNQQ;
      }
      else if (wtyp1 == W_QQ) {
        if (wtyp2 == W_EN) return CHAN_WW_ENQQ;
	if (wtyp2 == W_MN) return CHAN_WW_MNQQ;
	if (wtyp2 == W_TN) return CHAN_WW_TNQQ;
      }
    }
  }
  return CHAN_OTHER;
}


int dump (const Vec_mc& mc,
          const ROOT::VecOps::RVec<podio::ObjectID>& dindex)
{
  static std::mutex m;
  static std::atomic<int> n = 0;
  std::lock_guard g (m);
  int this_n = n++;
  if (this_n >= 20) return 0;
  printf ("dump --- %d\n", this_n);
  int i = 0;
  for (const auto& p : mc) {
    std::vector<int> d = get_list_of_particles_from_decay (i, mc, dindex);
    printf ("%3d %2d %4d ", i, p.generatorStatus, p.PDG);
    auto v = tlv (p);
    printf ("%5.1f %5.2f %5.2f %5.1f %5.1f %5.1f %5.1f %5.1f ->",
            v.Pt(), v.Eta(), v.Phi(), v.M(), v.Px(), v.Py(), v.Pz(), v.E());
    for (int dd : d) {
      printf (" %3d", dd);
    }
    printf ("\n");
    ++i;
  }
  auto [iz1, ztyp1] = find_z (0, mc, dindex);
  auto [iz2, ztyp2] = find_z (iz1+1, mc, dindex);
  auto [ih, ztyp3, htyp] = find_zh (mc, dindex);
  auto [iw1, wtyp1] = find_w (0, mc, dindex);
  auto [iw2, wtyp2] = find_w (iw1+1, mc, dindex);
  
  Channel chan = classify_event (mc, dindex);
  printf (" .. %d %d %d %d %d %d %d %d\n", chan, iw1, wtyp1, iw2, wtyp2, ih, ztyp3, htyp);
  return 0;
}

