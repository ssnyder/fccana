import ROOT
import sys

def dump_head (t, l, titles):
    if titles:
        l += ['run_number', 'event_number']
        return True
    l += [t.run_number, t.event_number]
    return True


def dump_mcmuo (t, l, titles):
    if titles:
        l += ['mcmu1_pt', 'mcmu1_theta', 'mcmu1_phi',
              'mcmu2_pt', 'mcmu2_theta', 'mcmu2_phi']
        return True
    if len(t.mcmuo_pt) < 2: return False
    l += [t.mcmuo_pt[0], t.mcmuo_theta[0], t.mcmuo_phi[0],
          t.mcmuo_pt[1], t.mcmuo_theta[1], t.mcmuo_phi[1]]
    return True


def dump_muo (t, l, titles):
    if titles:
        l += ['mu1_pt', 'mu1_theta', 'mu1_phi',
              'mu2_pt', 'mu2_theta', 'mu2_phi']
        return True
    if len(t.muo_pt) < 2: return False
    l += [t.muo_pt[0], t.muo_theta[0], t.muo_phi[0],
          t.muo_pt[1], t.muo_theta[1], t.muo_phi[1]]
    return True


def dump_h (t, l, titles):
    if titles:
        l += ['h_pt', 'h_m']
        return True
    l += [t.h_pt[0], t.h_m[0]]
    return True


def dump_trues (t, l, titles):
    if titles:
        l += ['truesqrts', 'x1', 'x2']
        return True
    l += [t.truesqrts, t.x1, t.x2]
    return True


def dump_htrues (t, l, titles):
    if titles:
        l += ['htrues_pt', 'htrues_m', 'htrues_theta']
        return True
    l += [t.htrues_pt[0], t.htrues_m[0], t.htrues_theta[0]]
    return True


def dump_mch (t, l, titles):
    if titles:
        l += ['mch_pt', 'mch_m', 'mch_theta']
        return True
    l += [t.mch_pt[0], t.mch_m[0], t.mch_theta[0]]
    return True


def dump (t, f, titles):
    l = []
    if not dump_head (t, l, titles): return False
    if not dump_mcmuo (t, l, titles): return False
    if not dump_muo (t, l, titles): return False
    if not dump_h (t, l, titles): return False
    if not dump_trues (t, l, titles): return False
    if not dump_htrues (t, l, titles): return False
    if not dump_mch (t, l, titles): return False
    l = [str(x) for x in l]
    print (','.join (l), file=f)
    return


f=ROOT.TFile ('muonTuple-wide.root')
t = f.events

outf = sys.stdout

dump (t, outf, True)
for i in range (t.GetEntries()):
    t.GetEntry(i)
    dump (t, outf, False)

