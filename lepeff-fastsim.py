#fccanalysis run ../lepeff-fastsim.py --output lepeff-fastsim-ele.root --files-list  /eos/experiment/fcc/ee/generation/DelphesEvents/winter2023/IDEA/wzp6_ee_eeH_HZZ_ecm240/*.root  2>&1|tee log-lepeff-fastsim-ele

#fccanalysis run ../lepeff-fastsim.py --output lepeff-fastsim-muo.root --files-list  /eos/experiment/fcc/ee/generation/DelphesEvents/winter2023/IDEA/wzp6_ee_mumuH_HZZ_ecm240/*.root  2>&1|tee log-lepeff-fastsim-muo

#fccanalysis run ../lepeff-fastsim.py --output lepeff-idea-ele.root --files-list  ../../data/delphes/hinvee-idea/hinvee-idea.root  2>&1|tee log-lepeff-idea-ele
#fccanalysis run ../lepeff-fastsim.py --output lepeff-cld-ele.root --files-list  ../../data/delphes/hinvee-cld/hinvee-cld.root  2>&1|tee log-lepeff-cld-ele
#fccanalysis run ../lepeff-fastsim.py --output lepeff-idea-muo.root --files-list  ../../data/delphes/hinvmm-idea/hinvmm-idea.root  2>&1|tee log-lepeff-idea-muo
#fccanalysis run ../lepeff-fastsim.py --output lepeff-cld-muo.root --files-list  ../../data/delphes/hinvmm-cld/hinvmm-cld.root  2>&1|tee log-lepeff-cld-muo

#fccanalysis run ../lepeff-fastsim.py --output lepeff-test-ele.root --files-list  test-ele.root  2>&1|tee log-lepeff-test-ele
#fccanalysis run ../lepeff-fastsim.py --output lepeff-test-muo.root --files-list  test-muo.root  2>&1|tee log-lepeff-test-muo
#fccanalysis run ../lepeff-fastsim.py --output lepeff-test-ele2.root --files-list  test-ele2.root  2>&1|tee log-lepeff-test-ele

#fccanalysis run ../lepeff-fastsim.py --output lepeff-cld-jj.root --files-list  ../../data/delphes/hinvjj-cld/hinvjj-cld.root  2>&1|tee log-lepeff-cld-jj




import sys
import os
import ROOT

mydir = os.path.dirname (sys.argv[2])
code = open (os.path.join (mydir, 'lepeff.cc')).read()
ROOT.gInterpreter.Declare(code)
code2 = open (os.path.join (mydir, 'bremrec.cc')).read()
ROOT.gInterpreter.Declare(code2)


class RDFanalysis:
    def analysers (df):
        df2 = (
            df
            .Define("mcp1", "MCParticle::sel_genStatus(1)(Particle)")
            .Define("mcp2", "MCParticle::sel_pt(5)(mcp1)")

            #.Alias("Photon0", "Photon#0.index")  #For old file versions
            .Alias("Photon0", "Photon_objIdx.index")
            .Define('gam', 'ReconstructedParticle::get(Photon0, ReconstructedParticles)')
            .Define('gam_pt', 'ReconstructedParticle::get_pt(gam)')
            .Define('gam_p', 'ReconstructedParticle::get_p(gam)')
            .Define('gam_eta', 'ReconstructedParticle::get_eta(gam)')
            .Define('gam_theta', 'ReconstructedParticle::get_theta(gam)')
            .Define('gam_phi', 'ReconstructedParticle::get_phi(gam)')
            .Define("gam_totE", "get_totE(gam)")

             #.Alias("Electron0", "Electron#0.index")
            .Alias("Electron0", "Electron_objIdx.index")
            .Define("all_eles",  "ReconstructedParticle::get(Electron0, ReconstructedParticles)")

            .Define("ele", "ReconstructedParticle::sel_pt(5)(all_eles)")
            .Define("ele_pt", "ReconstructedParticle::get_pt(ele)")
            .Define("ele_p", "ReconstructedParticle::get_p(ele)")
            .Define("ele_eta", "ReconstructedParticle::get_eta(ele)")
            .Define("ele_theta", "ReconstructedParticle::get_theta(ele)")
            .Define("ele_phi", "ReconstructedParticle::get_phi(ele)")

            .Define('gamele_match', 'photonLeptonMatch(gam, ele)')
            .Define('gamele_angle', 'get_match_angle(gamele_match)')
            .Define('gamele_lepndx', 'get_match_lepndx(gamele_match)')
            .Define('gamele_gamndx', 'get_match_gamndx(gamele_match)')
            .Define('ele_gamE', 'gamE(ele,gam,gamele_match,0.05)')

            .Define("mcele", "MCParticle::sel_pdgID(11,true)(mcp2)")
            .Define("mcele_pt", "MCParticle::get_pt(mcele)")
            .Define("mcele_p", "MCParticle::get_p(mcele)")
            .Define("mcele_eta", "MCParticle::get_eta(mcele)")
            .Define("mcele_theta", "MCParticle::get_theta(mcele)")
            .Define("mcele_phi", "MCParticle::get_phi(mcele)")
            .Define("mcele_matchrel", "match(mcele, ele)")
            .Define("mcele_matched", "sel_matched(mcele, mcele_matchrel)")
            .Define("mcele_matched_pt", "MCParticle::get_pt(mcele_matched)")
            .Define("mcele_matched_p", "MCParticle::get_p(mcele_matched)")
            .Define("mcele_matched_eta", "MCParticle::get_eta(mcele_matched)")
            .Define("mcele_matched_theta", "MCParticle::get_theta(mcele_matched)")
            .Define("mcele_matched_phi", "MCParticle::get_phi(mcele_matched)")
            .Define("mcele_matched_diff", "calc_diff(mcele_matchrel, mcele, ele)")
            .Define("mcele_matched_res", "calc_res(mcele_matchrel, mcele, ele)")
            .Define("mcele_matched_ires", "calc_ires(mcele_matchrel, mcele, ele)")
            .Define("mcele_unmatched", "sel_unmatched(mcele, mcele_matchrel)")
            .Define("mcele_unmatched_pt", "MCParticle::get_pt(mcele_unmatched)")
            .Define("mcele_unmatched_p", "MCParticle::get_p(mcele_unmatched)")
            .Define("mcele_unmatched_eta", "MCParticle::get_eta(mcele_unmatched)")
            .Define("mcele_unmatched_theta", "MCParticle::get_theta(mcele_unmatched)")
            .Define("mcele_unmatched_phi", "MCParticle::get_phi(mcele_unmatched)")

            .Define('ee', 'resonanceBuilder(91)(ele)')
            .Define('mcee', 'resonanceBuilder(91)(mcele)')
            .Define('ee_m', 'ReconstructedParticle::get_mass(ee)')
            .Define('mcee_m', 'MCParticle::get_mass(mcee)')


            #Alias("Muon0", "Muon#0.index")
            .Alias("Muon0", "Muon_objIdx.index")
            .Define("all_muos",  "ReconstructedParticle::get(Muon0, ReconstructedParticles)")

            .Define("muo", "ReconstructedParticle::sel_pt(5)(all_muos)")
            .Define("muo_pt", "ReconstructedParticle::get_pt(muo)")
            .Define("muo_p", "ReconstructedParticle::get_p(muo)")
            .Define("muo_eta", "ReconstructedParticle::get_eta(muo)")
            .Define("muo_theta", "ReconstructedParticle::get_theta(muo)")
            .Define("muo_phi", "ReconstructedParticle::get_phi(muo)")

            .Define('gammuo_match', 'photonLeptonMatch(gam, muo)')
            .Define('gammuo_angle', 'get_match_angle(gammuo_match)')
            .Define('gammuo_lepndx', 'get_match_lepndx(gammuo_match)')
            .Define('gammuo_gamndx', 'get_match_gamndx(gammuo_match)')
            .Define('muo_gamE', 'gamE(muo,gam,gammuo_match,0.05)')

            .Define("mcmuo", "MCParticle::sel_pdgID(13,true)(mcp2)")
            .Define("mcmuo_pt", "MCParticle::get_pt(mcmuo)")
            .Define("mcmuo_p", "MCParticle::get_p(mcmuo)")
            .Define("mcmuo_eta", "MCParticle::get_eta(mcmuo)")
            .Define("mcmuo_theta", "MCParticle::get_theta(mcmuo)")
            .Define("mcmuo_phi", "MCParticle::get_phi(mcmuo)")
            .Define("mcmuo_matchrel", "match(mcmuo, muo)")
            .Define("mcmuo_matched", "sel_matched(mcmuo, mcmuo_matchrel)")
            .Define("mcmuo_matched_pt", "MCParticle::get_pt(mcmuo_matched)")
            .Define("mcmuo_matched_p", "MCParticle::get_p(mcmuo_matched)")
            .Define("mcmuo_matched_eta", "MCParticle::get_eta(mcmuo_matched)")
            .Define("mcmuo_matched_theta", "MCParticle::get_theta(mcmuo_matched)")
            .Define("mcmuo_matched_phi", "MCParticle::get_phi(mcmuo_matched)")
            .Define("mcmuo_matched_diff", "calc_diff(mcmuo_matchrel, mcmuo, muo)")
            .Define("mcmuo_matched_res", "calc_res(mcmuo_matchrel, mcmuo, muo)")
            .Define("mcmuo_matched_ires", "calc_ires(mcmuo_matchrel, mcmuo, muo)")
            .Define("mcmuo_unmatched", "sel_unmatched(mcmuo, mcmuo_matchrel)")
            .Define("mcmuo_unmatched_pt", "MCParticle::get_pt(mcmuo_unmatched)")
            .Define("mcmuo_unmatched_p", "MCParticle::get_p(mcmuo_unmatched)")
            .Define("mcmuo_unmatched_eta", "MCParticle::get_eta(mcmuo_unmatched)")
            .Define("mcmuo_unmatched_theta", "MCParticle::get_theta(mcmuo_unmatched)")
            .Define("mcmuo_unmatched_phi", "MCParticle::get_phi(mcmuo_unmatched)")

            .Define('mm', 'resonanceBuilder(91)(muo)')
            .Define('mcmm', 'resonanceBuilder(91)(mcmuo)')
            .Define('mm_m', 'ReconstructedParticle::get_mass(mm)')
            .Define('mcmm_m', 'MCParticle::get_mass(mcmm)')

            .Define('vis', 'sum(ReconstructedParticles)')
            )
        return df2


    def output():
        return [
            'gam',
            'gam_pt',
            'gam_p',
            'gam_eta',
            'gam_theta',
            'gam_phi',
            'gam_totE',

            'ele',
            'ele_pt',
            'ele_p',
            'ele_eta',
            'ele_theta',
            'ele_phi',
            'ele_gamE',

            'gamele_angle',
            'gamele_lepndx',
            'gamele_gamndx',

            'mcele',
            'mcele_pt',
            'mcele_p',
            'mcele_eta',
            'mcele_theta',
            'mcele_phi',
            'mcele_matched',
            'mcele_matched_pt',
            'mcele_matched_p',
            'mcele_matched_eta',
            'mcele_matched_theta',
            'mcele_matched_phi',
            'mcele_matched_diff',
            'mcele_matched_res',
            'mcele_matched_ires',
            'mcele_unmatched',
            'mcele_unmatched_pt',
            'mcele_unmatched_p',
            'mcele_unmatched_eta',
            'mcele_unmatched_theta',
            'mcele_unmatched_phi',

            'ee_m',
            'mcee_m',

            'muo',
            'muo_pt',
            'muo_p',
            'muo_eta',
            'muo_theta',
            'muo_phi',
            'muo_gamE',

            'gammuo_angle',
            'gammuo_lepndx',
            'gammuo_gamndx',

            'mcmuo',
            'mcmuo_pt',
            'mcmuo_p',
            'mcmuo_eta',
            'mcmuo_theta',
            'mcmuo_phi',
            'mcmuo_matched',
            'mcmuo_matched_pt',
            'mcmuo_matched_p',
            'mcmuo_matched_eta',
            'mcmuo_matched_theta',
            'mcmuo_matched_phi',
            'mcmuo_matched_diff',
            'mcmuo_matched_res',
            'mcmuo_matched_ires',
            'mcmuo_unmatched',
            'mcmuo_unmatched_pt',
            'mcmuo_unmatched_p',
            'mcmuo_unmatched_eta',
            'mcmuo_unmatched_theta',
            'mcmuo_unmatched_phi',

            'mm_m',
            'mcmm_m',

            'vis',
            ]


