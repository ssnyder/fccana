import os
import sys
import ROOT
from printpdf import plotfilename

mydir = os.path.dirname (sys.argv[0])


ROOT.gInterpreter.Declare("""
struct flat_t {
  int eventNumber;
  float angle;
  float dist;
  float p;
  float pt;
  float theta;
  float clustE;
  float gam_p;
  float gam_pt;
  float gam_theta;
  float gam_clustE;
  float corr_p;
  float corr_pt;
  float corr_theta;
  float mc_p;
  float mc_pt;
  float mc_theta;
  float best_p;
  int should_add;
};
""")


objs = []


def flatten_tree (inf):
    tout = ROOT.TTree ('flattened', 'flattened')
    flat = ROOT.flat_t()
    tout.Branch ('flat', flat, 'eventNumber/I:angle/F:dist/F:p/F:pt/F:theta/F:clustE/F:gam_p/F:gam_pt/F:gam_theta/F:gam_clustE/F:corr_p/F:corr_pt/F:corr_theta/F:mc_p/F:mc_pt/F:mc_theta/F:best_p/F:should_add/I')
    t = inf.events
    for i in range(t.GetEntriesFast()):
        t.GetEntry(i)
        for j in range(len(t.mcele_matchrel_mcndx)):
            mcndx = t.mcele_matchrel_mcndx[j]
            lepndx = t.mcele_matchrel_rpndx[j]
            for k in range(len(t.gamele_lepndx)):
                if t.gamele_lepndx[k] == lepndx:
                    angle = t.gamele_angle[k]
                    if angle < 0.05:
                        gamndx = t.gamele_gamndx[k]
                        flat.eventNumber = t.eventNumber
                        flat.angle = angle
                        flat.dist = t.gamele_dist[k]
                        flat.p = t.ele0_p[lepndx]
                        flat.pt = t.ele0_pt[lepndx]
                        flat.theta = t.ele0_theta[lepndx]
                        flat.clustE = t.ele0_clustE[lepndx]
                        flat.corr_p = t.ele_p[lepndx]
                        flat.corr_pt = t.ele_pt[lepndx]
                        flat.corr_theta = t.ele_theta[lepndx]
                        flat.gam_p = t.gam_p[gamndx]
                        flat.gam_pt = t.gam_pt[gamndx]
                        flat.gam_theta = t.gam_theta[gamndx]
                        flat.gam_clustE = t.gam_clustE[gamndx]
                        flat.mc_p = t.mcele_p[lepndx]
                        flat.mc_pt = t.mcele_pt[lepndx]
                        flat.mc_theta = t.mcele_theta[lepndx]
                        if abs(flat.p-flat.mc_p) < abs(flat.corr_p-flat.mc_p):
                            flat.best_p = flat.p
                            flat.should_add = False
                        else:
                            flat.best_p = flat.corr_p
                            flat.should_add = True
                        tout.Fill()
                        break
    return tout


infile = 'lepeff-ele.root'
inf = ROOT.TFile (infile)
print ('flatten', flush=True)
fout = ROOT.TFile (os.path.splitext (os.path.basename (infile))[0] + '-flattened.root',
                   'RECREATE')
tflat = flatten_tree (inf)
tflat.Write()
print ('done', flush=True)

outputFileName = 'lepfsr-nn.root'
outputFile = ROOT.TFile (outputFileName, 'RECREATE')

# Trying to do it as a regression
# fac = ROOT.TMVA.Factory ('TMVARegression', outputFile, '!V:!Silent:Color:DrawProgressBar:AnalysisType=Regression')
# dataloader = ROOT.TMVA.DataLoader ('datasetreg')

# dataloader.AddVariable ('angle', 'ele/gam angle', '', 'F')
# dataloader.AddVariable ('dist', 'ele/gam dist', 'mm', 'F')
# dataloader.AddVariable ('p', 'ele p', 'GeV', 'F')
# dataloader.AddVariable ('pt', 'ele pt', 'GeV', 'F')
# dataloader.AddVariable ('theta', 'ele theta', '', 'F')
# dataloader.AddVariable ('clustE', 'ele clustE', 'GeV', 'F')
# dataloader.AddVariable ('gam_p', 'gam p', 'GeV', 'F')
# dataloader.AddVariable ('gam_pt', 'gam pt', 'GeV', 'F')
# dataloader.AddVariable ('gam_theta', 'gam theta', '', 'F')
# dataloader.AddVariable ('gam_clustE', 'gam clustE', 'GeV', 'F')
# dataloader.AddVariable ('corr_p', 'corr_p', 'GeV', 'F')

# dataloader.AddTarget ('mc_p')
# #dataloader.AddTarget ('corr_p')

# dataloader.AddRegressionTree (tflat)

# mycut = ROOT.TCut ('')

# # If no numbers of events are given, half of the events in the tree are used
# # for training, and the other half for testing:
# dataloader.PrepareTrainingAndTestTree (mycut,  'SplitMode=random:!V' )

# UseMLP = True

# if UseMLP:
#     #fac.BookMethod (dataloader, ROOT.TMVA.Types.kMLP, 'MLP',
#     #                '!H:!V:VarTransform=Norm:NeuronType=tanh:NCycles=2000:HiddenLayers=N+20:TestRate=6:TrainingMethod=BFGS:Sampling=0.3:SamplingEpoch=0.8:ConvergenceImprove=1e-6:ConvergenceTests=15:!UseRegulator')
#     #fac.BookMethod (dataloader, ROOT.TMVA.Types.kMLP, 'MLP',
#                     #'!H:!V:VarTransform=Gauss:NeuronType=tanh:NCycles=2000:HiddenLayers=N+20,N:TestRate=6:TrainingMethod=BFGS:ConvergenceImprove=1e-10:ConvergenceTests=15:!UseRegulator') #sig:0.819/1/13;gaus:0.53/0.59;437s
#                     #'!H:!V:VarTransform=Gauss:NeuronType=tanh:NCycles=2000:HiddenLayers=N+20,N:TestRate=6:TrainingMethod=BFGS:!UseRegulator')
#                     #'!H:!V:VarTransform=Gauss:NeuronType=sigmoid:NCycles=2000:HiddenLayers=N+20,N:TestRate=6:TrainingMethod=BFGS:!UseRegulator') #sig:0.31/0.212;gaus:0.18/0.14;917s
#                     #'!H:!V:VarTransform=Gauss:NeuronType=radial:NCycles=2000:HiddenLayers=N+20,N:TestRate=6:TrainingMethod=BFGS:!UseRegulator')
#                     #'!H:!V:VarTransform=Gauss:NeuronType=radial:NCycles=2000:HiddenLayers=N+3,N:TestRate=6:TrainingMethod=BFGS:!UseRegulator')
#     fac.BookMethod (dataloader, ROOT.TMVA.Types.kDL, 'DNN',
#                     #'!H:!V:Layout=TANH|10,TANH|10,LINEAR') #sig:0.0408/0.046;gaus:0.019/0.022;53s
#                     #'!H:!V:Layout=TANH|10,TANH|10,LINEAR:ErrorStrategy=SUMOFSQUARES') #sig:0.109/0.271;gaus:0.031/0.083;39s
#                     #'!H:!V:Layout=SIGMOID|10,SIGMOID|10,LINEAR') #sig:0.037/0.039;gaus:0.018/0.020;29s
#                     #'!H:!V:Layout=SIGMOID|20,SIGMOID|10,LINEAR')
#                     #'!H:!V:Layout=TANH|20,TANH|10,LINEAR')
#                     #'!H:!V:Layout=RELU|20,GAUSS|10,LINEAR:TrainingStrategy=Momentum=0.9')
#                     '!H:!V:Layout=RELU|20,GAUSS|10,LINEAR')

ROOT.TMVA.gConfig().GetVariablePlotting().fPlotFormat = ROOT.TMVA.gConfig().GetVariablePlotting().kPDF

fac = ROOT.TMVA.Factory ('TMVA', outputFile, '!V:!Silent:Color:DrawProgressBar')
dataloader = ROOT.TMVA.DataLoader ('dataset')

dataloader.AddVariable ('angle', 'ele/gam angle', '', 'F')
dataloader.AddVariable ('p', 'ele p', 'GeV', 'F')
dataloader.AddVariable ('theta', 'ele theta', '', 'F')
dataloader.AddVariable ('clustE', 'ele clustE', 'GeV', 'F')
dataloader.AddVariable ('gam_clustE', 'gam clustE', 'GeV', 'F')
#dataloader.AddVariable ('dist', 'ele/gam dist', 'mm', 'F')
#dataloader.AddVariable ('pt', 'ele pt', 'GeV', 'F')
#dataloader.AddVariable ('gam_p', 'gam p', 'GeV', 'F')
#dataloader.AddVariable ('gam_pt', 'gam pt', 'GeV', 'F')
#dataloader.AddVariable ('gam_theta', 'gam theta', '', 'F')
#dataloader.AddVariable ('corr_p', 'corr_p', 'GeV', 'F')
dataloader.AddSpectator ('best_p', 'F')
dataloader.AddSpectator ('mc_p', 'F')

should_add_cut = ROOT.TCut('should_add')
should_not_add_cut = ROOT.TCut('!should_add')
dataloader.SetInputTrees (tflat, should_add_cut, should_not_add_cut)

mycut = ROOT.TCut ('')
dataloader.PrepareTrainingAndTestTree (mycut,  'SplitMode=random:!V' )
fac.BookMethod (dataloader, ROOT.TMVA.Types.kDL, 'DNN2',
                H=False, V=False, Layout='TANH|14,TANH|6,LINEAR', VarTransform='G')
                


fac.TrainAllMethods()
fac.TestAllMethods()
fac.EvaluateAllMethods()
outputFile.Close()
#ROOT.TMVA.TMVARegGui (outputFileName) 
#ROOT.TMVA.TMVAGui (outputFileName) 

ff=ROOT.TFile('lepfsr-nn.root')
ttrain = ff.dataset.TrainTree
ttest = ff.dataset.TestTree


def rename (inroot, outroot):
    os.rename (f'dataset/plots/{inroot}.pdf', plotfilename(f'plots/{outroot}', 'pdf'))
    return
def makeplots():
    dsname = dataloader.GetName()
    ROOT.TMVA.variables(dsname, outputFileName,
                        'InputVariables_Id', 'Input variables (training)')
    rename ('variables_id_c1', 'nn-input')
    ROOT.TMVA.mvas(dsname, outputFileName, ROOT.TMVA.kCompareType)
    rename ('overtrain_DNN2', 'nn-output')
    ROOT.TMVA.mvaeffs(dsname, outputFileName)
    rename ('mvaeffs_DNN2', 'nn-effs')
    ROOT.TMVA.efficiencies(dsname, outputFileName)
    rename ('rejBvsS', 'nn-roc')
    ROOT.TMVA.training_history(dsname, outputFileName)
    rename ('TrainingHistory', 'nn-training')
    return


makeplots()


def execfile (f = os.path.join (mydir, 'lepfsr-nn.py')):
    exec (open(f).read(), globals())
    return

