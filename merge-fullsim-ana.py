# python ../merge-fullsim-ana.py wzp6_ee_tautau_ecm240.root  wzp6_ee_tautau_ecm240_??.root 2>&1 | tee wzp6_ee_tautau_ecm240.log

import ROOT
import sys
import os


class Counts:
    def __init__ (self, f):
        self.fromFile(f)
        return
    def fromFile (self, f):
        self.n_all = f.events.GetEntries()

        self.ee0 = f.events.GetEntries('n_eles==2&&n_muos==0&&sel_eles[0].type+sel_eles[1].type==0')
        self.ee0_met = f.events.GetEntries('n_eles==2&&n_muos==0&&sel_eles[0].type+sel_eles[1].type==0&&vis0_pt>10')
        self.ee0_mz = int (f.ee0_a_vis_p.GetEntries())
        self.ee0_mom = int (f.ee0_n_gams.GetEntries())

        self.ee = self.ee0
        self.ee_met = f.events.GetEntries('n_eles==2&&n_muos==0&&sel_eles[0].type+sel_eles[1].type==0&&vis_pt>10')
        self.ee_mz = int (f.ee_a_vis_p.GetEntries())
        self.ee_mom = int (f.ee_n_gams.GetEntries())

        self.mm0 = f.events.GetEntries('n_muos==2&&n_eles==0&&sel_muos[0].type+sel_muos[1].type==0')
        self.mm0_met = f.events.GetEntries('n_muos==2&&n_eles==0&&sel_muos[0].type+sel_muos[1].type==0&&vis0_pt>10')
        self.mm0_mz = int (f.mm0_a_vis_p.GetEntries())
        self.mm0_mom = int (f.mm0_n_gams.GetEntries())

        self.mm = self.mm0
        self.mm_met = f.events.GetEntries('n_muos==2&&n_eles==0&&sel_muos[0].type+sel_muos[1].type==0&&vis_pt>10')
        self.mm_mz = int (f.mm_a_vis_p.GetEntries())
        self.mm_mom = int (f.mm_n_gams.GetEntries())

        self.qq = f.events.GetEntries('n_eles+n_muos==0')
        self.qq_met = f.events.GetEntries('n_eles+n_muos==0&&vis_pt>15')
        self.qq_mz = int (f.qq_a_vis_p.GetEntries())
        self.qq_mom = int (f.qq_mjj.GetEntries())
        return

    def merge (self, other):
        self.n_all += other.n_all

        self.ee0 += other.ee0
        self.ee0_met += other.ee0_met
        self.ee0_mz  += other.ee0_mz
        self.ee0_mom  += other.ee0_mom
        self.ee += other.ee
        self.ee_met += other.ee_met
        self.ee_mz  += other.ee_mz
        self.ee_mom  += other.ee_mom

        self.mm0 += other.mm0
        self.mm0_met += other.mm0_met
        self.mm0_mz  += other.mm0_mz
        self.mm0_mom  += other.mm0_mom
        self.mm += other.mm
        self.mm_met += other.mm_met
        self.mm_mz  += other.mm_mz
        self.mm_mom  += other.mm_mom

        self.qq += other.qq
        self.qq_met += other.qq_met
        self.qq_mz  += other.qq_mz
        self.qq_mom  += other.qq_mom
        return


    def dump1 (self, nm, count, prev, all):
        eff = count/prev*100 if prev!=0 else 0
        cum = count/all*100 if all!=0 else 0
        print (f'{nm:10}: pass={count:<10} all={prev:<10} -- eff={eff:.2f} % cumulative eff={cum:.2f} %')
        return

    def dump (self):
        self.dump1 ('ee0', self.ee0, self.n_all, self.n_all)
        self.dump1 ('ee0_met', self.ee0_met, self.ee0, self.n_all)
        self.dump1 ('ee0_mz', self.ee0_mz, self.ee0_met, self.n_all)
        self.dump1 ('ee0_mom', self.ee0_mom, self.ee0_mz, self.n_all)
        self.dump1 ('ee', self.ee, self.n_all, self.n_all)
        self.dump1 ('ee_met', self.ee_met, self.ee, self.n_all)
        self.dump1 ('ee_mz', self.ee_mz, self.ee_met, self.n_all)
        self.dump1 ('ee_mom', self.ee_mom, self.ee_mz, self.n_all)

        self.dump1 ('mm0', self.mm0, self.n_all, self.n_all)
        self.dump1 ('mm0_met', self.mm0_met, self.mm0, self.n_all)
        self.dump1 ('mm0_mz', self.mm0_mz, self.mm0_met, self.n_all)
        self.dump1 ('mm0_mom', self.mm0_mom, self.mm0_mz, self.n_all)
        self.dump1 ('mm', self.mm, self.n_all, self.n_all)
        self.dump1 ('mm_met', self.mm_met, self.mm, self.n_all)
        self.dump1 ('mm_mz', self.mm_mz, self.mm_met, self.n_all)
        self.dump1 ('mm_mom', self.mm_mom, self.mm_mz, self.n_all)

        self.dump1 ('qq', self.qq, self.n_all, self.n_all)
        self.dump1 ('qq_met', self.qq_met, self.qq, self.n_all)
        self.dump1 ('qq_mz', self.qq_mz, self.qq_met, self.n_all)
        self.dump1 ('qq_mom', self.qq_mom, self.qq_mz, self.n_all)

        return


def copyout (f, fout):
    hists = {}
    fout.cd()
    for k in f.GetListOfKeys():
        nm = k.GetName()
        obj = getattr (f, nm)
        if isinstance (obj, ROOT.TH1):
            hists[nm] = obj.Clone()
        elif isinstance (obj, ROOT.TTree):
            #hists[nm] = obj.CloneTree (-1, 'fast')
            pass
    return hists


def mergein (f, fout, hists):
    fout.cd()
    for k in f.GetListOfKeys():
        nm = k.GetName()
        obj = getattr (f, nm)
        if isinstance (obj, ROOT.TH1):
            hists[nm].Add (obj)
        elif isinstance (obj, ROOT.TTree):
            #hists[nm].CopyEntries (obj, -1, 'fast')
            pass
    return


def finalize (fout, hists):
    fout.cd()
    for h in hists.values():
        h.Write()
    return


fout_name = sys.argv[1]
if os.path.exists (fout_name):
    print ('output file', fout_name, 'exists; not overwriting')
    sys.exit(1)
fout = ROOT.TFile (fout_name, 'RECREATE')

fname = sys.argv[2]
f = ROOT.TFile (fname)
hists = copyout (f, fout)
counts = Counts(f)
f.Close()

for fname in sys.argv[3:]:
    f = ROOT.TFile (fname)
    mergein (f, fout, hists)
    cc = Counts(f)
    counts.merge (cc)
    f.Close()

finalize (fout, hists)
counts.dump()

