import sys
import os
import ROOT
import cppyy
from draw_obj import draw_obj, zone
from printpdf import printpdf

if 'events' not in globals():
    f = ROOT.TFile ('lepeff-ele.root')
    events = f.events

mydir = os.path.dirname (sys.argv[0])

if not 'style_loaded' in globals():
    ROOT.gROOT.LoadMacro (os.path.join (mydir, 'atlas-macros/AtlasStyle.C'))
    ROOT.gROOT.LoadMacro (os.path.join (mydir, 'atlas-macros/AtlasUtils.C'))
    ROOT.gROOT.LoadMacro (os.path.join (mydir, 'atlas-macros/AtlasLabels.C'))
    ROOT.gROOT.ProcessLine ("SetAtlasStyle();")
    ROOT.gStyle.SetErrorX (0)
    style_loaded = True


objs = []

flavdesc = {'ele' : 'Electron', 'muo' : 'Muon'}
vardesc = {'pt' : 'p_{T} [GeV]', 'theta' : '#theta'}
vardesc = {'pt'    : ('p_{T} [GeV]', 50,  0,   100),
           'theta' : ('#theta',      50,  0,   3.2),
           'phi'   : ('#phi',        50, -3.2, 3.2),
           'eta'   : ('#eta',        50, -3.0, 3.0)}

def matchplot (events, flav, var, xleg=0.7, yleg=0.7):
    name_all = f'{flav}_{var}_mcall'
    name_unmatched = f'{flav}_{var}_mcunmatched'
    vtitle, nbins, xlo, xhi = vardesc[var]
    hall = ROOT.TH1F (name_all, name_all,
                      nbins, xlo, xhi)
    hunmatched = ROOT.TH1F (name_unmatched, name_unmatched, 
                            nbins, xlo, xhi)
    events.Project (name_all, f'mc{flav}_{var}')
    events.Project (name_unmatched, f'mc{flav}_unmatched_{var}')
    hall.SetMinimum(0)
    hall.GetXaxis().SetTitle (f'MC {flavdesc[flav]} {vtitle}')
    hunmatched.SetFillColor (ROOT.kRed)
    draw_obj (hall)
    draw_obj (hunmatched, 'SAME')

    leg = ROOT.TLegend (xleg, yleg, xleg+0.2, yleg+0.2)
    leg.SetFillColor (0)
    leg.SetTextSize (0.04)
    leg.SetBorderSize (0)
    leg.AddEntry (hall, 'All', 'f')
    leg.AddEntry (hunmatched, 'Unmatched', 'f')
    leg.Draw ('l')

    objs.append ([hall, hunmatched, leg])
    return


def lepplots (events, flav):
    zone (2, 2)
    matchplot (events, flav, 'pt')
    matchplot (events, flav, 'theta', xleg=0.5, yleg=0.6)
    matchplot (events, flav, 'phi', xleg=0.6, yleg=0.5)
    #matchplot (events, flav, 'eta', xleg=0.7, yleg=0.7)
    printpdf (f'plots/{flav}match')
    return


def effplot (events, flav, var):
    name_all = f'{flav}_{var}_mcall'
    name_matched = f'{flav}_{var}_mcmatched'
    vtitle, nbins, xlo, xhi = vardesc[var]
    hall = ROOT.TH1F (name_all, name_all,
                      nbins, xlo, xhi)
    hmatched = ROOT.TH1F (name_matched, name_matched, 
                          nbins, xlo, xhi)
    events.Project (name_all, f'mc{flav}_{var}')
    events.Project (name_matched, f'mc{flav}_matched_{var}')
    hall.Sumw2()
    hmatched.Sumw2()
    hmatched.Divide (hall)
    hmatched.GetXaxis().SetTitle (f'MC {flavdesc[flav]} {vtitle}')
    draw_obj (hmatched)

    l = ROOT.TLine (xlo, 1, xhi, 1)
    l.SetLineStyle (2)
    l.Draw()
    objs.append ([hall, hmatched, l])
    return


def effplots (events, flav):
    zone (2, 2)
    effplot (events, flav, 'pt')
    effplot (events, flav, 'theta')
    effplot (events, flav, 'phi')
    #effplot (events, flav, 'eta')
    printpdf (f'plots/{flav}eff')
    return


#muo, res, pt
def plotres1 (events, nm, flav, var1, var2, title=None, max=None):
    hname = f'{flav}_res_{var1}_{var2}'
    if var2 == 'pt':
        xmin, xmax = 0, 100
    elif var2 == 'theta':
        xmin, xmax = 0, 3.5
    elif var2 == 'phi':
        xmin, xmax = -3.5, 3.5
    res2d = ROOT.TH2F (hname, hname, 25, xmin, xmax, 200, -0.1, 0.1)
    events.Project (hname, f'mc{flav}_matched_{var1}:mc{flav}_matched_{var2}')
    oa = ROOT.TObjArray()
    res2d.FitSlicesY (cppyy.nullptr, 0, -1, 0, "", oa)
    if max != None: oa[2].SetMaximum (max)
    oa[2].SetMinimum (0)
    oa[2].GetXaxis().SetTitle (var2)
    draw_obj (oa[2])
    if title != None: ROOT.myText(0.25, 0.25, 1, title)
    return oa[2].Clone (nm + oa[2].GetName())


def plotres (events, flav, var1, max=None):
    zone (2, 2)
    nm='nm'
    plotres1 (events, nm, flav, var1, 'pt', 'pt resolution vs pt', max)
    plotres1 (events, nm, flav, var1, 'theta', 'pt resolution vs Theta', max)
    plotres1 (events, nm, flav, var1, 'phi', 'pt resolution vs Phi', max)
    plotresbook (events, flav, max=0.1)
    printpdf (f'plots/{flav}res')
    return


# pt res like in FCC book
def plotresbook (events, flav, title=None, max=None):
    hname = f'{flav}_res_book'
    xmin, xmax = 0, 100
    res2d = ROOT.TH2F (hname, hname, 25, xmin, xmax, 200, -0.1, 0.1)

    events.Project (hname, f'mc{flav}_matched_diff/mc{flav}_matched_pt/mc{flav}_matched_pt*100:mc{flav}_matched_p')
    oa = ROOT.TObjArray()
    res2d.FitSlicesY (cppyy.nullptr, 0, -1, 0, "", oa)
    if max != None: oa[2].SetMaximum (max)
    oa[2].SetMinimum (0)
    oa[2].GetYaxis().SetTitle ('#Delta p_{T} / p_{T,true}^{2} #times 100')
    oa[2].GetXaxis().SetTitle ('p')
    draw_obj (oa[2])
    if title != None: ROOT.myText(0.25, 0.25, 1, title)
    return oa[2].Clone (oa[2].GetName())


def plotmll1 (events, flav):
    hname1 = f'{flav}_m'
    h1 = ROOT.TH1F (hname1, hname1, 150, 0, 150)
    events.Project (hname1, f'{flav}_m')
    draw_obj (h1)

    fcb = ROOT.TF1 (f'{flav}_m_f', 'crystalball', 0, 150)
    fcb.SetParameters (h1.GetMaximum(), 90, 3, 1, 2)
    ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2")
    r = h1.Fit (fcb, 'sq')
    if h1.GetEntries() > 100:
        draw_obj (fcb, 'same')
        w = r.Parameter(2)
        l = ROOT.TLatex()
        l.SetTextSize(0.07)
        l.SetNDC()
        l.DrawLatex (0.7, 0.8, f'CB width {w:.2f}')
        objs.append ((l, h1))

    return h1

def plotmll (events, flav, which=''):
    zone (1, 2)

    h1 = plotmll1 (events, flav + which)
    h2 = plotmll1 (events, f'mc{flav}')

    printpdf (f'plots/{flav}_m')
    return



import os
os.makedirs ('plots', exist_ok=True)
#lepplots (events, 'ele')
#lepplots (events, 'muo')
#effplots (events, 'ele')
#effplots (events, 'muo')
#plotres (events, 'muo', 'res', max=0.02)
#plotres (events, 'ele', 'res', max=0.02)
#plotmll (events, 'ee')
#plotmll (events, 'mm')


def execfile (f = os.path.join (mydir, 'lepeff-plots.py')):
    exec (open(f).read(), globals())
    return
