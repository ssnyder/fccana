#fccanalysis run muonTuple.py --output muonTuple.root --files-list ../data/reco-cld/hinvmm.1002_500/hinvmm.1002_500.rec.root  2>&1|tee log-muonTuple
#fccanalysis run muonTuple.py --output muonTuple.root --files-list  ../data/reco-cld/hinvmm.2003_20000/hinvmm.2003_20000_*.rec_edm4hep.root  2>&1|tee log-muonTuple
#fccanalysis run muonTuple.py --output muonTuple.root --files-list  ../data/reco-cld/hinvmm-wide.2103_20000/hinvmm-wide.2103_20000_*.rec_edm4hep.root  2>&1|tee log-muonTuple

import ROOT
code = open ('recoil.cc').read()
ROOT.gInterpreter.Declare(code)

class RDFanalysis:
    def analysers (df):
        df2 = (
            df
            .Define("event_number", "EventHeader[0].eventNumber")
            .Define("run_number",   "EventHeader[0].runNumber")
            .Alias("TightSelectedPandoraPFOsIndex", "TightSelectedPandoraPFOs_objIdx.index")
            .Define("TightSelectedPandoraPFOs", "ReconstructedParticle::get (TightSelectedPandoraPFOsIndex, PandoraPFOs)")
            .Define("rp1", "ReconstructedParticle::sel_pt(5)(TightSelectedPandoraPFOs)")
            .Define("rp2", "ReconstructedParticle::sel_eta(4)(rp1)")
            .Define("mcp1", "MCParticle::sel_genStatus(1)(MCParticles)")
            .Define("mcp2", "MCParticle::sel_pt(5)(mcp1)")

            .Define('mcisrgam', 'sel_isrgam()(MCParticles, _MCParticles_daughters)')
            .Define('mcisrgamz1', 'calc_mcisrgamz1(mcisrgam)')
            .Define('mcisrgamz2', 'calc_mcisrgamz2(mcisrgam)')
            .Define('x1', '1 - mcisrgamz1/120.')
            .Define('x2', '1 + mcisrgamz2/120.')
            .Define('beta', '(x1-x2)/(x1+x2)')
            .Define('truesqrts0', 'calc_truesqrts()(MCParticles)')
            .Define('truesqrts', '240.*sqrt(x1*x2)')
            .Define("muo", "sel_type(13)(rp2)")
            .Define("muo_pt", "ReconstructedParticle::get_pt(muo)")
            .Define("muo_p",  "ReconstructedParticle::get_p(muo)")
            .Define("muo_eta", "ReconstructedParticle::get_eta(muo)")
            .Define("muo_theta", "ReconstructedParticle::get_theta(muo)")
            .Define("muo_phi", "ReconstructedParticle::get_phi(muo)")
            .Define("mcmuo", "MCParticle::sel_pdgID(13,true)(mcp2)")
            .Define("mcmuo_pt", "MCParticle::get_pt(mcmuo)")
            .Define("mcmuo_eta", "MCParticle::get_eta(mcmuo)")
            .Define("mcmuo_theta", "MCParticle::get_theta(mcmuo)")
            .Define("mcmuo_phi", "MCParticle::get_phi(mcmuo)")
            .Define("mcmuo_matchrel", "match(mcmuo, muo)")
            .Define("mcmuo_matched", "sel_matched(mcmuo, mcmuo_matchrel)")
            .Define("mcmuo_matched_pt", "MCParticle::get_pt(mcmuo_matched)")
            .Define("mcmuo_matched_p", "MCParticle::get_p(mcmuo_matched)")
            .Define("mcmuo_matched_eta", "MCParticle::get_eta(mcmuo_matched)")
            .Define("mcmuo_matched_theta", "MCParticle::get_theta(mcmuo_matched)")
            .Define("mcmuo_matched_phi", "MCParticle::get_phi(mcmuo_matched)")

            .Define("z", "resonanceBuilder(91)(muo)")
            .Define("z_pt", "ReconstructedParticle::get_pt(z)")
            .Define("z_p", "ReconstructedParticle::get_p(z)")
            .Define("z_theta", "ReconstructedParticle::get_theta(z)")
            .Define("z_phi", "ReconstructedParticle::get_phi(z)")
            .Define("z_m", "ReconstructedParticle::get_mass(z)")
            .Define('h', 'recoilBuilder()(240, z)')
            .Define("h_pt", "ReconstructedParticle::get_pt(h)")
            .Define("h_p", "ReconstructedParticle::get_p(h)")
            .Define("h_theta", "ReconstructedParticle::get_theta(h)")
            .Define("h_phi", "ReconstructedParticle::get_phi(h)")
            .Define("h_m", "ReconstructedParticle::get_mass(h)")
            .Define('htrues', 'recoilBuilder()(truesqrts, z)')
            .Define("htrues_pt", "ReconstructedParticle::get_pt(htrues)")
            .Define("htrues_p", "ReconstructedParticle::get_p(htrues)")
            .Define("htrues_theta", "ReconstructedParticle::get_theta(htrues)")
            .Define("htrues_phi", "ReconstructedParticle::get_phi(htrues)")
            .Define("htrues_m", "ReconstructedParticle::get_mass(htrues)")
            .Define("mch", "MCParticle::sel_pdgID(25,true)(MCParticles)")
            .Define("mch_pt", "MCParticle::get_pt(mch)")
            .Define("mch_p", "MCParticle::get_p(mch)")
            .Define("mch_theta", "MCParticle::get_theta(mch)")
            .Define("mch_phi", "MCParticle::get_phi(mch)")
            .Define("mch_m", "MCParticle::get_mass(mch)")
            )
        return df2

    def output():
        return [
            'event_number',
            'run_number',
            'mcisrgam',
            'mcisrgamz1',
            'mcisrgamz2',
            'x1',
            'x2',
            'beta',
            'truesqrts0',
            'truesqrts',
            'muo',
            'muo_pt',
            'muo_p',
            'muo_eta',
            'muo_theta',
            'muo_phi',
            'mcmuo',
            'mcmuo_pt',
            'mcmuo_eta',
            'mcmuo_theta',
            'mcmuo_phi',
            'mcmuo_matched',
            'mcmuo_matched_pt',
            'mcmuo_matched_p',
            'mcmuo_matched_eta',
            'mcmuo_matched_theta',
            'mcmuo_matched_phi',
            'z',
            'z_pt',
            'z_p',
            'z_theta',
            'z_phi',
            'z_m',
            'h',
            'h_pt',
            'h_p',
            'h_theta',
            'h_phi',
            'h_m',
            'htrues',
            'htrues_pt',
            'htrues_p',
            'htrues_theta',
            'htrues_phi',
            'htrues_m',
            'mch',
            'mch_pt',
            'mch_p',
            'mch_theta',
            'mch_phi',
            'mch_m',
            ]
