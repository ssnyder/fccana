import cairo
import os
import DDG4
import ROOT
from math import sqrt, sin, cos, tan, pi, atan2, hypot

TWOPAGE = False

fname = 'pcbdraw.pdf'
WIDTH = 11*72.27
HEIGHT = 8.5*72.27
SCALE = 4.7 if TWOPAGE else 2.3
BINFAC = 4
FONTSIZE = 2
BINOFFSET = 2
LAYEROFFSET = 3

class ECalGeo:
    def __init__ (self):
        lcgeo = os.environ['LCGEO']
        kernel = DDG4.Kernel()
        geo_file = "file:" + lcgeo + "/FCCee/ALLEGRO/compact/ALLEGRO_o1_v03/ALLEGRO_o1_v03.xml"
        kernel.loadGeometry(geo_file)

        code = open ('ddtest.cc').read()
        ROOT.gInterpreter.Declare(code)

        self.dd = kernel.detectorDescription()
        self.emb = self.dd.detector('ECalBarrel')
        self.bath = self.emb.child('bath')
        self.readout0 = self.bath.child('readout0')
        self.vol_PCB = self.readout0.volume()

        self.rmin = self.dd.constantAsDouble('EMBarrel_rmin')
        self.rmax = self.dd.constantAsDouble('EMBarrel_rmax')
        self.angle = self.dd.constantAsDouble('InclinationAngle')

        self.layers = ROOT.getLayerInfo(self.emb).layers
        self.layer_pos = [self.r_to_l (l.distance) for l in self.layers]

        self.ecro = self.dd.readout('ECalBarrelModuleThetaMerged')
        self.seg = self.ecro.segmentation()
        for p in self.seg.parameters():
            n = p.name()
            if n == 'offset_theta':
                self.offset_theta = float (p.value())
            elif n == 'grid_size_theta':
                self.grid_size_theta = float (p.value())
            elif n == 'mergedCells_Theta':
                self.theta_ganging = [int(x) for x in p.value().split()]
        return

    # PCB height
    def pcb_h (self):
        return 2*self.vol_PCB.GetShape().GetDZ()

    # PCB z half-width
    def pcb_zwid (self):
        return self.vol_PCB.GetShape().GetDY()

    # Convert cylindrical radius r to distance along the PCB l.
    # This is the incorrect version used in the geometry.
    #def r_to_l (self, r):
    #    dR = self.rmax - self.rmin
    #    scale_fact = dR / (-self.rmin*cos(self.angle) + sqrt(self.rmax**2 - (self.rmin*sin(self.angle))**2))
    #    return (r - self.rmin) / scale_fact
    #def l_to_r (self, l):
    #    dR = self.rmax - self.rmin
    #    scale_fact = dR / (-self.rmin*cos(self.angle) + sqrt(self.rmax**2 - (self.rmin*sin(self.angle))**2))
    #    return l*scale_fact + self.rmin

    # Correct version?
    def r_to_l (self, r):
        return sqrt (r**2 - (self.rmin*sin(self.angle))**2) - self.rmin*cos(self.angle)
    def l_to_r (self, l):
        return sqrt (self.rmin**2 + l**2 + 2*l*self.rmin*cos(self.angle))
        
        


ecalgeo = ECalGeo()


def binline (ecalgeo, th):
    rmin = ecalgeo.rmin
    rmax = ecalgeo.rmax
    lmax = ecalgeo.layer_pos[-1]
    zwid = ecalgeo.pcb_zwid()

    cotth = 1/tan(th)
    l0 = 0
    z0 = rmin*cotth
    z1 = rmax*cotth
    l1 = lmax
    if z1 > zwid:
        z1 = zwid
        l1 = ecalgeo.r_to_l (z1/cotth)
    return (l0, z0, l1, z1)


def draw_binlines (ctx, ecalgeo):
    ss = ctx.get_source()
    ctx.set_source_rgba (0.5, 0, 0)

    zwid = ecalgeo.pcb_zwid()

    # offset_theta is the theta of the _center_ of the first bin.
    th = ecalgeo.offset_theta - ecalgeo.grid_size_theta/2
    while th <= pi/2:
        l0, z0, l1, z1 = binline (ecalgeo, th)
        if z0 < zwid:
            ctx.move_to (z0, l0)
            ctx.line_to (z1, l1)
        th += ecalgeo.grid_size_theta * ecalgeo.theta_ganging[0]
    ctx.stroke()
    ctx.set_source (ss)
    return


def draw_edge (ctx, ecalgeo, lmin, lmax, theta):
    maxdist = 0
    maxzdist = 0
    l0, z0, l1, z1 = binline (ecalgeo, theta)

    zwid = ecalgeo.pcb_zwid()
    cotth = 1/tan(theta)
    lstep = 0.1
    l = lmin
    r = ecalgeo.l_to_r (l)
    p0 = (r*cotth, l)
    if p0[0] >= zwid: return (None, None, 0, 0)
    ctx.move_to (p0[0], l)
    l += lstep
    while l < lmax and r*cotth < zwid:
        r = ecalgeo.l_to_r (l)
        z = min (zwid, r*cotth)
        ctx.line_to (z, l)

        dist = abs ((l1-l0)*z - (z1-z0)*l + z1*l0 - l1*z0) / hypot (l1-l0, z1-z0)
        maxdist = max (maxdist, dist)
        zz = (l-l0) * (z1-z0)/(l1-l0) + z0
        maxzdist = max (maxzdist, abs(zz-z))
        l += lstep
    r = ecalgeo.l_to_r (l)
    p1 = (min (zwid, r*cotth), l)

    l = lmax
    r = ecalgeo.l_to_r (l)
    p2 = (min (zwid, r*cotth), l)
    ctx.line_to (p2[0], l)
    ctx.stroke()
    return (p0, p1, maxdist, maxzdist)


def draw_index (ctx, ecalgeo, ibin, pa, pb):
    m = cairo.Matrix()
    zwid = ecalgeo.pcb_zwid()
    h = ecalgeo.pcb_h()
    if abs(pa[0]-zwid)<1e-6 and abs(pb[0]-zwid)<1e-6:
        # Right edge
        #print ('r', ibin, pa, pb)
        yavg = (pa[1]+pb[1])/2
        m.scale (FONTSIZE, -FONTSIZE)
        ctx.set_font_matrix (m)
        ctx.move_to (zwid+BINOFFSET, yavg - FONTSIZE/2)
        ctx.show_text ('%d' % (ibin*BINFAC))
        ctx.stroke()
    elif abs(pa[1]-h)<2 and abs(pb[1]-h)<0.1:
        # Top edge
        th = ecalgeo.offset_theta + ibin*BINFAC*ecalgeo.grid_size_theta
        m.rotate (th)
        m.scale (FONTSIZE, -FONTSIZE)
        ctx.set_font_matrix (m)
        #print ('t', ibin, th, xth, pa, pb)
        ctx.move_to (ecalgeo.l_to_r(h+BINOFFSET)/tan(th)-FONTSIZE/4, h+BINOFFSET)
        ctx.show_text ('%d' % (ibin*BINFAC))
        ctx.stroke()
    else:
        print ('x', h, pa, pb)
    return


def draw_theta (ctx, ecalgeo, ibin, p):
    ctx.set_line_width (SCALE / 100)
    m = cairo.Matrix()
    zwid = ecalgeo.pcb_zwid()
    h = ecalgeo.pcb_h()
    th = ecalgeo.offset_theta + ibin*BINFAC*ecalgeo.grid_size_theta - ecalgeo.grid_size_theta/2
    if abs(p[0]-zwid)<1e-6:
        # Right edge
        ctx.move_to (zwid + 1, p[1])
        ctx.line_to (zwid + 6, p[1])
        ctx.stroke()
        m.scale (FONTSIZE, -FONTSIZE)
        ctx.set_font_matrix (m)
        ctx.move_to (zwid + 7, p[1] - FONTSIZE/4)
        ctx.show_text ('%.4f' % th)
        ctx.stroke()
    elif abs(p[1]-h)<2:
        # Top
        xr = hypot (p[0], ecalgeo.l_to_r(p[1]))
        def ext(r):
            return ((xr+r)*cos(th), ecalgeo.r_to_l((xr+r)*sin(th)))
        ctx.move_to (*ext(1))
        ctx.line_to (*ext(6))
        ctx.stroke()
        m.rotate (th)
        m.scale (FONTSIZE, -FONTSIZE)
        ctx.set_font_matrix (m)
        tx, ty = ext(7)
        ctx.move_to (tx + FONTSIZE/4, ty)
        ctx.show_text ('%.4f' % th)
        ctx.stroke()
    return


def draw_layers (ctx, ecalgeo):
    m = cairo.Matrix()
    m.scale (FONTSIZE, -FONTSIZE)
    ctx.set_font_matrix (m)
    for ilayer, lmax in enumerate(ecalgeo.layer_pos):
        lmin = 0 if ilayer==0 else ecalgeo.layer_pos[ilayer-1]
        lavg = (lmin + lmax)/2
        ctx.move_to (-LAYEROFFSET, lavg)
        ctx.show_text ('%d' % ilayer)
        ctx.stroke()
    return


def draw_labels (ctx, ecalgeo, binpos):
    plast = [0, ecalgeo.pcb_h()]

    for ibin in range(len(binpos)-1):
        if binpos[ibin] is not None and binpos[ibin+1] is not None:
            draw_index (ctx, ecalgeo, ibin, binpos[ibin][1], binpos[ibin+1][1])
    draw_index (ctx, ecalgeo, len(binpos)-1, binpos[-1][1], plast)

    for ibin, p in enumerate (binpos):
        if p is not None:
            draw_theta (ctx, ecalgeo, ibin, p[1])
    draw_theta (ctx, ecalgeo, len(binpos), plast)

    draw_layers (ctx, ecalgeo)
    return


def draw_bins (ctx, ecalgeo, ilayer, binpos):
    maxdist = 0
    maxzdist = 0
    nlayers = len(ecalgeo.layer_pos)
    # offset_theta is the theta of the _center_ of the first bin.
    th = ecalgeo.offset_theta - ecalgeo.grid_size_theta/2
    lmin = 0 if ilayer==0 else ecalgeo.layer_pos[ilayer-1]
    lmax = ecalgeo.layer_pos[ilayer]
    itheta = 0
    while th <= pi/2:
        p0, p1, this_maxdist, this_maxzdist = draw_edge (ctx, ecalgeo, lmin, lmax, th)
        maxdist = max (maxdist, this_maxdist)
        maxzdist = max (maxzdist, this_maxzdist)
        if p0 and itheta%BINFAC == 0:
            ibin = itheta//BINFAC
            if len(binpos) <= ibin:
                binpos += (ibin-len(binpos)+1)*[None]
            if binpos[ibin] is None:
                binpos[ibin] = [p0, p1]
            else:
                binpos[ibin][1] = p1
        th_last = th
        th += ecalgeo.grid_size_theta * ecalgeo.theta_ganging[ilayer]
        itheta += ecalgeo.theta_ganging[ilayer]
    return maxdist, maxzdist


def draw_ruler (ctx, rlen, side):
    m = cairo.Matrix()
    m.scale (FONTSIZE, -FONTSIZE)
    ctx.set_font_matrix (m)

    DIV = 10
    divs = int((rlen+DIV-1)//DIV)
    ctx.move_to (0, 0)
    ctx.line_to (divs*DIV, 0)
    ctx.move_to (divs*DIV+2, -FONTSIZE/4)
    ctx.show_text ('[cm]')

    m.rotate (pi/2)
    ctx.set_font_matrix (m)
    if side==0:
        ypos = -3
    else:
        ypos = 4
    for i in range(divs+1):
        ltick = 2 if i%10 == 0 else 1
        ctx.move_to (i*DIV, ltick)
        ctx.line_to (i*DIV, -ltick)
        ctx.move_to (i*DIV-FONTSIZE/4, ypos)
        ctx.show_text ('%d' % (i*DIV))
    ctx.stroke()
    return


def draw_x_ruler (ctx, ecalgeo):
    m = ctx.get_matrix()
    ctx.translate (0, -10)
    draw_ruler (ctx, ecalgeo.pcb_zwid(), 0)
    ctx.set_matrix (m)
    return


def draw_y_ruler (ctx, ecalgeo):
    m = ctx.get_matrix()
    ctx.rotate (pi/2)
    ctx.translate (0, 7)
    draw_ruler (ctx, ecalgeo.pcb_h(), 1)
    ctx.set_matrix (m)
    return


def draw_legends (ctx, ecalgeo):
    off = ctx.get_font_face()
    m = cairo.Matrix()
    m.scale (FONTSIZE, -FONTSIZE)
    m.rotate (pi/2)
    ctx.set_font_matrix (m)
    ctx.move_to (ecalgeo.pcb_zwid()+2, -2)
    ctx.show_text ('itheta')
    ctx.stroke()
    ctx.move_to (-3, -2)
    ctx.show_text ('ilayer')
    ctx.stroke()

    ff = cairo.ToyFontFace ('serif')
    ctx.set_font_face(ff)
    m = cairo.Matrix()
    m.scale (1.5*FONTSIZE, -1.5*FONTSIZE)
    ctx.set_font_matrix (m)
    ctx.move_to (ecalgeo.pcb_zwid()+9, -5)
    ctx.show_text ('\N{GREEK SMALL LETTER THETA}') #U+03b8
    ctx.stroke()
    ctx.set_font_face (off)

    return


def drawit(ctx, ecalgeo):
    ctx.set_line_width (SCALE / 10)
    h = ecalgeo.pcb_h()
    zwid = ecalgeo.pcb_zwid()
    ctx.move_to (0, 0)
    ctx.line_to (zwid, 0)
    ctx.line_to (zwid, h)
    ctx.line_to (0, h)
    ctx.stroke()

    ctx.set_line_width (SCALE / 50)
    ctx.move_to (0, 0)
    ctx.line_to (0, h)
    ctx.stroke()

    for y in ecalgeo.layer_pos:
        ctx.move_to (0, y)
        ctx.line_to (zwid, y)
    ctx.stroke()

    draw_binlines (ctx, ecalgeo)

    maxdist = 0
    maxzdist = 0
    binpos = []
    for ilayer in range (len (ecalgeo.layer_pos)):
        this_maxdist, this_maxzdist = draw_bins (ctx, ecalgeo, ilayer, binpos)
        maxdist = max (maxdist, this_maxdist)
        maxzdist = max (maxzdist, this_maxzdist)
    draw_labels (ctx, ecalgeo, binpos)
    draw_x_ruler (ctx, ecalgeo)
    draw_y_ruler (ctx, ecalgeo)
    draw_legends (ctx, ecalgeo)

    print (f'maxdist: {maxdist:.2f}  maxzdist: {maxzdist:.2f}  (cm)')
    ctx.show_page()
    return
    

fout = open(fname, 'wb')
surface = cairo.PDFSurface (fout, WIDTH, HEIGHT)
ctx = cairo.Context (surface)
ctx.scale(SCALE, -SCALE)
ctx.translate(12, -ecalgeo.pcb_h() - 20)
drawit(ctx, ecalgeo)
if TWOPAGE:
    ctx.translate(-WIDTH/SCALE, 0)
    drawit(ctx, ecalgeo)
surface.finish()
fout.close()

os.system ('xpdf '+fname)
