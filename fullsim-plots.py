import sys
import os
import ROOT
import cppyy
from label import Label, texGeV
from draw_obj import draw_obj, zone, get_canvas
from printpdf import printpdf


MZ = 91.19


objs = []


mydir = os.path.dirname (sys.argv[0])


if 't_zz_qqqq' not in globals():
    f_hinv_ee = ROOT.TFile ('hinv-ee.root')
    t_hinv_ee = f_hinv_ee.events
    t_hinv_ee.nm = 'hinv-ee'
    f_hinv_mm = ROOT.TFile ('hinv-mm.root')
    t_hinv_mm = f_hinv_mm.events
    t_hinv_mm.nm = 'hinv-mm'
    f_hinv_qq = ROOT.TFile ('hinv-qq.root')
    t_hinv_qq = f_hinv_qq.events
    t_hinv_qq.nm = 'hinv-qq'
    f_zz_qqqq = ROOT.TFile ('zz_qqqq.root')
    t_zz_qqqq = f_zz_qqqq.events
    t_zz_qqqq.nm = 'qqqq'
    f_zz_mmqq = ROOT.TFile ('zz_mmqq.root')
    t_zz_mmqq = f_zz_mmqq.events
    t_zz_mmqq.nm = 'mmqq'
    f_zz_eeqq = ROOT.TFile ('zz_eeqq.root')
    t_zz_eeqq = f_zz_eeqq.events
    t_zz_eeqq.nm = 'eeqq'
    f_zh_nneenn = ROOT.TFile ('zh_nneenn.root')
    t_zh_nneenn = f_zh_nneenn.events
    f_zh_nnmmnn = ROOT.TFile ('zh_nnmmnn.root')
    t_zh_nnmmnn = f_zh_nnmmnn.events

    f_ww_eenn = ROOT.TFile('wzp6_ee_WW_enuenu_ecm240.root')
    t_ww_eenn = f_ww_eenn.events

    #f_fast = ROOT.TFile('lepeff-cld-jj.root')
    #t_fast = f_fast.events
    f_eff = ROOT.TFile('lepeff-ele.root')
    t_eff=f_eff.events
    f_nn = ROOT.TFile('lepfsr-nn.root')
    t_train = f_nn.dataset.TrainTree
    t_test = f_nn.dataset.TestTree

    f_norm = ROOT.TFile('hinv-norm.root')
    f_fast_zz_ee = ROOT.TFile('FCCMiniSamples/2e_channel/p8_ee_ZZ_ecm240_sel0_NoCuts_output.root')
    f_fast_zz_mm = ROOT.TFile('FCCMiniSamples/2mu_channel/p8_ee_ZZ_ecm240_sel0_NoCuts_output.root')
    f_fast_zz_qq = ROOT.TFile('FCCMiniSamples/2q_channel/p8_ee_ZZ_ecm240_sel0_NoCuts_output.root')

    f_fast_ww_ee = ROOT.TFile('FCCMiniSamples/fastSimNominal/2e_channel/p8_ee_WW_ecm240_sel0_NoCuts_output.root')
    f_fast_ww_mm = ROOT.TFile('FCCMiniSamples/fastSimNominal/2mu_channel/p8_ee_WW_ecm240_sel0_NoCuts_output.root')
    f_fast_ww_qq = ROOT.TFile('FCCMiniSamples/fastSimNominal/2q_channel/p8_ee_WW_ecm240_sel0_NoCuts_output.root')

    f_fast_zh_ee = ROOT.TFile('FCCMiniSamples/2e_channel/wzp6_ee_nunuH_HZZ_ecm240_sel0_NoCuts_output.root')
    f_fast_zh_mm = ROOT.TFile('FCCMiniSamples/2mu_channel/wzp6_ee_nunuH_HZZ_ecm240_sel0_NoCuts_output.root')
    f_fast_zh_qq = ROOT.TFile('FCCMiniSamples/2q_channel/wzp6_ee_nunuH_HZZ_ecm240_sel0_NoCuts_output.root')


#style_loaded = True
if not 'style_loaded' in globals():
    ROOT.gROOT.LoadMacro (os.path.join (mydir, 'atlas-macros/AtlasStyle.C'))
    ROOT.gROOT.LoadMacro (os.path.join (mydir, 'atlas-macros/AtlasUtils.C'))
    ROOT.gROOT.LoadMacro (os.path.join (mydir, 'atlas-macros/AtlasLabels.C'))
    ROOT.gROOT.ProcessLine ("SetAtlasStyle();")
    ROOT.gStyle.SetErrorX (0)
    style_loaded = True


def makevisphi (events, nm, root):
    name = f'{root}_{nm}_phi'
    h = ROOT.TH1F (name, name, 16, -3.2, 3.2)
    events.Project (name, f'{root}_phi')
    h.GetXaxis().SetTitle ('#phi(visible)')
    h.GetYaxis().SetTitle ('Events/bin')
    h.SetMinimum (0)
    return h
def makevispx (events, nm, root):
    name = f'{root}_{nm}_phi'
    h = ROOT.TH1F (name, name, 40, -20, 20)
    bw = h.GetXaxis().GetBinWidth(1)
    events.Project (name, f'{root}_pt*cos({root}_phi)')
    h.GetXaxis().SetTitle (r'p_{x}\,\text{(visible) ['+texGeV+']}')
    h.GetYaxis().SetTitle (f'\\text{{Entries / {bw:.1f} '+texGeV+'}}')
    return h
def plotvis (events, vis, makefunc, showmean = False):
    hvis = makefunc (events, events.nm, vis)
    hvisraw = makefunc (events, events.nm, vis + 'raw')
    hvisraw.SetLineColor(2)
    hvisraw.SetLineStyle(2)
    hvisraw.Draw()
    hvis.Draw('same')
    objs.append ((hvis, hvisraw))

    leg = ROOT.TLegend (0.2, 0.48, 0.3, 0.68)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.035)
    leg.AddEntry (hvisraw, r'\text{Uncorrected}', 'L')
    if showmean:
        leg.AddEntry (ROOT.nullptr, f'\\mu={hvisraw.GetMean():.2f}\\pm{hvisraw.GetMeanError():.2f}\\ \\text{{'+texGeV+'}}', '')
    leg.AddEntry (hvis, r'\text{Corrected}', 'L')
    if showmean:
        leg.AddEntry (ROOT.nullptr, f'\\mu={hvis.GetMean():.2f}\\pm{hvis.GetMeanError():.2f}\\ \\text{{'+texGeV+'}}', '')
    leg.Draw()
    objs.append (leg)
    lab = Label(0.2, 0.85)
    if events.nm == 'qqqq':
        lab.addline (r'(Z\rightarrow qq)(Z\rightarrow qq)')
    elif events.nm == 'eeqq':
        lab.addline (r'(Z\rightarrow ee)(Z\rightarrow qq)')
    elif events.nm == 'mmqq':
        lab.addline (r'(Z\rightarrow \mu\mu)(Z\rightarrow qq)')
    objs.append (lab)
    return
def plotvisphi (events):
    plotvis (events, 'vis', makevisphi)
    return
def plotvispx (events):
    plotvis (events, 'vis', makevispx, True)
    return
def plotmcvisphi (events):
    plotvis (events, 'mcvis', makevisphi)
    return
def plotmcvispx (events):
    plotvis (events, 'mcvis', makevispx, True)
    return


def plotpdf (dirname, nameroot, f, *args, **kw):
    if not os.path.exists (dirname):
        os.mkdir (dirname)
    f (*args, **kw)
    c1 = get_canvas();
    c1.Print (f'{dirname}/{nameroot}.pdf','pdf,Portrait')
    return

def ploteps (dirname, nameroot, f, *args, **kw):
    if not os.path.exists (dirname):
        os.mkdir (dirname)
    f (*args, **kw)
    c1 = get_canvas();
    c1.Print (f'{dirname}/{nameroot}.eps','eps,Portrait')
    return

def plotpdf_met (nameroot, f, *args):
    return plotpdf ('met-plots', nameroot, f, *args)
def met_plots():
    plotpdf_met ('visphi_qqqq', plotvisphi, t_zz_qqqq)
    plotpdf_met ('visphi_eeqq', plotvisphi, t_zz_eeqq)
    plotpdf_met ('visphi_mmqq', plotvisphi, t_zz_mmqq)
    plotpdf_met ('vispx_qqqq', plotvispx, t_zz_qqqq)
    plotpdf_met ('vispx_eeqq', plotvispx, t_zz_eeqq)
    plotpdf_met ('vispx_mmqq', plotvispx, t_zz_mmqq)
    plotpdf_met ('mcvisphi_qqqq', plotmcvisphi, t_zz_qqqq)
    plotpdf_met ('mcvisphi_eeqq', plotmcvisphi, t_zz_eeqq)
    plotpdf_met ('mcvisphi_mmqq', plotmcvisphi, t_zz_mmqq)
    plotpdf_met ('mcvispx_qqqq', plotmcvispx, t_zz_qqqq)
    plotpdf_met ('mcvispx_eeqq', plotmcvispx, t_zz_eeqq)
    plotpdf_met ('mcvispx_mmqq', plotmcvispx, t_zz_mmqq)
    return


def mllhist (t, name, dobrem):
    h = ROOT.TH1F (name, name, 60, 0, 120)
    t.Project (name, 'll_m' if dobrem else 'll0_m')
    return h
def mllplot (t):
    h1 = mllhist (t, 'mll-brem', True)
    h2 = mllhist (t, 'mll-nobrem', False)
    h1.Draw()
    h2.Draw('same')
    h2.SetLineColor(2)
    h2.SetLineStyle(2)

    bw = h1.GetXaxis().GetBinWidth(1)
    h1.GetXaxis().SetTitle (r'm_{\ell\ell}\ \text{['+texGeV+']}')
    h1.GetYaxis().SetTitle (f'\\text{{Entries / {bw:.1f} '+texGeV+'}}')

    def dofit (h, leg):
        res = h.Fit ('gaus', '0S')
        mu = res.Parameter(1)
        muerr = res.ParError(1)
        sig = res.Parameter(2)
        sigerr = res.ParError(2)
        leg.AddEntry (ROOT.nullptr, f'\\mu={mu:.2f}\\pm{muerr:.2f}\\,\\text{{'+texGeV+'}}', '')
        leg.AddEntry (ROOT.nullptr, f'\\sigma={sig:.2f}\\pm{sigerr:.2f}\\,\\text{{'+texGeV+'}}', '')
        return

    leg = ROOT.TLegend (0.2, 0.3, 0.4, 0.6)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.04)
    leg.AddEntry (h1, 'With brem recovery', 'L')
    dofit (h1, leg)
    leg.AddEntry (h2, 'Without brem recovery', 'L')
    dofit (h2, leg)
    leg.Draw()

    lab = Label(0.2, 0.85, ystep = 0.07)
    if t.nm == 'hinv-ee':
        lab.addline (r'(Z\rightarrow ee)(H\rightarrow 4\nu)')
    elif t.nm == 'hinv-mm':
        lab.addline (r'(Z\rightarrow \mu\mu)(H\rightarrow 4\nu)')
    else:
        print ('bad', t.nm)
    lab.addline (r'\text{No selections on missing }p_{T}\text{ or }m_{\ell\ell}')

    ymax = h1.GetMaximum()

    l1 = ROOT.TLine (MZ - 4, 0, MZ - 4, ymax * 0.8)
    l2 = ROOT.TLine (MZ + 4, 0, MZ + 4, ymax * 0.8)
    l1.Draw()
    l2.Draw()

    objs.append ((h1,h2,leg, lab,l1,l2))

    return


def mvisplot (t):
    h = ROOT.TH1F ('mvis', 'mvis', 60, 0, 120)
    t.Project ('mvis', 'vis_m')

    h.Draw()

    bw = h.GetXaxis().GetBinWidth(1)
    h.GetYaxis().SetTitle (f'\\text{{Entries / {bw:.1f} '+texGeV+'}}')

    h.GetXaxis().SetTitle (r'm_\text{vis}\ \text{['+texGeV+']}')

    lab = Label(0.2, 0.85, ystep = 0.07)
    lab.addline (r'(Z\rightarrow qq)(H\rightarrow 4\nu)')
    lab.addline (r'\text{No selections on missing }p_{T}\text{ or }m_\text{vis}')

    def dofit (h, leg):
        res = h.Fit ('gaus', '0S')
        mu = res.Parameter(1)
        muerr = res.ParError(1)
        sig = res.Parameter(2)
        sigerr = res.ParError(2)
        leg.AddEntry (ROOT.nullptr, f'\\mu={mu:.2f}\\pm{muerr:.2f}\\,\\text{{'+texGeV+'}}', '')
        leg.AddEntry (ROOT.nullptr, f'\\sigma={sig:.2f}\\pm{sigerr:.2f}\\,\\text{{'+texGeV+'}}', '')
        return

    leg = ROOT.TLegend (0.2, 0.45, 0.4, 0.6)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.04)
    dofit (h, leg)

    leg.Draw()

    ymax = h.GetMaximum()

    l1 = ROOT.TLine (86, 0, 86, ymax * 0.8)
    l2 = ROOT.TLine (105, 0, 105, ymax * 0.8)
    l1.Draw()
    l2.Draw()

    objs.append ((h,l1,l2,leg))

    return

def recoilplot2 (f):
    if f.events.nm == 'hinv-ee':
        chan = 'ee'
    elif f.events.nm == 'hinv-mm':
        chan = 'mm'
    elif f.events.nm == 'hinv-qq':
        chan = 'qq'

    h1 = getattr(f, f'{chan}_visrecoil_m').Clone ('mrecoil-brem')
    h1.Draw()
    h2 = None
    if chan != 'qq':
        h2 = getattr(f, f'{chan}0_visrecoil0_m').Clone ('mrecoil-nobrem')
        h2.Draw('same')
        h2.SetLineColor(2)
        h2.SetLineStyle(2)

    h1.GetXaxis().SetTitle ('recoil mass [GeV]')
    h1.GetYaxis().SetTitle ('Events/bin')

    leg = None
    if chan != 'qq':
         leg = ROOT.TLegend (0.2, 0.5, 0.4, 0.7)
         leg.SetBorderSize (0)
         leg.SetTextSize (0.04)
         leg.AddEntry (h1, 'w/brem rec', 'L')
         leg.AddEntry (h2, 'no brem rec', 'L')
         leg.Draw()

    lab = Label(0.2, 0.85, ystep = 0.07)
    if chan == 'ee':
        lab.addline ('(Z#rightarrow ee)(H#rightarrow 4#nu)')
    elif chan == 'mm':
        lab.addline ('(Z#rightarrow #mu#mu)(H#rightarrow 4#nu)')
    elif chan == 'qq':
        lab.addline ('(Z#rightarrow qq)(H#rightarrow 4#nu)')
    lab.addline ('Missing E_{T}, m_{ll} selections')

    ymax = h1.GetMaximum()
    l = ROOT.TArrow (125, ymax*0.8, 125, 0)
    l.Draw()

    objs.append ((h1,h2,leg,lab,l))

    return


def nninputhist (vname, nbin, xmin, xmax, cls):
    htrain = ROOT.TH1F (f'{vname}_train_{cls}', vname, nbin, xmin, xmax)
    htest = ROOT.TH1F (f'{vname}_test_{cls}', vname, nbin, xmin, xmax)
    t_train.Project (f'{vname}_train_{cls}', vname, f'classID=={cls}')
    t_test.Project (f'{vname}_test_{cls}', vname, f'classID=={cls}')
    htrain.Add (htest)
    return htrain

def nninputplot (vname, nbin, xmin, xmax, yform,
                 xlab = 0.6, ylab = 0.85,
                 xleg = 0.6, yleg = 0.68,
                 ymaxfac = 1.1):
    h_bkg = nninputhist (vname, nbin, xmin, xmax, 1)
    h_sig = nninputhist (vname, nbin, xmin, xmax, 0)

    h_bkg.SetLineStyle (2)
    h_bkg.SetLineColor (2)

    h_sig.SetMaximum (max (h_sig.GetMaximum(), h_bkg.GetMaximum())*ymaxfac)
    h_sig.Draw()
    h_bkg.Draw('SAME')

    bw = h_sig.GetXaxis().GetBinWidth(1)
    h_sig.GetYaxis().SetTitle (yform % bw)

    lab = Label (xlab, ylab)
    lab.addline (r'(Z\rightarrow ee)(H\rightarrow 4\nu)')

    leg = ROOT.TLegend (xleg, yleg - 0.15, xleg+0.2, yleg)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.04)
    leg.AddEntry (h_sig, r'\text{Should add }\gamma', 'L')
    leg.AddEntry (h_bkg, r'\text{Should not add }\gamma', 'L')
    leg.Draw()

    objs.append ((h_sig, h_bkg, lab, leg))

    return h_sig, h_bkg


def nn_angle_plot():
    h_sig, h_bkg = nninputplot ('angle', 50, 0, 0.05,
                                r'\text{Entries / %.3f rad}')
    h_sig.GetXaxis().SetTitle (r'\Delta\theta(e,\gamma)\text{ [rad]}')
    bw = h_sig.GetXaxis().GetBinWidth(1)
    h_sig.GetYaxis().SetTitle (f'Entries / {bw:.3f} rad')
    h_sig.GetXaxis().SetNdivisions(505)
    h_sig.GetYaxis().SetNdivisions(20)
    return
    

def nn_p_plot():
    h_sig, h_bkg = nninputplot ('p', 50, 0, 100,
                                r'\text{Entries / %.0f '+texGeV+'}',
                                xlab = 0.2, ylab = 0.89, xleg=0.65, yleg = 0.9, ymaxfac=1.3)
    h_sig.GetXaxis().SetTitle (r'\text{Electron momentum ['+texGeV+']}')
    return


def nn_theta_plot():
    h_sig, h_bkg = nninputplot ('theta', 50, 0, 3.2,
                                r'\text{Entries / %.3f rad}',
                                xlab = 0.2, ylab = 0.89, xleg=0.65, yleg = 0.9, ymaxfac=1.35)
    h_sig.GetXaxis().SetTitle (r'\text{Electron }\theta\text{ [rad]}')
    return
    

def nn_clustE_plot():
    h_sig, h_bkg = nninputplot ('clustE', 50, 0, 100,
                                r'\text{Entries / %.0f '+texGeV+'}',
                                xlab = 0.2, ylab = 0.89, xleg=0.65, yleg = 0.9, ymaxfac=1.35)
    h_sig.GetXaxis().SetTitle (r'\text{Electron cluster energy ['+texGeV+']}')
    return
    

def nn_gamE_plot():
    h_sig, h_bkg = nninputplot ('gam_clustE', 50, 0, 50,
                                r'\text{Entries / %.0f '+texGeV+'}')
    h_sig.GetXaxis().SetTitle (r'\text{Photon cluster energy ['+texGeV+']}')
    return


def nn_out_plot():
    h_train_sig = ROOT.TH1F ('train_sig', 'train_sig', 50, 0, 1)
    h_train_bkg = ROOT.TH1F ('train_bkg', 'train_bkg', 50, 0, 1)
    h_test_sig = ROOT.TH1F ('test_sig', 'test_sig', 50, 0, 1)
    h_test_bkg = ROOT.TH1F ('test_bkg', 'test_bkg', 50, 0, 1)

    t_train.Project ('train_sig', 'DNN2', 'classID==0')
    t_train.Project ('train_bkg', 'DNN2', 'classID==1')
    t_test.Project ('test_sig', 'DNN2', 'classID==0')
    t_test.Project ('test_bkg', 'DNN2', 'classID==1')

    h_test_sig.GetXaxis().SetTitle('DNN2 output')

    bw = h_test_sig.GetXaxis().GetBinWidth(1)
    h_test_sig.GetYaxis().SetTitle (f'\\text{{Entries / {bw:.2f}}}')

    h_test_sig.SetMaximum (h_test_sig.GetMaximum()*1.3)

    h_train_sig.Sumw2()
    h_train_bkg.Sumw2()
    h_test_sig.Draw()
    h_test_bkg.SetLineStyle (2)
    h_test_bkg.SetLineColor (2)
    h_test_bkg.Draw('SAME')
    h_train_sig.Draw('P,SAME')
    h_train_bkg.SetLineColor(2)
    h_train_bkg.SetLineStyle(2)
    h_train_bkg.SetMarkerColor(2)
    h_train_bkg.Draw('P,SAME')

    lab = Label (0.2, 0.78)
    lab.addline (r'(Z\rightarrow ee)(H\rightarrow 4\nu)')

    xleg = 0.43
    yleg = 0.9
    leg = ROOT.TLegend (xleg, yleg - 0.2, xleg+0.2, yleg)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.04)
    leg.AddEntry (h_train_sig, r'\text{Should add }\gamma\text{ (training)}', 'PL')
    leg.AddEntry (h_train_bkg, r'\text{Should not add }\gamma\text{ (training)}', 'PL')
    leg.AddEntry (h_test_sig, r'\text{Should add }\gamma\text{ (test)}', 'L')
    leg.AddEntry (h_test_bkg, r'\text{Should not add }\gamma\text{ (test)}', 'L')
    leg.Draw()

    objs.append ((h_train_sig, h_train_bkg, h_test_sig, h_test_bkg, lab, leg))
    return



def getfast (fastname, chan, fine):
    h_fast = globals()[f'f_fast_{fastname}_{chan}'].Get ('hist_recoilM')
    if fine:
        return h_fast
    h_fast_rebin1 = h_fast.Rebin (10, f'fast_{fastname}_{chan}_rebinned1')
    h_fast_rebin2 = ROOT.TH1F (f'fast_{fastname}_{chan}_rebinned2',
                               f'fast_{fastname}_{chan}_rebinned2',
                               80, 0, 200)
    for i in range(h_fast_rebin1.GetXaxis().GetNbins()):
        h_fast_rebin2.SetBinContent(i+1,
                                    h_fast_rebin1.GetBinContent(i+1))
        h_fast_rebin2.SetBinError(i+1,
                                    h_fast_rebin1.GetBinError(i+1))
    h_fast_rebin2.Scale (10.8/5)  # 5ab-1 -> 10.8ab-1
    return h_fast_rebin2
                                 
    
def recoilplot (chan, logy = False, fine = False):
    # ROOT.kYellow
    hdesc = [
        ['CHAN_zh_recoil',        'zh', ROOT.kGreen,   r'\text{Other }ZH'],
        ['CHAN_zz_recoil',        'zz', ROOT.kRed,     r'\text{}ZZ'],
        ['CHAN_ztautau_recoil',   '', ROOT.kAzure-4, r'Z/\gamma\rightarrow\tau\tau'],
        #['CHAN_ww_recoil',        'ww', ROOT.kBlue,    r'\text{}WW'],
        [None,                     'ww', ROOT.kBlue,    r'\text{}WW'],
        ['CHAN_hinv_CHAN_recoil', '', ROOT.kRed+3,   r'ZH \text{ Signal}'],
         ]

    ff = 'fine' if fine else ''
    if not logy:
        ROOT.TGaxis.SetMaxDigits(4)
    hstack = ROOT.THStack ('hstack', 'hstack')
    hsig = None
    leglist = []
    for pat, fastname, color, lab in hdesc:
        if pat:
            h_full = f_norm.Get (pat.replace ('CHAN', chan) + ff)
            h = h_full.Clone()
            if fastname:
                h_fast = getfast (fastname, chan, fine);
                h.Add (h_fast)
        else:
            h = getfast (fastname, chan, fine);
        objs.append (h)
        h.SetLineColor (color)
        h.SetFillColor (color)
        h.SetMarkerColor (color)
        h.SetMarkerSize (0)
        if lab.find ('Signal') >= 0:
            hsig = h
            if not logy:
                hsig.Scale (500)
                lab = lab + ' \\times 500'
        else:
            hstack.Add (h)
        leglist.append ((h, lab))

    leg = ROOT.TLegend (0.68, 0.73, 0.78, 0.93)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.035)
    for h, lab in reversed (leglist):
        leg.AddEntry (h, lab, 'LF')

    hstack.Draw()
    ROOT.gROOT.GetSelectedPad().SetLogy(logy)
    if logy:
        hstack.SetMaximum (50 * hstack.GetMaximum())
    else:
        hstack.SetMaximum (1.2 * hstack.GetMaximum())

    hstack.SetMinimum (0.1)
    bw = hstack.GetXaxis().GetBinWidth(1)
    hstack.GetXaxis().SetTitle (r'm_\text{recoil}\ \text{['+texGeV+']}')
    if fine:
        hstack.GetYaxis().SetTitle (f'\\text{{Entries / {bw:.2f} '+texGeV+'}}')
    else:
        hstack.GetYaxis().SetTitle (f'\\text{{Entries / {bw:.1f} '+texGeV+'}}')
    hstack.GetXaxis().SetRangeUser (0, 160)

    hstack.Draw('hist e2')
    hsig.SetFillStyle(3005)
    hsig.Draw ('H same')
    leg.Draw()

    lab = Label(0.2, 0.9, intlum = 10.8)
    if chan == 'ee':
        lab.addline (r'ee\ \text{channel}')
    elif chan == 'mm':
        lab.addline (r'\mu\mu\ \text{channel}')
    elif chan == 'qq':
        lab.addline (r'\text{Hadronic channel}')

    objs.append ((hstack, leg, lab, hsig))
    return
    


def ploteps_paper (nameroot, f, *args, **kw):
    return ploteps ('paper-plots', nameroot, f, *args, **kw)
def paperplots():
    ploteps_paper ('vispx', plotvispx, t_zz_qqqq)
    ploteps_paper ('mll-ee', mllplot, t_hinv_ee)
    ploteps_paper ('mll-mm', mllplot, t_hinv_mm)
    ploteps_paper ('mvis-qq', mvisplot, t_hinv_qq)
    ploteps_paper ('nn-angle', nn_angle_plot)
    ploteps_paper ('nn-p', nn_p_plot)
    ploteps_paper ('nn-theta', nn_theta_plot)
    ploteps_paper ('nn-clustE', nn_clustE_plot)
    ploteps_paper ('nn-gamE', nn_gamE_plot)
    ploteps_paper ('nn-out', nn_out_plot)
    ploteps_paper ('recoil-ee', recoilplot, 'ee')
    ploteps_paper ('recoil-mm', recoilplot, 'mm')
    ploteps_paper ('recoil-qq', recoilplot, 'qq')
    ploteps_paper ('recoil-ee-fine', recoilplot, 'ee', fine=True)
    ploteps_paper ('recoil-mm-fine', recoilplot, 'mm', fine=True)
    ploteps_paper ('recoil-qq-fine', recoilplot, 'qq', fine=True)
    ploteps_paper ('recoil-ee-logy', recoilplot, 'ee', True)
    ploteps_paper ('recoil-mm-logy', recoilplot, 'mm', True)
    ploteps_paper ('recoil-qq-logy', recoilplot, 'qq', True)
    ploteps_paper ('recoil-ee-logy-fine', recoilplot, 'ee', True, fine=True)
    ploteps_paper ('recoil-mm-logy-fine', recoilplot, 'mm', True, fine=True)
    ploteps_paper ('recoil-qq-logy-fine', recoilplot, 'qq', True, fine=True)
    #plotpdf_paper ('mvis-qq', mvisplot, t_hinv_qq)
    #plotpdf_paper ('recoil-ee', recoilplot2, f_hinv_ee)
    #plotpdf_paper ('recoil-mm', recoilplot2, f_hinv_mm)
    #plotpdf_paper ('recoil-qq', recoilplot2, f_hinv_qq)
    return


def dumpmc (t, g4=False):
    from PDG import pdgid_to_name
    dindex = [x.index for x in t._MCParticles_daughters]
    pindex = [x.index for x in t._MCParticles_parents]
    for ipart, p in enumerate (t.MCParticles):
        if p.generatorStatus == 0 and not g4: continue
        v = ROOT.TLorentzVector()
        v.SetXYZM (p.momentum.x, p.momentum.y, p.momentum.z, p.mass)
        dlist = dindex[p.daughters_begin:p.daughters_end]
        dstr = ' '.join ([str(x+1) for x in dlist])
        plist = pindex[p.parents_begin:p.parents_end]
        pstr = ' '.join ([str(x+1) for x in plist])
        print (f'{ipart+1:3} {pdgid_to_name(p.PDG):4} {p.generatorStatus} {v.Pt():6.1f} {v.Eta():5.2f} {v.Phi():5.2f} {v.M():5.1f} {v.Px():6.1f} {v.Py():6.1f} {v.Pz():6.1f} {v.E():6.1f} {pstr}>{dstr}')
    return


def dumppfo (t):
    from PDG import pdgid_to_name
    for ipart, p in enumerate (t.BoostedPandoraPFOs):
        v = ROOT.TLorentzVector()
        v.SetXYZM (p.momentum.x, p.momentum.y, p.momentum.z, p.mass)
        print (f'{ipart+1:3} {pdgid_to_name(p.type):4} {v.Pt():6.1f} {v.Eta():5.2f} {v.Phi():5.2f} {v.M():5.1f} {v.Px():6.1f} {v.Py():6.1f} {v.Pz():6.1f} {v.E():6.1f}')
    return


def execfile (f = os.path.join (mydir, 'fullsim-plots.py')):
    exec (open(f).read(), globals())
    return
