import sys
import os
import ROOT
import cppyy
from label import Label, texGeV
from draw_obj import draw_obj, zone, get_canvas
from printpdf import printpdf


mydir = os.path.dirname (sys.argv[0])


if 't_ele' not in globals():
    f_ele = ROOT.TFile ('lepeff-ele.root')
    t_ele = f_ele.events
    f_muo = ROOT.TFile ('lepeff-muo.root')
    t_muo = f_muo.events
    t = {'ele' : t_ele, 'muo' : t_muo}

    f_fast_ele = ROOT.TFile ('lepeff-fastsim-ele.root')
    t_fast_ele = f_fast_ele.events
    f_fast_muo = ROOT.TFile ('lepeff-fastsim-muo.root')
    t_fast_muo = f_fast_muo.events
    t_fast = {'ele' : t_fast_ele, 'muo' : t_fast_muo}

    f_cld_ele = ROOT.TFile ('lepeff-cld-ele.root')
    t_cld_ele = f_cld_ele.events
    f_cld_muo = ROOT.TFile ('lepeff-cld-muo.root')
    t_cld_muo = f_cld_muo.events
    t_cld = {'ele' : t_cld_ele, 'muo' : t_cld_muo}

    f_idea_ele = ROOT.TFile ('lepeff-idea-ele.root')
    t_idea_ele = f_idea_ele.events
    f_idea_muo = ROOT.TFile ('lepeff-idea-muo.root')
    t_idea_muo = f_idea_muo.events
    t_idea = {'ele' : t_idea_ele, 'muo' : t_idea_muo}

    f_test_ele = ROOT.TFile ('lepeff-test-ele.root')
    t_test_ele = f_test_ele.events
    f_test_muo = ROOT.TFile ('lepeff-test-muo.root')
    t_test_muo = f_test_muo.events
    t_test = {'ele' : t_test_ele, 'muo' : t_test_muo}

    f_test2_ele = ROOT.TFile ('lepeff-test-ele2.root')
    t_test2_ele = f_test2_ele.events
    f_test2_muo = ROOT.TFile ('lepeff-test-muo2.root')
    t_test2_muo = f_test2_muo.events
    t_test2 = {'ele' : t_test2_ele, 'muo' : t_test2_muo}


if not 'style_loaded' in globals():
    ROOT.gROOT.LoadMacro (os.path.join (mydir, 'atlas-macros/AtlasStyle.C'))
    ROOT.gROOT.LoadMacro (os.path.join (mydir, 'atlas-macros/AtlasUtils.C'))
    ROOT.gROOT.LoadMacro (os.path.join (mydir, 'atlas-macros/AtlasLabels.C'))
    ROOT.gROOT.ProcessLine ("SetAtlasStyle();")
    ROOT.gStyle.SetErrorX (0)
    style_loaded = True

objs = []


resplots = [
    [t, 'full', 2, 20, 'CLD full sim', '02'],
    [t_cld, 'cld', 4, 22, 'CLD Delphes'],
    [t_idea, 'idea', 6, 23, 'IDEA Delphes'],
    #[t_fast, 'fast', 7, 24, 'fast sim']
    #[t_test, 'test', 7, 24, 'fast sim'],
    [t_test2, 'test2', 8, 25, 'CLD Delphes/CALICE res']
    ]
vardesc = {'pt'    : ('p_{T}\\text{ ['+texGeV+']}', 20,  0,   80),
           'p'     : ('p\\text{ [GeV]}', 40,  0,   80),
           'theta' : ('\\theta',      32,  0,   3.2),
           'phi'   : ('\\phi',        16, -3.2, 3.2),
           'eta'   : ('\\eta',        30, -3.0, 3.0)}
flavdesc = {'ele' : 'electron', 'muo' : 'muon'}


#muo, res, pt
def makeres1 (events, nm, flav, var1, var2, which='', sel=''):
    hname = f'{flav}_res_{var1}{which}_{var2}'
    if var2 == 'pt' or var2 == 'p':
        nxbins, xmin, xmax = 20, 0, 80
    elif var2 == 'theta':
        nxbins, xmin, xmax = 16, 0, 3.2
    elif var2 == 'phi':
        nxbins, xmin, xmax = 16, -3.2, 3.2
    res2d = ROOT.TH2F (hname, hname, nxbins, xmin, xmax, 200, -0.1, 0.1)

    if var1 == 'res':
        yvar = f'mc{flav}_matched_{var1}{which}'
    elif var1 == 'res2':
        yvar = f'mc{flav}_matched_diff{which}/mc{flav}_matched_pt{which}/mc{flav}_matched_pt{which}'
    events.Project (hname, f'{yvar}:mc{flav}_matched_{var2}', sel)
    oa = ROOT.TObjArray()
    res2d.FitSlicesY (cppyy.nullptr, 0, -1, 0, "", oa)
    oa[2].SetMinimum (0)
    return oa[2].Clone (nm + oa[2].GetName())


def plotres_comparison1 (r, flav, var1, var2, which='', sel=''):
    which0 = which
    if len(r) < 6 or not which in r[5]:
        which = ''
    h = makeres1 (r[0][flav], f'{r[1]}_{flav}', flav, var1, var2, which=which, sel=sel)
    h.SetMarkerColor (r[2])
    h.SetLineColor (r[2])
    h.SetMarkerStyle(r[3])
    return h
def plotres_comparison (flav, var1, var2, max=0.05, which='', sel=''):
    rp = [x for x in resplots if x[1]!='test2' or flav=='ele']
    hists = [plotres_comparison1 (r, flav, var1, var2, which=which, sel=sel) for r in rp]
    hists[0].SetMaximum (max)
    hists[0].GetXaxis().SetTitle (r'\text{MC %s }%s' %
                                  (flavdesc[flav],
                                   vardesc[var2][0]))
    if var1 == 'res':
        hists[0].GetYaxis().SetTitle (r'\sigma(\Delta p_{T}\, / p_{T,\text{true}})')
    hists[0].GetYaxis().SetNdivisions (505)
    if var1 == 'res2':
        get_canvas()
        ROOT.gPad.SetLogx(1)
        ROOT.gPad.SetLogy(1)
        for h in hists:
            h.GetXaxis().SetLimits (9,100)
            h.SetMinimum (1e-5)
            h.SetMaximum (1)
    else:
        get_canvas()
        ROOT.gPad.SetLogx(0)
        ROOT.gPad.SetLogy(0)
    hists[0].Draw()
    for hh in hists[1:]:
        hh.Draw('same')
    objs.append (hists)
    leg = ROOT.TLegend (0.47, 0.7, 0.85, 0.9)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.04)
    for hh, r in zip (hists, rp):
        leg.AddEntry (hh, r[4], 'P')
    leg.Draw()
    objs.append (leg)
    lab = Label(0.2, 0.85)
    if flav == 'ele':
        lab.addline (r'(Z\rightarrow ee)(H\rightarrow 4\nu)')
    elif flav == 'muo':
        lab.addline (r'(Z\rightarrow \mu\mu)(H\rightarrow 4\nu)')
    objs.append (lab)
    return


def makeres_slice (r, events, nm, flav, which=''):
    if len(r) < 6 or which not in r[5]:
        which=''
    hname = f'{flav}_{nm}_slice'
    h = ROOT.TH1F (hname, hname, 50, -0.1, 0.1)
    events.Project (hname, f'mc{flav}_matched_res{which}',
                    f'mc{flav}_matched_pt>40&&mc{flav}_matched_pt<50')
    return h
def plotres_slice (flav, which=''):
    hists = [makeres_slice(r, r[0][flav], r[1], flav, which=which) for r in resplots]
    for h, r in zip (hists, resplots):
        h.SetLineColor (r[2])
    hists[1].SetLineStyle(2)
    hists[2].SetLineStyle(3)
    m = max([h.GetMaximum() for h in hists])
    hists[0].GetXaxis().SetTitle ('#Delta p_{T} / p_{T,true}')
    hists[0].SetMaximum(m*1.1)
    hists[0].Draw()
    for h in hists[1:]:
        h.Draw('same')
    objs.append (hists)
    leg = ROOT.TLegend (0.65, 0.7, 0.85, 0.9)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.04)
    for hh, r in zip (hists, resplots):
        leg.AddEntry (hh, r[4], 'L')
    leg.Draw()
    objs.append (leg)
    lab = Label(0.2, 0.85)
    if flav == 'ele':
        lab.addline ('(Z#rightarrow ee)(H#rightarrow 4#nu)')
    elif flav == 'muo':
        lab.addline ('(Z#rightarrow #mu#mu)(H#rightarrow 4#nu)')
    lab.addline ('40 GeV < p_{T} < 50 GeV')
    objs.append (lab)
    return


def makemll (events, nm, flav):
    hname = f'{flav}_{nm}_mll'
    h = ROOT.TH1F (hname, hname, 60, 0, 120)
    if flav == 'ele':
        var = 'ee_m'
    elif flav == 'muo':
        var = 'mm_m'
    events.Project (hname, var)
    return h
def plotmll (flav):
    hists = [makemll(r[0][flav], r[1], flav) for r in resplots]
    for h, r in zip (hists, resplots):
        h.SetLineColor (r[2])
    hists[1].SetLineStyle(2)
    hists[2].SetLineStyle(3)
    m = max([h.GetMaximum() for h in hists])
    hists[0].GetXaxis().SetTitle ('m(ll) [GeV]')
    hists[0].SetMaximum(m*1.1)
    hists[0].Draw()
    for h in hists[1:]:
        h.Draw('same')
    objs.append (hists)
    leg = ROOT.TLegend (0.25, 0.6, 0.45, 0.8)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.04)
    for hh, r in zip (hists, resplots):
        leg.AddEntry (hh, r[4], 'L')
    leg.Draw()
    objs.append (leg)
    lab = Label(0.2, 0.85)
    if flav == 'ele':
        lab.addline ('(Z#rightarrow ee)(H#rightarrow 4#nu)')
    elif flav == 'muo':
        lab.addline ('(Z#rightarrow #mu#mu)(H#rightarrow 4#nu)')
    objs.append (lab)
    return


def make_effplot (events, flav, var, which=''):
    name_all = f'{flav}_{var}_mcall'
    name_matched = f'{flav}_{var}_mcmatched'
    vtitle, nbins, xlo, xhi = vardesc[var]
    hall = ROOT.TH1F (name_all, name_all,
                      nbins, xlo, xhi)
    hmatched = ROOT.TH1F (name_matched, name_matched, 
                          nbins, xlo, xhi)
    events.Project (name_all, f'mc{flav}_{var}')
    events.Project (name_matched, f'mc{flav}_matched_{var}{which}')
    hall.Sumw2()
    hmatched.Sumw2()
    hmatched.Divide (hall)
    hmatched.GetXaxis().SetTitle (f'MC {flavdesc[flav]} {vtitle}')
    return hmatched
def ploteff_comparison1 (r, flav, var, which=''):
    if len(r) < 6 or not which in r[5]:
        which = ''
    h = make_effplot (r[0][flav], flav, var, which=which)
    h.SetMarkerColor (r[2])
    h.SetLineColor (r[2])
    h.SetMarkerStyle(r[3])
    return h
def ploteff_comparison (flav, var, which=''):
    hists = [ploteff_comparison1 (r, flav, var, which=which) for r in resplots]
    if var == 'phi':
        hists[0].SetMinimum(0.8)
        hists[0].SetMaximum(1.03)
    else:
        hists[0].SetMinimum(0)
    hists[0].GetYaxis().SetTitle (f'{flavdesc[flav].capitalize()} efficiency')
    hists[0].Draw()
    for hh in hists[1:]:
        hh.Draw('same')
    objs.append (hists)
    leg = ROOT.TLegend (0.6, 0.2, 0.9, 0.4)
    leg.SetBorderSize (0)
    leg.SetTextSize (0.04)
    for hh, r in zip (hists, resplots):
        leg.AddEntry (hh, r[4], 'P')
    leg.Draw()
    objs.append (leg)
    lab = Label(0.3, 0.3)
    if flav == 'ele':
        lab.addline ('(Z#rightarrow ee)(H#rightarrow 4#nu)')
    elif flav == 'muo':
        lab.addline ('(Z#rightarrow #mu#mu)(H#rightarrow 4#nu)')
    objs.append (lab)
    return


def plotpdf (nameroot, f, *args):
    if not os.path.exists ('lepeff-plots'):
        os.mkdir ('lepeff-plots')
    f (*args)
    c1 = get_canvas();
    c1.Print (f'lepeff-plots/{nameroot}.pdf','pdf,Portrait')
    return
def allplots():
     plotpdf ('ele_res_pt',    plotres_comparison, 'ele', 'res', 'pt')
     plotpdf ('ele_res_theta', plotres_comparison, 'ele', 'res', 'theta')
     plotpdf ('ele_res_phi',   plotres_comparison, 'ele', 'res', 'phi')
     plotpdf ('muo_res_pt',    plotres_comparison, 'muo', 'res', 'pt')
     plotpdf ('muo_res_theta', plotres_comparison, 'muo', 'res', 'theta')
     plotpdf ('muo_res_phi',   plotres_comparison, 'muo', 'res', 'phi')
     plotpdf ('ele_res_slice', plotres_slice, 'ele')
     plotpdf ('muo_res_slice', plotres_slice, 'muo')
     plotpdf ('m_ee', plotmll, 'ele')
     plotpdf ('m_mm', plotmll, 'muo')
     plotpdf ('ele_eff_pt',    ploteff_comparison, 'ele', 'pt')
     plotpdf ('ele_eff_theta', ploteff_comparison, 'ele', 'theta')
     plotpdf ('ele_eff_phi',   ploteff_comparison, 'ele', 'phi')
     plotpdf ('muo_eff_pt',    ploteff_comparison, 'muo', 'pt')
     plotpdf ('muo_eff_theta', ploteff_comparison, 'muo', 'theta')
     plotpdf ('muo_eff_phi',   ploteff_comparison, 'muo', 'phi')
     return


def ploteps (dirname, nameroot, f, *args, **kw):
    if not os.path.exists (dirname):
        os.mkdir (dirname)
    f (*args, **kw)
    c1 = get_canvas();
    c1.Print (f'{dirname}/{nameroot}.eps','eps,Portrait')
    return
def ploteps_paper (nameroot, f, *args, **kw):
    return ploteps ('paper-plots', nameroot, f, *args, **kw)
def paperplots():
    ploteps_paper ('ele_res_pt',    plotres_comparison, 'ele', 'res', 'pt', which='0')
    ploteps_paper ('muo_res_pt',    plotres_comparison, 'muo', 'res', 'pt', which='0')
    return


def execfile (f = os.path.join (mydir, 'lepeff-comparison-plots.py')):
    exec (open(f).read(), globals())
    return
