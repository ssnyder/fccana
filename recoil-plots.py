import ROOT
import cppyy
from draw_obj import draw_obj, zone
from printpdf import printpdf

if not 'style_loaded' in globals():
    ROOT.gROOT.LoadMacro ('atlas-macros/AtlasStyle.C')
    ROOT.gROOT.LoadMacro ('atlas-macros/AtlasUtils.C')
    ROOT.gROOT.LoadMacro ('atlas-macros/AtlasLabels.C')
    ROOT.gROOT.ProcessLine ("SetAtlasStyle();")
    ROOT.gStyle.SetErrorX (0)
    style_loaded = True


def truesqrts0 (t, label=None):
    hname = f'{t.title}_truesqrts0'
    h = ROOT.TH1F (hname, hname, 100, 238, 242)
    t.Project (hname, 'truesqrts0')
    h.GetXaxis().SetTitle ('#sqrt{s} from incoming particles')
    draw_obj (h)
    if label:
        ROOT.myText (0.85, 0.85, 1, label)
    printpdf ('plots/recoil-' + hname)
    return h


def truesqrts (t, label=None):
    hname = f'{t.title}_truesqrts'
    h = ROOT.TH1F (hname, hname, 100, 150, 250)
    t.Project (hname, 'truesqrts')
    h.GetXaxis().SetTitle ('#sqrt{s} of ZH system')
    draw_obj (h, 'logy')
    if label:
        ROOT.myText (0.25, 0.85, 1, label)
    printpdf ('plots/recoil-' + hname)
    return h


def mcisrgamz (t, n, label=None):
    hname = f'{t.title}_mcisrgamz{n}'
    if n==1:
        h = ROOT.TH1F (hname, hname, 60, 0, 60)
    else:
        h = ROOT.TH1F (hname, hname, 60, -60, 0)
    t.Project (hname, f'mcisrgamz{n}')
    h.GetXaxis().SetTitle ('MC collinear photon P_{z}')
    draw_obj (h, 'logy')
    if label:
        ROOT.myText (0.85, 0.85, 1, label)
    printpdf ('plots/recoil-' + hname)
    return h


def mcx (t, n, label=None):
    hname = f'{t.title}_x{n}'
    h = ROOT.TH1F (hname, hname, 80, 0.6, 1)
    t.Project (hname, f'x{n}')
    h.GetXaxis().SetTitle (f'x{n}')
    draw_obj (h, 'logy')
    if label:
        ROOT.myText (0.85, 0.85, 1, label)
    printpdf ('plots/recoil-' + hname)
    return h


def beta (t, label=None):
    hname = f'{t.title}_beta'
    h = ROOT.TH1F (hname, hname, 60, -0.3, 0.3)
    t.Project (hname, 'beta')
    h.GetXaxis().SetTitle ('#beta of ZH system')
    draw_obj (h, 'logy')
    if label:
        ROOT.myText (0.85, 0.85, 1, label)
    printpdf ('plots/recoil-' + hname)
    return h


def recoilplot (t, var, sel='', lo=100, hi=250, label=None, opt='', suff=''):
    hname = f'{t.title}_{var}{suff}'
    h = ROOT.TH1F (hname, hname, 100, lo, hi)
    t.Project (hname, var, sel)
    h.GetXaxis().SetTitle ('Recoil mass')
    draw_obj (h, opt)
    if label:
        ROOT.myText (0.45, 0.85, 1, label)
    printpdf ('plots/recoil-' + hname)
    return


def allplots():
    truesqrts0 (tmmnew, '#mu#mu')
    truesqrts0 (tmmold, '#mu#mu')
    truesqrts0 (teenew, 'ee')
    truesqrts0 (teeold, 'ee')
    truesqrts0 (tjjnew, 'jj')
    truesqrts0 (tjjold, 'jj')

    mcisrgamz (tmmnew, 1, '#mu#mu')
    mcisrgamz (tmmnew, 2, '#mu#mu')
    mcisrgamz (tmmold, 1, '#mu#mu')
    mcisrgamz (tmmold, 2, '#mu#mu')
    mcisrgamz (teenew, 1, 'ee')
    mcisrgamz (teenew, 2, 'ee')
    mcisrgamz (teeold, 1, 'ee')
    mcisrgamz (teeold, 2, 'ee')
    mcisrgamz (tjjnew, 1, 'jj')
    mcisrgamz (tjjnew, 2, 'jj')
    mcisrgamz (tjjold, 1, 'jj')
    mcisrgamz (tjjold, 2, 'jj')

    mcx (tmmnew, 1, '#mu#mu')
    mcx (tmmnew, 2, '#mu#mu')
    mcx (tmmold, 1, '#mu#mu')
    mcx (tmmold, 2, '#mu#mu')
    mcx (teenew, 1, 'ee')
    mcx (teenew, 2, 'ee')
    mcx (teeold, 1, 'ee')
    mcx (teeold, 2, 'ee')
    mcx (tjjnew, 1, 'jj')
    mcx (tjjnew, 2, 'jj')
    mcx (tjjold, 1, 'jj')
    mcx (tjjold, 2, 'jj')

    truesqrts (tmmnew, '#mu#mu')
    truesqrts (tmmold, '#mu#mu')
    truesqrts (teenew, 'ee')
    truesqrts (teeold, 'ee')
    truesqrts (tjjnew, 'jj')
    truesqrts (tjjold, 'jj')

    beta (tmmnew, '#mu#mu')
    beta (tmmold, '#mu#mu')
    beta (teenew, 'ee')
    beta (teeold, 'ee')
    beta (tjjnew, 'jj')
    beta (tjjold, 'jj')

    recoilplot (tmmnew, 'mch_m', label='#mu#mu (MC)')
    recoilplot (tmmold, 'mch_m', label='#mu#mu (MC)')
    recoilplot (teenew, 'mch_m', label='ee (MC)')
    recoilplot (teeold, 'mch_m', label='ee (MC)')

    recoilplot (tmmnew, 'mch_m', '@mcgam.size()==0', label='#mu#mu (MC), no mcgam', suff='_nomcgam')
    recoilplot (tmmold, 'mch_m', '@mcgam.size()==0', label='#mu#mu (MC), no mcgam', suff='_nomcgam')
    recoilplot (teenew, 'mch_m', '@mcgam.size()==0', label='ee (MC), no mcgam', suff='_nomcgam')
    recoilplot (teeold, 'mch_m', '@mcgam.size()==0', label='ee (MC), no mcgam', suff='_nomcgam')

    recoilplot (tmmnew, 'mcht_m', '@mcgam.size()==0', label='#mu#mu (MC), no mcgam, HZ #sqrt{s}', suff='_trues')
    recoilplot (tmmold, 'mcht_m', '@mcgam.size()==0', label='#mu#mu (MC), no mcgam, HZ #sqrt{s}', suff='_trues')
    recoilplot (teenew, 'mcht_m', '@mcgam.size()==0', label='ee (MC), no mcgam, HZ #sqrt{s}', suff='_trues')
    recoilplot (teeold, 'mcht_m', '@mcgam.size()==0', label='ee (MC), no mcgam, HZ #sqrt{s}', suff='_trues')

    recoilplot (tmmnew, 'h_m', label='#mu#mu', suff='neg', lo=-200, opt='logy')
    recoilplot (tmmold, 'h_m', label='#mu#mu', suff='neg', lo=-200, opt='logy')
    recoilplot (teenew, 'h_m', label='ee', suff='neg', lo=-200, opt='logy')
    recoilplot (teeold, 'h_m', label='ee', suff='neg', lo=-200, opt='logy')
    recoilplot (tjjnew, 'h_m', label='jj', suff='neg', lo=-200, opt='logy')
    recoilplot (tjjold, 'h_m', label='jj', suff='neg', lo=-200, opt='logy')

    recoilplot (tmmnew, 'h_m', label='#mu#mu')
    recoilplot (tmmold, 'h_m', label='#mu#mu')
    recoilplot (teenew, 'h_m', label='ee')
    recoilplot (teeold, 'h_m', label='ee')
    recoilplot (tjjnew, 'h_m', label='jj')
    recoilplot (tjjold, 'h_m', label='jj')

    recoilplot (tmmnew, 'ht_m', label='#mu#mu, true HZ #sqrt{s}')
    recoilplot (tmmold, 'ht_m', label='#mu#mu, true HZ #sqrt{s}')
    recoilplot (teenew, 'ht_m', label='ee, true HZ #sqrt{s}')
    recoilplot (teeold, 'ht_m', label='ee, true HZ #sqrt{s}')
    recoilplot (tjjnew, 'ht_m', label='jj, true HZ #sqrt{s}')
    recoilplot (tjjold, 'ht_m', label='jj, true HZ #sqrt{s}')


    return

    
    
    


def recoilplot2 (t):
    zone (2, 2)
    recoilplot1 (t, 'h_m', label = 'Recoil mass')
    recoilplot1 (t, 'mch_m', label = 'Recoil mass (MC)')
    recoilplot1 (t, 'ht_m', label = 'Recoil mass, true #sqrt{s}')
    recoilplot1 (t, 'mch_m', label = 'Recoil mass (MC), true #sqrt{s}')
    return


def recoilplot3 (t):
    zone (2, 2)
    recoilplot1 (t, 'h_m', label = 'Recoil mass')
    recoilplot1 (t, 'mch_m', label = 'Recoil mass (MC)')
    recoilplot1 (t, 'h_m', '@mcgam.size()==0', label = 'Recoil mass, no mcgam')
    recoilplot1 (t, 'mch_m', '@mcgam.size()==0', label = 'Recoil mass (MC), nomcgam')
    return


def recoilplot4 (t):
    zone (2, 2)
    recoilplot1 (t, 'h_m', label = 'Recoil mass', opt='logy', lo=-200)
    recoilplot1 (t, 'mch_m', label = 'Recoil mass (MC)', opt='logy', lo=-200)
    return


def samp (name, fname):
    if not name in globals():
        f = ROOT.TFile(fname)
        globals()[name] = f
        t = f.events
        globals()['t'+name] = t
        t.title = name
    return
samp ('eenew', 'recoilee-new.root')
samp ('eeold', 'recoilee-old.root')
samp ('mmnew', 'recoilmm-new.root')
samp ('mmold', 'recoilmm-old.root')
samp ('jjnew', 'recoiljj-new.root')
samp ('jjold', 'recoiljj-old.root')

#recoilplots (tmmnew)

def execfile (f = 'recoil-plots.py'):
    exec (open(f).read(), globals())
    return
