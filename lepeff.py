#fccanalysis run ../lepeff.py --output lepeff-ele.root --files-list   ../../data/reco-cld/hinvee.2001_20000/hinvee.2001_20000_*.rec_edm4hep.root 2>&1|tee log-lepeff-ele
#fccanalysis run ../lepeff.py --output lepeff-muo.root --files-list   ../../data/reco-cld/hinvmm.2003_20000/hinvmm.2003_20000_*.rec_edm4hep.root 2>&1 | tee log-lepeff-muo

#fccanalysis run lepeff.py --output lepeff-test.root --files-list   ../data/reco-cld/hinvmm.1002_500/hinvmm.1002_500*.rec_edm4hep.root 2>&1|tee log-lepeff-test
#fccanalysis run lepeff.py --output lepeff-oldele.root --files-list   ../data/reco-cld/hinvee.2001_20000-save/hinvee.2001_20000_0?.rec.root ../data/reco-cld/hinvee.2001_20000-save/hinvee.2001_20000_1[012346789].rec.root ../data/reco-cld/hinvee.2001_20000-save/hinvee.2001_20000_20.rec.root 2>&1| tee log-lepeff-oldele

#fccanalysis run lepeff.py --output lepeff-oldmuo.root --files-list ../data/reco-cld/hinvmm.2003_20000-save/*.rec.root 2>&1|tee log-lepeff-oldmuo



import sys
import os
import ROOT

mydir = os.path.dirname (sys.argv[2])
code1 = open (os.path.join (mydir, 'lepeff.cc')).read()
ROOT.gInterpreter.Declare(code1)
code2 = open (os.path.join (mydir, 'bremrec.cc')).read()
ROOT.gInterpreter.Declare(code2)
ROOT.DNN2Reader.setWeightsFile (os.path.join (mydir, 'TMVA_DNN2.weights.xml'))


class RDFanalysis:
    def analysers (df):
        df2 = (
            df
            .Define('eventNumber', 'EventHeader[0].eventNumber')

            .Alias("TightSelectedPandoraPFOsIndex", "TightSelectedPandoraPFOs_objIdx.index")
            .Define("TightSelectedPandoraPFOs", "ReconstructedParticle::get (TightSelectedPandoraPFOsIndex, PandoraPFOs)")
            .Define("rp1", "ReconstructedParticle::sel_pt(5)(TightSelectedPandoraPFOs)")
            .Define("rp2", "ReconstructedParticle::sel_eta(4)(rp1)")
            .Define("mcp1", "MCParticle::sel_genStatus(1)(MCParticles)")
            .Define("mcp2", "MCParticle::sel_pt(5)(mcp1)")

            .Define('gam', 'ReconstructedParticle::sel_absType(22)(TightSelectedPandoraPFOs)')
            .Define('gam_pt', 'ReconstructedParticle::get_pt(gam)')
            .Define('gam_p', 'ReconstructedParticle::get_p(gam)')
            .Define('gam_theta', 'ReconstructedParticle::get_theta(gam)')
            .Define('gam_phi', 'ReconstructedParticle::get_phi(gam)')
            .Define("gam_clustE", "get_rp_clustE(gam, PandoraClusters)")
            .Define("gam_clustEerror", "get_rp_clustEerror(gam, PandoraClusters)")
            .Define("gam_clustFasterr", "get_fastEerror(gam_clustE)")
            .Define("gam_totE", "get_totE(gam)")

            .Define("ele0", "sel_absType(11)(rp2)")
            .Define("ele0_pt", "ReconstructedParticle::get_pt(ele0)")
            .Define("ele0_p", "ReconstructedParticle::get_p(ele0)")
            .Define("ele0_eta", "ReconstructedParticle::get_eta(ele0)")
            .Define("ele0_theta", "ReconstructedParticle::get_theta(ele0)")
            .Define("ele0_phi", "ReconstructedParticle::get_phi(ele0)")
            .Define("ele0_clustE", "get_rp_clustE(ele0, PandoraClusters)")
            .Define("ele0_clustEerror", "get_rp_clustEerror(ele0, PandoraClusters)")
            .Define("ele0_clustFasterr", "get_fastEerror(ele0_clustE)")
            .Define('gamele_match', 'photonLeptonMatch(gam, ele0, PandoraClusters)')
            .Define('gamele_angle', 'get_match_angle(gamele_match)')
            .Define('gamele_dist', 'get_match_dist(gamele_match)')
            .Define('gamele_lepndx', 'get_match_lepndx(gamele_match)')
            .Define('gamele_gamndx', 'get_match_gamndx(gamele_match)')
            .Define('gamele_DNN2', 'get_match_DNN2(gamele_match)')
            .Define('ele', 'addfsr(ele0,gam,gamele_match,0.05)')
            .Define('ele_gamE', 'gamE(ele0,gam,gamele_match,0.05)')
            .Define("ele_pt", "ReconstructedParticle::get_pt(ele)")
            .Define("ele_p", "ReconstructedParticle::get_p(ele)")
            .Define("ele_eta", "ReconstructedParticle::get_eta(ele)")
            .Define("ele_theta", "ReconstructedParticle::get_theta(ele)")
            .Define("ele_phi", "ReconstructedParticle::get_phi(ele)")
            .Define('ele2', 'addfsr_DNN2(ele0,gam,gamele_match,0.05,0.5)')
            .Define("ele2_pt", "ReconstructedParticle::get_pt(ele2)")
            .Define("ele2_p", "ReconstructedParticle::get_p(ele2)")
            .Define("ele2_eta", "ReconstructedParticle::get_eta(ele2)")
            .Define("ele2_theta", "ReconstructedParticle::get_theta(ele2)")
            .Define("ele2_phi", "ReconstructedParticle::get_phi(ele2)")

            .Define("ele_tsindex", "get_rp_tsindex(ele0, SiTracks_Refitted, _PandoraPFOs_tracks)")
            .Define("ele_trackmom", "get_trackmom(ele_tsindex, _SiTracks_Refitted_trackStates)")
            .Define("ele_track_p", "get_tlv_p(ele_trackmom)")
            .Define("ele_track_pt", "get_tlv_pt(ele_trackmom)")
            .Define("ele_track_theta", "get_tlv_theta(ele_trackmom)")
            .Define("ele_track_sigp", "get_trackSigP(ele_tsindex, ele_trackmom, _SiTracks_Refitted_trackStates)")

            .Define("mcele", "MCParticle::sel_pdgID(11,true)(mcp2)")
            .Define("mcele_pt", "MCParticle::get_pt(mcele)")
            .Define("mcele_p", "MCParticle::get_p(mcele)")
            .Define("mcele_eta", "MCParticle::get_eta(mcele)")
            .Define("mcele_theta", "MCParticle::get_theta(mcele)")
            .Define("mcele_phi", "MCParticle::get_phi(mcele)")
            .Define("mcele_matchrel", "match(mcele, ele)")
            .Define("mcele_matchrel_mcndx", "get_matchrel_mcndx(mcele_matchrel)")
            .Define("mcele_matchrel_rpndx", "get_matchrel_rpndx(mcele_matchrel)")
            .Define("mcele_matched", "sel_matched(mcele, mcele_matchrel)")
            .Define("mcele_matched_pt", "MCParticle::get_pt(mcele_matched)")
            .Define("mcele_matched_p", "MCParticle::get_p(mcele_matched)")
            .Define("mcele_matched_eta", "MCParticle::get_eta(mcele_matched)")
            .Define("mcele_matched_theta", "MCParticle::get_theta(mcele_matched)")
            .Define("mcele_matched_phi", "MCParticle::get_phi(mcele_matched)")
            .Define("mcele_matched_diff", "calc_diff(mcele_matchrel, mcele, ele)")
            .Define("mcele_matched_res", "calc_res(mcele_matchrel, mcele, ele)")
            .Define("mcele_matched_ires", "calc_ires(mcele_matchrel, mcele, ele)")
            .Define("mcele_matched_diff0", "calc_diff(mcele_matchrel, mcele, ele0)")
            .Define("mcele_matched_res0", "calc_res(mcele_matchrel, mcele, ele0)")
            .Define("mcele_matched_clustRes0", "calc_clustRes(mcele_matchrel, mcele, ele0_clustE)")
            .Define("mcele_matched_trackRes0", "calc_clustRes(mcele_matchrel, mcele, ele_track_p)")
            .Define("mcele_matched_ires0", "calc_ires(mcele_matchrel, mcele, ele0)")
            .Define("mcele_matched_diff2", "calc_diff(mcele_matchrel, mcele, ele2)")
            .Define("mcele_matched_res2", "calc_res(mcele_matchrel, mcele, ele2)")
            .Define("mcele_matched_ires2", "calc_ires(mcele_matchrel, mcele, ele2)")
            .Define("mcele_unmatched", "sel_unmatched(mcele, mcele_matchrel)")
            .Define("mcele_unmatched_pt", "MCParticle::get_pt(mcele_unmatched)")
            .Define("mcele_unmatched_p", "MCParticle::get_p(mcele_unmatched)")
            .Define("mcele_unmatched_eta", "MCParticle::get_eta(mcele_unmatched)")
            .Define("mcele_unmatched_theta", "MCParticle::get_theta(mcele_unmatched)")
            .Define("mcele_unmatched_phi", "MCParticle::get_phi(mcele_unmatched)")

            .Define('ee0', 'resonanceBuilder(91)(ele0)')
            .Define('ee2', 'resonanceBuilder(91)(ele2)')
            .Define('ee', 'resonanceBuilder(91)(ele)')
            .Define('mcee', 'resonanceBuilder(91)(mcele)')
            .Define('ee0_m', 'ReconstructedParticle::get_mass(ee0)')
            .Define('ee2_m', 'ReconstructedParticle::get_mass(ee2)')
            .Define('ee_m', 'ReconstructedParticle::get_mass(ee)')
            .Define('mcee_m', 'MCParticle::get_mass(mcee)')

            .Define('elegoodfsr', 'selgoodbadfsr(mcele_matchrel, gamele_match, ele0, gam, mcele, 0.05, true)')
            .Define('elegoodfsr_angle', 'get_match_angle(elegoodfsr)')
            .Define('elegoodfsr_dist', 'get_match_dist(elegoodfsr)')
            .Define('elegoodfsr_pt', 'get_match_pt(elegoodfsr, ele0)')
            .Define('elegoodfsr_dratio', 'get_match_dratio(elegoodfsr, ele0)')
            .Define('elegoodfsr_deltaE', 'get_match_deltaE(elegoodfsr)')

            .Define('elebadfsr', 'selgoodbadfsr(mcele_matchrel, gamele_match, ele0, gam, mcele, 0.05, false)')
            .Define('elebadfsr_angle', 'get_match_angle(elebadfsr)')
            .Define('elebadfsr_dist', 'get_match_dist(elebadfsr)')
            .Define('elebadfsr_pt', 'get_match_pt(elebadfsr, ele0)')
            .Define('elebadfsr_dratio', 'get_match_dratio(elebadfsr, ele0)')
            .Define('elebadfsr_deltaE', 'get_match_deltaE(elebadfsr)')


            .Define("muo0", "sel_absType(13)(rp2)")
            .Define("muo0_pt", "ReconstructedParticle::get_pt(muo0)")
            .Define("muo0_p", "ReconstructedParticle::get_p(muo0)")
            .Define("muo0_eta", "ReconstructedParticle::get_eta(muo0)")
            .Define("muo0_theta", "ReconstructedParticle::get_theta(muo0)")
            .Define("muo0_phi", "ReconstructedParticle::get_phi(muo0)")
            .Define('gammuo_match', 'photonLeptonMatch(gam, muo0, PandoraClusters)')
            .Define('gammuo_angle', 'get_match_angle(gammuo_match)')
            .Define('gammuo_dist', 'get_match_dist(gammuo_match)')
            .Define('gammuo_lepndx', 'get_match_lepndx(gammuo_match)')
            .Define('gammuo_gamndx', 'get_match_gamndx(gammuo_match)')
            .Define('gammuo_DNN2', 'get_match_DNN2(gammuo_match)')
            .Define('muo', 'addfsr(muo0,gam,gammuo_match,0.05)')
            .Define('muo_gamE', 'gamE(muo0,gam,gammuo_match,0.05)')
            .Define("muo_pt", "ReconstructedParticle::get_pt(muo)")
            .Define("muo_p", "ReconstructedParticle::get_p(muo)")
            .Define("muo_eta", "ReconstructedParticle::get_eta(muo)")
            .Define("muo_theta", "ReconstructedParticle::get_theta(muo)")
            .Define("muo_phi", "ReconstructedParticle::get_phi(muo)")
            .Define('muo2', 'addfsr_DNN2(muo0,gam,gammuo_match,0.05,0.5)')
            .Define("muo2_pt", "ReconstructedParticle::get_pt(muo2)")
            .Define("muo2_p", "ReconstructedParticle::get_p(muo2)")
            .Define("muo2_eta", "ReconstructedParticle::get_eta(muo2)")
            .Define("muo2_theta", "ReconstructedParticle::get_theta(muo2)")
            .Define("muo2_phi", "ReconstructedParticle::get_phi(muo2)")

            .Define("muo_tsindex", "get_rp_tsindex(muo0, SiTracks_Refitted, _PandoraPFOs_tracks)")
            .Define("muo_trackmom", "get_trackmom(muo_tsindex, _SiTracks_Refitted_trackStates)")
            .Define("muo_track_p", "get_tlv_p(muo_trackmom)")
            .Define("muo_track_pt", "get_tlv_pt(muo_trackmom)")
            .Define("muo_track_theta", "get_tlv_theta(muo_trackmom)")
            .Define("muo_track_sigp", "get_trackSigP(muo_tsindex, muo_trackmom, _SiTracks_Refitted_trackStates)")

            .Define("mcmuo", "MCParticle::sel_pdgID(13,true)(mcp2)")
            .Define("mcmuo_pt", "MCParticle::get_pt(mcmuo)")
            .Define("mcmuo_p", "MCParticle::get_p(mcmuo)")
            .Define("mcmuo_eta", "MCParticle::get_eta(mcmuo)")
            .Define("mcmuo_theta", "MCParticle::get_theta(mcmuo)")
            .Define("mcmuo_phi", "MCParticle::get_phi(mcmuo)")
            .Define("mcmuo_matchrel", "match(mcmuo, muo)")
            .Define("mcmuo_matched", "sel_matched(mcmuo, mcmuo_matchrel)")
            .Define("mcmuo_matched_pt", "MCParticle::get_pt(mcmuo_matched)")
            .Define("mcmuo_matched_p", "MCParticle::get_p(mcmuo_matched)")
            .Define("mcmuo_matched_eta", "MCParticle::get_eta(mcmuo_matched)")
            .Define("mcmuo_matched_theta", "MCParticle::get_theta(mcmuo_matched)")
            .Define("mcmuo_matched_phi", "MCParticle::get_phi(mcmuo_matched)")
            .Define("mcmuo_matched_diff", "calc_diff(mcmuo_matchrel, mcmuo, muo)")
            .Define("mcmuo_matched_res", "calc_res(mcmuo_matchrel, mcmuo, muo)")
            .Define("mcmuo_matched_ires", "calc_ires(mcmuo_matchrel, mcmuo, muo)")
            .Define("mcmuo_matched_diff0", "calc_diff(mcmuo_matchrel, mcmuo, muo0)")
            .Define("mcmuo_matched_res0", "calc_res(mcmuo_matchrel, mcmuo, muo0)")
            .Define("mcmuo_matched_trackRes0", "calc_clustRes(mcmuo_matchrel, mcmuo, muo_track_p)")
            .Define("mcmuo_matched_ires0", "calc_ires(mcmuo_matchrel, mcmuo, muo0)")
            .Define("mcmuo_matched_diff2", "calc_diff(mcmuo_matchrel, mcmuo, muo2)")
            .Define("mcmuo_matched_res2", "calc_res(mcmuo_matchrel, mcmuo, muo2)")
            .Define("mcmuo_matched_ires2", "calc_ires(mcmuo_matchrel, mcmuo, muo2)")
            .Define("mcmuo_unmatched", "sel_unmatched(mcmuo, mcmuo_matchrel)")
            .Define("mcmuo_unmatched_pt", "MCParticle::get_pt(mcmuo_unmatched)")
            .Define("mcmuo_unmatched_p", "MCParticle::get_p(mcmuo_unmatched)")
            .Define("mcmuo_unmatched_eta", "MCParticle::get_eta(mcmuo_unmatched)")
            .Define("mcmuo_unmatched_theta", "MCParticle::get_theta(mcmuo_unmatched)")
            .Define("mcmuo_unmatched_phi", "MCParticle::get_phi(mcmuo_unmatched)")

            .Define('mm0', 'resonanceBuilder(91)(muo0)')
            .Define('mm', 'resonanceBuilder(91)(muo)')
            .Define('mcmm', 'resonanceBuilder(91)(mcmuo)')
            .Define('mm0_m', 'ReconstructedParticle::get_mass(mm0)')
            .Define('mm_m', 'ReconstructedParticle::get_mass(mm)')
            .Define('mcmm_m', 'MCParticle::get_mass(mcmm)')

            )
        return df2


    def output():
        return [
            'eventNumber',

            'gam',
            'gam_pt',
            'gam_p',
            'gam_theta',
            'gam_phi',
            'gam_clustE',
            'gam_clustEerror',
            'gam_clustFasterr',
            'gam_totE',

            'ele0',
            'ele0_pt',
            'ele0_p',
            'ele0_eta',
            'ele0_theta',
            'ele0_phi',
            'ele0_clustE',
            'ele0_clustEerror',
            'ele0_clustFasterr',
            'gamele_angle',
            'gamele_DNN2',
            'gamele_dist',
            'gamele_lepndx',
            'gamele_gamndx',
            'ele',
            'ele_gamE',
            'ele_pt',
            'ele_p',
            'ele_eta',
            'ele_theta',
            'ele_phi',
            'ele2',
            'ele2_pt',
            'ele2_p',
            'ele2_eta',
            'ele2_theta',
            'ele2_phi',

            #'ele_trackmom',
            'ele_track_p',
            'ele_track_pt',
            'ele_track_theta',
            'ele_track_sigp',

            #'elegoodfsr',
            'elegoodfsr_angle',
            'elegoodfsr_dist',
            'elegoodfsr_pt',
            'elegoodfsr_dratio',
            'elegoodfsr_deltaE',
            #'elebadfsr',
            'elebadfsr_angle',
            'elebadfsr_dist',
            'elebadfsr_pt',
            'elebadfsr_dratio',
            'elebadfsr_deltaE',

            'mcele',
            'mcele_pt',
            'mcele_p',
            'mcele_eta',
            'mcele_theta',
            'mcele_phi',
            'mcele_matchrel_mcndx',
            'mcele_matchrel_rpndx',
            'mcele_matched',
            'mcele_matched_pt',
            'mcele_matched_p',
            'mcele_matched_eta',
            'mcele_matched_theta',
            'mcele_matched_phi',
            'mcele_matched_diff',
            'mcele_matched_res',
            'mcele_matched_ires',
            'mcele_matched_diff0',
            'mcele_matched_res0',
            'mcele_matched_clustRes0',
            'mcele_matched_trackRes0',
            'mcele_matched_ires0',
            'mcele_matched_diff2',
            'mcele_matched_res2',
            'mcele_matched_ires2',
            'mcele_unmatched',
            'mcele_unmatched_pt',
            'mcele_unmatched_p',
            'mcele_unmatched_eta',
            'mcele_unmatched_theta',
            'mcele_unmatched_phi',

            'ee0_m',
            'ee2_m',
            'ee_m',
            'mcee_m',

            'muo0',
            'muo0_pt',
            'muo0_p',
            'muo0_eta',
            'muo0_theta',
            'muo0_phi',
            'gammuo_angle',
            'gammuo_dist',
            'gammuo_lepndx',
            'gammuo_gamndx',
            'gammuo_DNN2',
            'muo',
            'muo_gamE',
            'muo_pt',
            'muo_p',
            'muo_eta',
            'muo_theta',
            'muo_phi',
            'muo2',
            'muo2_pt',
            'muo2_p',
            'muo2_eta',
            'muo2_theta',
            'muo2_phi',

            #'muo_trackmom',
            'muo_track_p',
            'muo_track_pt',
            'muo_track_theta',
            'muo_track_sigp',

            'mcmuo',
            'mcmuo_pt',
            'mcmuo_p',
            'mcmuo_eta',
            'mcmuo_theta',
            'mcmuo_phi',
            'mcmuo_matched',
            'mcmuo_matched_pt',
            'mcmuo_matched_p',
            'mcmuo_matched_eta',
            'mcmuo_matched_theta',
            'mcmuo_matched_phi',
            'mcmuo_matched_diff',
            'mcmuo_matched_res',
            'mcmuo_matched_ires',
            'mcmuo_matched_diff0',
            'mcmuo_matched_res0',
            'mcmuo_matched_trackRes0',
            'mcmuo_matched_ires0',
            'mcmuo_matched_diff2',
            'mcmuo_matched_res2',
            'mcmuo_matched_ires2',
            'mcmuo_unmatched',
            'mcmuo_unmatched_pt',
            'mcmuo_unmatched_p',
            'mcmuo_unmatched_eta',
            'mcmuo_unmatched_theta',
            'mcmuo_unmatched_phi',

            'mm0_m',
            'mm_m',
            'mcmm_m',
            ]


