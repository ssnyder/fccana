# in tuple
#fccanalysis run ../hinv-fastsim-ana.py --output hinv-fast-ww.root --files-list /eos/experiment/fcc/ee/generation/DelphesEvents/winter2023/IDEA/p8_ee_WW_ecm240/events_130445619.root  2>&1|tee log-hinv-fast-ww
#fccanalysis run ../hinv-fastsim-ana.py --output hinv-fast-ww.root --files-list /eos/experiment/fcc/ee/generation/DelphesEvents/winter2023/IDEA/p8_ee_WW_ecm240/events_*.root  2>&1|tee log-hinv-fast-ww
#fccanalysis run ../hinv-fastsim-ana.py --output hinv-fast-ww.root --files-list /eos/experiment/fcc/ee/generation/DelphesEvents/winter2023/IDEA/p8_ee_WW_ecm240/events_1[0-5]*.root  2>&1|tee log-hinv-fast-ww


import sys
import os
import ROOT
ROOT.gROOT.SetBatch (True)

mydir = os.path.dirname (sys.argv[2])
code1 = open (os.path.join (mydir, 'recoil.cc')).read()
ROOT.gInterpreter.Declare(code1)
code2 = open (os.path.join (mydir, 'bremrec.cc')).read()
ROOT.gInterpreter.Declare(code2)
ROOT.DNN2Reader.setWeightsFile (os.path.join (mydir, 'TMVA_DNN2.weights.xml'))

MZ = 91.19

def writehists (filename, hists):
    if not hists: return
    fout = ROOT.TFile.Open (filename, 'UPDATE')
    for h in hists:
        h.Write()
    fout.Close()

    pdfname = os.path.splitext (filename)[0] + '.pdf'
    c = ROOT.TCanvas('c')
    c.Print (pdfname + '[')
    for h in hists:
        h.Draw()
        c.Print (pdfname, 'Title: ' + h.GetName())
    c.Print (pdfname + ']', 'Title: ' + hists[-1].GetName())
    return


def patchdf (df):
    if not hasattr (df.__class__.Snapshot, '__name__'):
        oldsnap = df.__class__.Snapshot
        def mysnap (self, treename, filename, *args):
            ret = oldsnap (self, treename, filename, *args)
            writehists (filename, self.hists)
            self.ee.Report().Print()
            self.mm.Report().Print()
            self.qq.Report().Print()
            return ret
        df.__class__.Snapshot = mysnap
    return


def addhist (df, h):
    if not hasattr (df, 'hists'):
        df.hists = []
    df.hists.append (h)
    return
def h1 (df, name, nx, xlo, xhi, expr = None, title = None):
    if not expr: expr = name
    if not title: title = name
    h = df.Histo1D (ROOT.RDF.TH1DModel (name, title, nx, xlo, xhi), expr)
    addhist (df, h)
    return


class Histmaker:
    def __init__ (self, df, final, prefix = ''):
        self.df = df
        self.prefix = prefix
        if not hasattr (final, 'hists'):
            final.hists = []
        self.hists = final.hists
        return

    def h1 (self, name, nx, xlo, xhi, expr = None, title = None):
        if not expr: expr = name
        if not title: title = name
        h = self.df.Histo1D (ROOT.RDF.TH1DModel (self.prefix + name, title, nx, xlo, xhi), expr)
        self.hists.append (h)
        return


    def kin (self, name):
        self.h1 (name + '_p', 80, 0, 200)
        self.h1 (name + '_pt', 80, 0, 200)
        self.h1 (name + '_m', 80, 0, 200)
        #self.h1 (name + '_mfine', 400, 0, 200)
        self.h1 (name + '_mhigh', 75, 0, 300, expr = name + '_m')
        self.h1 (name + '_theta', 50, 0, 3.5)
        self.h1 (name + '_phi', 50, -3.5, 3.5)
        return


class RDFanalysis:
    @staticmethod
    def hists (hm):
        #hm.h1 ('ele_iso', 100, 0, 100)
        #hm.h1 ('muo_iso', 100, 0, 100)
        #hm.h1 ('muo_iso2', 100, 0, 10, expr = 'muo_iso')
        hm.h1 ('n_eles', 10, 0, 10)
        hm.h1 ('n_muos', 10, 0, 10)
        #hm.h1 ('n_gams', 10, 0, 10)
        hm.h1 ('n_leps', 10, 0, 10)
        hm.h1 ('n_paired_eles', 10, 0, 10)
        hm.h1 ('n_paired_muos', 10, 0, 10)
        hm.h1 ('n_paired_leps', 10, 0, 10)

        #hm.kin ('all_eles')
        hm.kin ('sel_eles')
        #hm.kin ('all_muos')
        hm.kin ('sel_muos')

        #hm.kin ('all_gams')
        #hm.h1 ('all_gams_angle', 100, 0, 2)
        #hm.h1 ('all_gams_angle2', 100, 0, 0.1, expr='all_gams_angle')
        #hm.h1 ('all_gams_dist', 100, 0, 100)
        #hm.h1 ('all_gams_nclust', 10, 0, 10)

        #hm.h1 ('n_charged_had', 50, 0, 50)
        #hm.kin ('charged_had')

        #hm.kin ('ll0')
        hm.kin ('ll')
        #hm.kin ('vis0')
        hm.kin ('vis')
        #hm.kin ('vis2')
        #hm.kin ('mctot')
        #hm.kin ('mcvis')
        #hm.kin ('llrecoil0')
        #hm.kin ('llrecoil')
        #hm.kin ('visrecoil0')
        hm.kin ('visrecoil')

        #hm.h1 ('mjj', 100, 0, 200)
        return


    @staticmethod
    def hists_a (hm):
        hm.kin ('ll0')
        hm.kin ('ll')
        hm.kin ('vis')
        return


    def analysers (df):
        prefilt = (
            df
        #     .Define('mcp1', 'MCParticle::sel_genStatus(1)(MCParticles)')
        #     .Define("mcp2", 'boost(0.030, mcp1)')
        #     .Define('mcmz', 'mcmz(MCParticles, _MCParticles_daughters)')
        #     .Define('mctot', 'sum(mcp2)')
        #     .Define('mctot_pt', 'mctot.Pt()')
        #     .Define('mctot_p', 'mctot.P()')
        #     .Define('mctot_m', 'mctot.M()')
        #     .Define('mctot_theta', 'mctot.Theta()')
        #     .Define('mctot_phi', 'mctot.Phi()')
        #     .Define('mctotraw', 'sum(mcp1)')
        #     .Define('mctotraw_pt', 'mctotraw.Pt()')
        #     .Define('mctotraw_p', 'mctotraw.P()')
        #     .Define('mctotraw_m', 'mctotraw.M()')
        #     .Define('mctotraw_theta', 'mctotraw.Theta()')
        #     .Define('mctotraw_phi', 'mctotraw.Phi()')

        #     .Define('mcvisraw',       'sum_mcvis(mcp1)')
        #     .Define('mcvisraw_pt',    'mcvisraw.Pt()')
        #     .Define('mcvisraw_p',     'mcvisraw.P()')
        #     .Define('mcvisraw_m',     'mcvisraw.M()')
        #     .Define('mcvisraw_theta', 'mcvisraw.Theta()')
        #     .Define('mcvisraw_phi',   'mcvisraw.Phi()')
        #     .Define('mcvis',       'sum_mcvis(mcp2)')
        #     .Define('mcvis_pt',    'mcvis.Pt()')
        #     .Define('mcvis_p',     'mcvis.P()')
        #     .Define('mcvis_m',     'mcvis.M()')
        #     .Define('mcvis_theta', 'mcvis.Theta()')
        #     .Define('mcvis_phi',   'mcvis.Phi()')

        #     .Define('BoostedPandoraPFOs', 'boost(0.030, PandoraPFOs)')
        #     .Alias('TightSelectedPandoraPFOsIndex', 'TightSelectedPandoraPFOs_objIdx.index')
        #     .Define('TightSelectedPandoraPFOs', 'ReconstructedParticle::get (TightSelectedPandoraPFOsIndex, BoostedPandoraPFOs)')

        #     .Define('all_gams', 'ReconstructedParticle::sel_absType(22)(TightSelectedPandoraPFOs)')
        #     .Define('all_gams_pt', 'ReconstructedParticle::get_pt(all_gams)')
        #     .Define('all_gams_p', 'ReconstructedParticle::get_p(all_gams)')
        #     .Define('all_gams_m', 'ReconstructedParticle::get_mass(all_gams)')
        #     .Define('all_gams_theta', 'ReconstructedParticle::get_theta(all_gams)')
        #     .Define('all_gams_phi', 'ReconstructedParticle::get_phi(all_gams)')
        #     .Define('all_gams_nclust', 'get_nclust(all_gams)')

            .Alias("Electron0", "Electron#0.index")
            .Define("all_eles",  "ReconstructedParticle::get(Electron0, ReconstructedParticles)")

            .Define('ele_iso', 'coneIsolation()(all_eles, ReconstructedParticles)')
            .Define('iso_eles', 'sel_iso (0.5, all_eles, ele_iso)')
            .Define('sel_eles', 'ReconstructedParticle::sel_p(10)(iso_eles)')
            .Define('sel_eles_pt', 'ReconstructedParticle::get_pt(sel_eles)')
            .Define('sel_eles_p', 'ReconstructedParticle::get_p(sel_eles)')
            .Define('sel_eles_m', 'ReconstructedParticle::get_mass(sel_eles)')
            .Define('sel_eles_theta', 'ReconstructedParticle::get_theta(sel_eles)')
            .Define('sel_eles_phi', 'ReconstructedParticle::get_phi(sel_eles)')

            .Alias("Muon0", "Muon#0.index")
            .Define("all_muos",  "ReconstructedParticle::get(Muon0, ReconstructedParticles)")
            .Define('muo_iso', 'coneIsolation()(all_muos, ReconstructedParticles)')
            .Define('iso_muos', 'sel_iso (0.5, all_muos, muo_iso)')
            .Define('sel_muos', 'ReconstructedParticle::sel_p(10)(iso_muos)')
            .Define('sel_muos_pt', 'ReconstructedParticle::get_pt(sel_muos)')
            .Define('sel_muos_p', 'ReconstructedParticle::get_p(sel_muos)')
            .Define('sel_muos_m', 'ReconstructedParticle::get_mass(sel_muos)')
            .Define('sel_muos_theta', 'ReconstructedParticle::get_theta(sel_muos)')
            .Define('sel_muos_phi', 'ReconstructedParticle::get_phi(sel_muos)')


            .Define('n_eles', 'sel_eles.size()')
            .Define('n_muos', 'sel_muos.size()')
        #     .Define('n_gams', 'all_gams.size()')
            .Define('n_leps', 'n_eles+n_muos')
            .Define("paired_eles", "sel_ospair()(sel_eles)")
            .Define("paired_muos", "sel_ospair()(sel_muos)")
            .Define('n_paired_eles', 'paired_eles.size()')
            .Define('n_paired_muos', 'paired_muos.size()')
            .Define('paired_leps', 'merge(paired_eles, paired_muos)')
            .Define('n_paired_leps', 'paired_leps.size()')

        #     .Define('gam_match', 'photonLeptonMatch(all_gams, paired_leps, PandoraClusters)')
        #     .Define('all_gams_angle', 'get_match_angle(gam_match)')
        #     .Define('all_gams_dist', 'get_match_dist(gam_match)')
        #     .Define('all_gams_lepndx', 'get_match_lepndx(gam_match)')
        #     .Define('all_gams_gamndx', 'get_match_gamndx(gam_match)')
        #     .Define('all_gams_DNN2', 'get_match_DNN2(gam_match)')

        #     .Define('paired_leps_fsr', 'addfsr_DNN2(paired_leps,all_gams,gam_match,0.05,0.5)')
        #     .Define('paired_leps_mc_match', 'match (mcp2, paired_leps)')
        #     .Define('paired_leps_gam_match', 'leptonPhotonMatches(paired_leps, gam_match, 0.05)')
        #     .Define('paired_leps_gamndx', 'get_match_gamndx(paired_leps_gam_match)')
        #     .Define('paired_leps_angle', 'get_match_angle(paired_leps_gam_match)')
        #     .Define('paired_leps_dist', 'get_match_dist(paired_leps_gam_match)')
        #     .Define('paired_leps_gamE', 'get_match_gamE(paired_leps_gam_match, all_gams, PandoraClusters)')
        #     .Define('paired_leps_clustE', 'get_rp_clustE(paired_leps, PandoraClusters)')
        #     .Define('paired_leps_DNN2', 'get_match_DNN2(paired_leps_gam_match)')
        #     .Define('paired_leps_should_add', 'make_should_add(paired_leps, all_gams, mcp2, paired_leps_gam_match, paired_leps_mc_match)')

        #     .Define('mjj', 'calc_mjj(RefinedVertexJets)')
        #     .Define('charged_had', 'sel_charged_had (BoostedPandoraPFOs, 5)')
        #     .Define('charged_had_pt', 'ReconstructedParticle::get_pt(charged_had)')
        #     .Define('charged_had_p', 'ReconstructedParticle::get_p(charged_had)')
        #     .Define('charged_had_m', 'ReconstructedParticle::get_mass(charged_had)')
        #     .Define('charged_had_theta', 'ReconstructedParticle::get_theta(charged_had)')
        #     .Define('charged_had_phi', 'ReconstructedParticle::get_phi(charged_had)')
        #     .Define('n_charged_had', 'charged_had.size()')
        #     .Define('charged_had_match', 'photonLeptonMatch(all_gams, charged_had, PandoraClusters)')
        #     .Define('charged_had_angle', 'get_match_angle(charged_had_match)')
        #     .Define('charged_had_dist', 'get_match_dist(charged_had_match)')
        #     .Define('charged_had_lepndx', 'get_match_lepndx(charged_had_match)')
        #     .Define('charged_had_gamndx', 'get_match_gamndx(charged_had_match)')
        #     .Define('charged_had_DNN2', 'get_match_DNN2(charged_had_match)')

        #     .Define('ll0', f'resonanceBuilder({MZ})(paired_leps)')
        #     .Define('ll0_pt', 'ReconstructedParticle::get_pt(ll0)')
        #     .Define('ll0_p', 'ReconstructedParticle::get_p(ll0)')
        #     .Define('ll0_m', 'ReconstructedParticle::get_mass(ll0)')
        #     .Define('ll0_theta', 'ReconstructedParticle::get_theta(ll0)')
        #     .Define('ll0_phi', 'ReconstructedParticle::get_phi(ll0)')

            .Define('ll', f'resonanceBuilder({MZ})(paired_leps)')
            .Define('ll_pt', 'ReconstructedParticle::get_pt(ll)')
            .Define('ll_p', 'ReconstructedParticle::get_p(ll)')
            .Define('ll_m', 'ReconstructedParticle::get_mass(ll)')
            .Define('ll_theta', 'ReconstructedParticle::get_theta(ll)')
            .Define('ll_phi', 'ReconstructedParticle::get_phi(ll)')

        #     .Define('llrecoil', 'recoilBuilder2()(240, ll)')
        #     .Define('llrecoil_pt', 'llrecoil.Pt()')
        #     .Define('llrecoil_p', 'llrecoil.P()')
        #     .Define('llrecoil_m', 'llrecoil.M()')
        #     .Define('llrecoil_theta', 'llrecoil.Theta()')
        #     .Define('llrecoil_phi', 'llrecoil.Phi()')

        #     .Define('llrecoil0', 'recoilBuilder2()(240, ll0)')
        #     .Define('llrecoil0_pt', 'llrecoil0.Pt()')
        #     .Define('llrecoil0_p', 'llrecoil0.P()')
        #     .Define('llrecoil0_m', 'llrecoil0.M()')
        #     .Define('llrecoil0_theta', 'llrecoil0.Theta()')
        #     .Define('llrecoil0_phi', 'llrecoil0.Phi()')

        #     .Define('visraw', 'sum(PandoraPFOs)')
        #     .Define('visraw_pt', 'visraw.Pt()')
        #     .Define('visraw_p', 'visraw.P()')
        #     .Define('visraw_m', 'visraw.M()')
        #     .Define('visraw_theta', 'visraw.Theta()')
        #     .Define('visraw_phi', 'visraw.Phi()')
        #     .Define('vis0', 'sum(BoostedPandoraPFOs)')
        #     .Define('vis0_pt', 'vis0.Pt()')
        #     .Define('vis0_p', 'vis0.P()')
        #     .Define('vis0_m', 'vis0.M()')
        #     .Define('vis0_theta', 'vis0.Theta()')
        #     .Define('vis0_phi', 'vis0.Phi()')

            .Define('vis', 'sum(ReconstructedParticles)')
            .Define('vis_pt', 'vis.Pt()')
            .Define('vis_p', 'vis.P()')
            .Define('vis_m', 'vis.M()')
            .Define('vis_theta', 'vis.Theta()')
            .Define('vis_phi', 'vis.Phi()')


        #     .Define('vis2', 'corrvis_DNN2(vis,charged_had,all_gams,charged_had_match,0.05,0.5)')
        #     .Define('vis2_pt', 'vis2.Pt()')
        #     .Define('vis2_p', 'vis2.P()')
        #     .Define('vis2_m', 'vis2.M()')
        #     .Define('vis2_theta', 'vis2.Theta()')
        #     .Define('vis2_phi', 'vis2.Phi()')

        #     .Define('visrecoil0', 'recoilBuilder2()(240, vis0)')
        #     .Define('visrecoil0_pt', 'visrecoil0.Pt()')
        #     .Define('visrecoil0_p', 'visrecoil0.P()')
        #     .Define('visrecoil0_m', 'visrecoil0.M()')
        #     .Define('visrecoil0_theta', 'visrecoil0.Theta()')
        #     .Define('visrecoil0_phi', 'visrecoil0.Phi()')
            .Define('visrecoil', 'recoilBuilder2()(240, vis)')
            .Define('visrecoil_pt', 'visrecoil.Pt()')
            .Define('visrecoil_p', 'visrecoil.P()')
            .Define('visrecoil_m', 'visrecoil.M()')
            .Define('visrecoil_theta', 'visrecoil.Theta()')
            .Define('visrecoil_phi', 'visrecoil.Phi()')
        )

        prefilt.ee = (
            prefilt
             .Filter('n_leps==2 && n_paired_eles==2', 'ee')
             .Filter('vis_pt > 10', 'ee_met')
             .Filter(f'abs(ll_m[0]-{MZ}) < 4', 'ee_mz')
             )
        prefilt.mm = (
            prefilt
             .Filter('n_leps==2 && n_paired_muos==2', 'mm')
             .Filter('vis_pt > 10', 'mm_met')
             .Filter(f'abs(ll_m[0]-{MZ}) < 4', 'mm_mz')
             )
        prefilt.qq = (
            prefilt
             .Filter('n_leps==0', 'qq')
             .Filter('vis_pt > 15', 'qq_met')
             .Filter(f'vis_m>86&&vis_m<105', 'qq_mz')
             )
        
        RDFanalysis.hists (Histmaker (prefilt, prefilt, 'prefilt_'))
        RDFanalysis.hists (Histmaker (prefilt.ee, prefilt, 'ee_'))
        RDFanalysis.hists (Histmaker (prefilt.mm, prefilt, 'mm_'))
        RDFanalysis.hists (Histmaker (prefilt.qq, prefilt, 'qq_'))

        patchdf (prefilt)
        return prefilt


    def output():
        return [
            #'n_eles',
            #'sel_eles',
            #'sel_eles_pt',
            #'sel_eles_p',
            #'sel_eles_theta',
            #'sel_eles_phi',

            #'n_muos',
            #'sel_muos',
            #'sel_muos_pt',
            #'sel_muos_p',
            #'sel_muos_theta',
            #'sel_muos_phi',
            #'ele_iso',
            # 'muo_iso',
            # 'sel_eles',
            # 'sel_muos',
            # 'n_eles',
            # 'n_muos',

            # 'all_gams',
            # 'all_gams_pt',
            # 'all_gams_p',
            # 'all_gams_m',
            # 'all_gams_theta',
            # 'all_gams_phi',
            # 'all_gams_angle',
            # 'all_gams_dist',
            # 'all_gams_lepndx',
            # 'all_gams_gamndx',
            # 'all_gams_DNN2',
            # 'n_gams',

            # 'paired_leps_angle',
            # 'paired_leps_dist',
            # 'paired_leps_gamE',
            # 'paired_leps_clustE',
            # 'paired_leps_DNN2',
            # 'paired_leps_should_add',
            # 'paired_leps_gamndx',
            # 'sel_eles_p',
            # 'sel_eles_theta',
            # 'sel_eles_phi',

            # 'mjj',
            # 'charged_had_pt',
            # 'charged_had_p',
            # 'charged_had_m',
            # 'charged_had_theta',
            # 'charged_had_phi',
            # 'charged_had_angle',
            # 'charged_had_dist',
            # 'charged_had_lepndx',
            # 'charged_had_gamndx',
            # 'charged_had_DNN2',

            # 'll',
            # 'll_pt',
            # 'll_p',
            # 'll_m',
            # 'll_theta',
            # 'll_phi',
            # 'llrecoil',
            # 'llrecoil_pt',
            # 'llrecoil_p',
            # 'llrecoil_m',
            # 'llrecoil_theta',
            # 'llrecoil_phi',

            # 'll0',
            # 'll0_pt',
            # 'll0_p',
            # 'll0_m',
            # 'll0_theta',
            # 'll0_phi',
            # 'llrecoil0',
            # 'llrecoil0_pt',
            # 'llrecoil0_p',
            # 'llrecoil0_m',
            # 'llrecoil0_theta',
            # 'llrecoil0_phi',

            # 'mcmz',
            # 'mctot',
            # 'mctot_pt',
            # 'mctot_p',
            # 'mctot_m',
            # 'mctot_theta',
            # 'mctot_phi',
            # 'mctotraw',
            # 'mctotraw_pt',
            # 'mctotraw_p',
            # 'mctotraw_m',
            # 'mctotraw_theta',
            # 'mctotraw_phi',

            # 'mcvisraw',
            # 'mcvisraw_pt',
            # 'mcvisraw_p',
            # 'mcvisraw_m',
            # 'mcvisraw_theta',
            # 'mcvisraw_phi',
            # 'mcvis',
            # 'mcvis_pt',
            # 'mcvis_p',
            # 'mcvis_m',
            # 'mcvis_theta',
            # 'mcvis_phi',

            # 'vis0',
            # 'vis0_pt',
            # 'vis0_p',
            # 'vis0_m',
            # 'vis0_theta',
            # 'vis0_phi',
            # 'vis',
            # 'vis_pt',
            # 'vis_p',
            # 'vis_m',
            # 'vis_theta',
            # 'vis_phi',
            # 'vis2',
            # 'vis2_pt',
            # 'vis2_p',
            # 'vis2_m',
            # 'vis2_theta',
            # 'vis2_phi',
            # 'visraw',
            # 'visraw_pt',
            # 'visraw_p',
            # 'visraw_m',
            # 'visraw_theta',
            # 'visraw_phi',
            # 'visrecoil0',
            # 'visrecoil0_pt',
            # 'visrecoil0_p',
            # 'visrecoil0_m',
            # 'visrecoil0_theta',
            # 'visrecoil0_phi',
            # 'visrecoil',
            # 'visrecoil_pt',
            # 'visrecoil_p',
            # 'visrecoil_m',
            # 'visrecoil_theta',
            # 'visrecoil_phi',
            ]

