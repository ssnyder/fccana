#include "FCCAnalyses/ReconstructedParticle.h"
#include "FCCAnalyses/defines.h"
#include "edm4hep/ClusterData.h"
#include <vector>

using FCCAnalyses::Vec_rp;
using FCCAnalyses::Vec_mc;
using FCCAnalyses::Vec_f;
using Vec_cl = ROOT::VecOps::RVec<edm4hep::ClusterData>;


/////////////////////////////////////////////////////////////////////////////
// NN for brem recovery.


struct DNN2Reader
{
  float angle = 0;
  float p = 0;
  float theta = 0;
  float clustE = 0;
  float gam_clustE = 0;
  float best_p = 0;
  float mc_p = 0;
  TMVA::Reader reader;
  DNN2Reader();
  double eval();
  static void setWeightsFile (const std::string& s) {
    weightsFile = s;
  }
  static std::string weightsFile;
};
std::string DNN2Reader::weightsFile;


DNN2Reader::DNN2Reader()
  : reader("!V")
{
  reader.AddVariable ("angle",      &angle);
  reader.AddVariable ("p",          &p);
  reader.AddVariable ("theta",      &theta);
  reader.AddVariable ("clustE",     &clustE);
  reader.AddVariable ("gam_clustE", &gam_clustE);
  reader.AddSpectator ("best_p",    &best_p);
  reader.AddSpectator ("mc_p",      &mc_p);
  reader.BookMVA ("DNN2", weightsFile);
}


double DNN2Reader::eval()
{
  return reader.EvaluateMVA ("DNN2");
}


thread_local std::unique_ptr<DNN2Reader> dnn2Reader;


// Return the DNN2 value for this lepton/gamma match.
float DNN2_value (float angle,
                  const edm4hep::ReconstructedParticleData& lep,
                  const edm4hep::ReconstructedParticleData& gam,
                  const Vec_cl& clusts)
{
  if (!dnn2Reader) dnn2Reader = std::make_unique<DNN2Reader>();
  DNN2Reader& r = *dnn2Reader;
  TLorentzVector vlep = tlv (lep);

  r.angle = angle;
  r.p = vlep.P();
  r.theta = vlep.Theta();
  r.clustE = 0;
  if (lep.clusters_begin != lep.clusters_end) {
    r.clustE = clusts.at (lep.clusters_begin).energy;
  }
  r.gam_clustE = 0;
  if (gam.clusters_begin != gam.clusters_end) {
    r.gam_clustE = clusts.at (gam.clusters_begin).energy;
  }

  return r.eval();
}


struct PhotonLeptonMatch
{
  PhotonLeptonMatch (int lepndx_) : lepndx (lepndx_) {}
  PhotonLeptonMatch (float angle_, float dist_, float deltaE_,
                     float DNN2_,
                     int gamndx_, int lepndx_, int lepid_)
    : angle(angle_), dist(dist_), deltaE(deltaE_), DNN2(DNN2_),
      gamndx(gamndx_),lepndx(lepndx_), lepid(lepid_) {}
  float angle = 999;
  float dist = 999;
  float deltaE = 999;
  float DNN2 = 999;
  int gamndx = -1;
  int lepndx = -1;
  int lepid = -1;
};


std::vector<float> get_match_angle (const std::vector<PhotonLeptonMatch>& mv)
{
  std::vector<float> out;
  out.reserve (mv.size());
  for (const PhotonLeptonMatch& m : mv) {
    out.push_back (m.angle);
  }
  return out;
}


std::vector<float> get_match_dist (const std::vector<PhotonLeptonMatch>& mv)
{
  std::vector<float> out;
  out.reserve (mv.size());
  for (const PhotonLeptonMatch& m : mv) {
    out.push_back (m.dist);
  }
  return out;
}


std::vector<float> get_match_DNN2 (const std::vector<PhotonLeptonMatch>& mv)
{
  std::vector<float> out;
  out.reserve (mv.size());
  for (const PhotonLeptonMatch& m : mv) {
    out.push_back (m.DNN2);
  }
  return out;
}


std::vector<int> get_match_lepndx (const std::vector<PhotonLeptonMatch>& mv)
{
  std::vector<int> out;
  out.reserve (mv.size());
  for (const PhotonLeptonMatch& m : mv) {
    out.push_back (m.lepndx);
  }
  return out;
}


std::vector<int> get_match_gamndx (const std::vector<PhotonLeptonMatch>& mv)
{
  std::vector<int> out;
  out.reserve (mv.size());
  for (const PhotonLeptonMatch& m : mv) {
    out.push_back (m.gamndx);
  }
  return out;
}


std::vector<float> get_match_pt (const std::vector<PhotonLeptonMatch>& mv,
                                 const Vec_rp& leps)
{
  std::vector<float> out;
  out.reserve (mv.size());
  for (const PhotonLeptonMatch& m : mv) {
    TLorentzVector l = tlv (leps[m.lepndx]);
    out.push_back (l.Perp());
  }
  return out;
}


std::vector<float> get_match_dratio (const std::vector<PhotonLeptonMatch>& mv,
                                     const Vec_rp& leps)
{
  std::vector<float> out;
  out.reserve (mv.size());
  for (const PhotonLeptonMatch& m : mv) {
    TLorentzVector l = tlv (leps[m.lepndx]);
    out.push_back (m.dist / l.Perp());
  }
  return out;
}


std::vector<float> get_match_deltaE (const std::vector<PhotonLeptonMatch>& mv)
{
  std::vector<float> out;
  out.reserve (mv.size());
  for (const PhotonLeptonMatch& m : mv) {
    out.push_back (m.deltaE);
  }
  return out;
}


std::vector<float> get_match_gamE (const std::vector<PhotonLeptonMatch>& mv,
                                   const Vec_rp& gams,
                                   const Vec_cl& clusts)
{
  std::vector<float> out;
  out.reserve (mv.size());
  for (const PhotonLeptonMatch& m : mv) {
    if (m.gamndx >= 0) {
      const auto& gam = gams.at(m.gamndx);
      if (gam.clusters_begin != gam.clusters_end) {
        out.push_back (clusts.at (gam.clusters_begin).energy);
        continue;
      }
    }
    out.push_back (999);
  }
  return out;
}


std::vector<float>
get_rp_clustE (const Vec_rp& rps,
               const Vec_cl& clusts)
{
  std::vector<float> out;
  out.reserve (rps.size());
  for (const auto& rp : rps) {
    if (rp.clusters_begin != rp.clusters_end) {
      out.push_back (clusts.at (rp.clusters_begin).energy);
    }
    else {
      out.push_back (999);
    }
  }
  return out;
}


const PhotonLeptonMatch* get_best_match (size_t ilep,
                                         const std::vector<PhotonLeptonMatch>& mv,
                                         float angmax)
{
  const PhotonLeptonMatch* best_match = nullptr;
  for (const PhotonLeptonMatch& m : mv) {
    if (m.lepndx == ilep && m.angle < angmax) {
      return &m;
    }
  }
  return nullptr;
}


// Convert a vector of per-photon PhotonLeptonMatch to per-lepton.
std::vector<PhotonLeptonMatch>
leptonPhotonMatches (const Vec_rp& leps,
                     const std::vector<PhotonLeptonMatch>& mv,
                     float angmax)
{
  std::vector<PhotonLeptonMatch> out;
  out.reserve (leps.size());
  for (size_t i = 0; i < leps.size(); i++) {
    const PhotonLeptonMatch* m = get_best_match (i, mv, angmax);
    if (m) {
      out.emplace_back (*m);
    }
    else {
      out.emplace_back (i);
    }
  }
  return out;
}


std::vector<PhotonLeptonMatch>
photonLeptonMatch (const Vec_rp& gams, const Vec_rp& leps,
                   const Vec_cl& clusts = Vec_cl())
{
  std::vector<PhotonLeptonMatch> ret;
  ret.reserve (gams.size());
  for (size_t i = 0; i < gams.size(); i++) {
    TLorentzVector vgam = tlv (gams[i]);
    float minangle = 999;
    int lepndx = -1;
    int lepid = -999;

    for (size_t j = 0; j < leps.size(); j++) {
      const auto& lep = leps[j];
      TLorentzVector vlep = tlv (lep);
      float angle = vgam.Angle (vlep.Vect());
      if (angle < minangle) {
        minangle = angle;
        lepndx = j;
        lepid = lep.type;
      }
    }

    float dist = 999;
    float deltaE = 999;
    float DNN2 = -1;

    if (!clusts.empty()) {
      if (lepndx >= 0) {
        const edm4hep::ReconstructedParticleData& lep = leps.at(lepndx);
        TLorentzVector vlep = tlv (lep);
        const auto& cl_gam = clusts.at (gams.at(i).clusters_begin);
        const auto& cl_lep = clusts.at (lep.clusters_begin);
        TVector3 v3gam (cl_gam.position.x, cl_gam.position.y, cl_gam.position.z);
        TVector3 v3lep (cl_lep.position.x, cl_lep.position.y, cl_lep.position.z);
        dist = (v3gam - v3lep).Mag();
        deltaE = vlep.E() - cl_lep.energy;
      }

      if (minangle < 0.2) {
        DNN2 = DNN2_value (minangle, leps.at(lepndx), gams.at(i), clusts);
      }
    }

    ret.emplace_back (minangle, dist, deltaE, DNN2, i, lepndx, lepid);
  }
  return ret;
}


Vec_rp addfsr (const Vec_rp& leps, const Vec_rp& gams,
               const std::vector<PhotonLeptonMatch>& mv,
               float angmax)
{
  std::vector<bool> done (leps.size());
  Vec_rp out = leps;
  for (size_t i = 0; i < mv.size(); i++) {
    const PhotonLeptonMatch& m = mv[i];
    if (m.angle < angmax) {
      if (m.lepndx >= 0 && !done[m.lepndx]) {
        done[m.lepndx] = true;
        auto& lep = out[m.lepndx];
        TLorentzVector vgam = tlv (gams[i]);
        lep.momentum.x += vgam.Px();
        lep.momentum.y += vgam.Py();
        lep.momentum.z += vgam.Pz();
      }
    }
  }
  return out;
}


std::vector<int> make_should_add (const Vec_rp& leps,
                                  const Vec_rp& gams,
                                  const Vec_mc& mcs,
                                  const std::vector<PhotonLeptonMatch>& lep_mv,
                                  const std::vector<MatchRel>& mcmatches)
{
  std::vector<int> should_add;
  should_add.reserve (leps.size());
  for (size_t ilep = 0; ilep < leps.size(); ++ilep) {
    auto plep = tlv (leps.at(ilep));
    const PhotonLeptonMatch& m = lep_mv.at(ilep);
    int mcndx = -1;
    for (const MatchRel& match : mcmatches) {
      if (match.rpndx == ilep) {
        mcndx = match.mcndx;
        break;
      }
    }
    if (mcndx < 0 || m.gamndx < 0) {
      should_add.push_back (-1);
      continue;
    }
    auto pmc = tlv (mcs.at(mcndx));
    auto pgam = tlv (gams.at(m.gamndx));

    auto ptot = plep + pgam;
    if (std::abs (pmc.P() - ptot.P()) < std::abs (pmc.P() - plep.P())) {
      should_add.push_back (1);
    }
    else {
      should_add.push_back (0);
    }
  }
  return should_add;
}


std::vector<float> gamE (const Vec_rp& leps, const Vec_rp& gams,
                         const std::vector<PhotonLeptonMatch>& mv,
                         float angmax)
{
  std::vector<float> out (leps.size());
  for (size_t i = 0; i < mv.size(); i++) {
    const PhotonLeptonMatch& m = mv[i];
    if (m.angle < angmax) {
      if (m.lepndx >= 0) {
        TLorentzVector vgam = tlv (gams[i]);
        out[m.lepndx] += vgam.E();
      }
    }
  }
  return out;
}


Vec_rp addfsr_DNN2 (const Vec_rp& leps, const Vec_rp& gams,
                    const std::vector<PhotonLeptonMatch>& mv,
                    float angmax, float DNNval)
{
  std::vector<bool> done (leps.size());
  Vec_rp out = leps;
  for (size_t i = 0; i < mv.size(); i++) {
    const PhotonLeptonMatch& m = mv[i];
    if (m.angle < angmax) {
      if (m.lepndx >= 0 && !done[m.lepndx]) {
        done[m.lepndx] = true;
        if (m.DNN2 > DNNval) {
          auto& lep = out[m.lepndx];
          TLorentzVector vgam = tlv (gams[i]);
          lep.momentum.x += vgam.Px();
          lep.momentum.y += vgam.Py();
          lep.momentum.z += vgam.Pz();
        }
      }
    }
  }
  return out;
}


TLorentzVector corrvis_DNN2 (const TLorentzVector& vis,
                             const Vec_rp& leps, const Vec_rp& gams,
                             const std::vector<PhotonLeptonMatch>& mv,
                             float angmax, float DNNval)
{
  TLorentzVector out = vis;
  std::vector<bool> done (leps.size());
  for (size_t i = 0; i < mv.size(); i++) {
    const PhotonLeptonMatch& m = mv[i];
    if (m.angle < angmax) {
      if (m.lepndx >= 0 && !done[m.lepndx]) {
        done[m.lepndx] = true;
        if (m.DNN2 <= DNNval) {
          out -= tlv (gams[i]);
        }
      }
    }
  }
  return out;
}


std::vector<PhotonLeptonMatch>
selgoodbadfsr (const std::vector<MatchRel>& mrv,
               const std::vector<PhotonLeptonMatch>& plv,
               const Vec_rp& leps,
               const Vec_rp& gams,
               const Vec_mc& mcs,
               float angmax,
               bool good)
{
  std::vector<PhotonLeptonMatch> out;
  for (const MatchRel& mr : mrv) {
    for (const PhotonLeptonMatch& pl : plv) {
      if (mr.rpndx == pl.lepndx && pl.angle < angmax) {
        TLorentzVector lep0 = tlv (leps[pl.lepndx]);
        TLorentzVector lep1 = lep0 + tlv (gams[pl.gamndx]);
        TLorentzVector mc = tlv (mcs[mr.mcndx]);
        bool fsr_closer = std::abs(lep1.Perp() - mc.Perp()) < std::abs(lep0.Perp() - mc.Perp());
        if ((good && fsr_closer) || (!good && !fsr_closer)) {
          out.push_back (pl);
        }
        break;
      }
    }
  }
  return out;
}



