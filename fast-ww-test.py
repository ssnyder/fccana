import ROOT
from printpdf import printpdf
from math import *

# In fb
fb = 1
pb = 1000*fb
intlum = 5000/fb
xsect = 16.4385*pb

norm = ROOT.TFile('hinv-norm.root')

full_ww_ee = norm.Get('ee_ww_recoil')
full_ww_mm = norm.Get('mm_ww_recoil')
full_ww_qq = norm.Get('qq_ww_recoil')

fast = ROOT.TFile('hinv-fast-ww.root')

fast_ww_prefilt = fast.Get('prefilt_visrecoil_m')
nfast = fast_ww_prefilt.GetEntries()
scale = intlum * xsect / nfast

fast_ww_ee = fast.Get('ee_visrecoil_m').Clone('fast_ww_ee')
fast_ww_ee.Sumw2()
fast_ww_ee.Scale(scale)

fast_ww_mm = fast.Get('mm_visrecoil_m').Clone('fast_ww_mm')
fast_ww_mm.Sumw2()
fast_ww_mm.Scale(scale)

fast_ww_qq = fast.Get('qq_visrecoil_m').Clone('fast_ww_qq')
fast_ww_qq.Sumw2()
fast_ww_qq.Scale(scale)


