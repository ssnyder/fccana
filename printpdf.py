import time
import os
import string
from draw_obj import get_canvas


def gettime():
    return time.strftime('%y%m%d%H%M')
def inttime(t):
    pos = len(t)
    while pos > 0 and t[pos-1] in string.ascii_letters:
        pos -= 1
    return int (t[:pos])
def timediff(a, b):
    return inttime(b) - inttime(a)
    

lasttime = None
def plotfilename (nameroot, ext):
    global lasttime
    thistime = gettime()
    if lasttime == None or timediff (lasttime, thistime) >= 10 or os.path.exists (f'{lasttime}_{nameroot}.{ext}'):
        lasttime = thistime
        ilet = 0
        while os.path.exists (f'{lasttime}_{nameroot}.{ext}'):
            assert ilet < len(string.ascii_lowercase)
            lasttime = thistime + string.ascii_lowercase[ilet]
            ilet += 1
    return f'{lasttime}_{nameroot}.{ext}'


def printpdf (nameroot):
    """Print the current canvas as a pdf file."""

    fname = plotfilename (nameroot, 'pdf')

    c1 = get_canvas()
    c1.Print (fname, "pdf,Portrait")
    return
