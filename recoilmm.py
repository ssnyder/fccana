#fccanalysis run recoilmm.py --output recoilmm-new.root --files-list   ../data/reco-cld/hinvmm.2003_20000/hinvmm.2003_20000_*.rec_edm4hep.root 2>&1|tee log-recoilmm-new
#fccanalysis run recoilmm.py --output recoilmm-old.root --files-list ../data/reco-cld/hinvmm.2003_20000-save/*.rec.root 2>&1|tee log-recoilmm-old

import ROOT
code = open ('recoil.cc').read()
ROOT.gInterpreter.Declare(code)


class RDFanalysis:
    def analysers (df):
        df2 = (
            df
            #.Alias("_MCParticles_daughters", "MCParticles#1")
            .Alias("TightSelectedPandoraPFOsIndex", "TightSelectedPandoraPFOs_objIdx.index")
            .Define("TightSelectedPandoraPFOs", "ReconstructedParticle::get (TightSelectedPandoraPFOsIndex, PandoraPFOs)")
            .Define("eventNumber", "EventHeader[0].eventNumber")
            .Define("mcp1", "MCParticle::sel_genStatus(1)(MCParticles)")
            .Define("mcp2", "MCParticle::sel_pt(5)(mcp1)")
            .Define('mcisrgam', 'sel_isrgam()(MCParticles, _MCParticles_daughters)')
            .Define('mcisrgamz1', 'calc_mcisrgamz1(mcisrgam)')
            .Define('mcisrgamz2', 'calc_mcisrgamz2(mcisrgam)')
            .Define('x1', '1 - mcisrgamz1/120.')
            .Define('x2', '1 + mcisrgamz2/120.')
            .Define('beta', '(x1-x2)/(x1+x2)')
            .Define('truesqrts0', 'calc_truesqrts()(MCParticles)')
            .Define('truesqrts', '240.*sqrt(x1*x2)')
            .Define("mcmuo", "MCParticle::sel_pdgID(13,true)(mcp2)")
            .Define("mcgam", "MCParticle::sel_pdgID(22,true)(mcp2)")
            .Define("mcz", "resonanceBuilder(91)(mcmuo)")
            .Define("mcz_pt", "MCParticle::get_pt(mcz)")
            .Define("mcz_p", "MCParticle::get_p(mcz)")
            .Define("mcz_m", "MCParticle::get_mass(mcz)")
            .Define('mch', 'recoilBuilder()(240, mcz)')
            .Define("mch_pt", "MCParticle::get_pt(mch)")
            .Define("mch_m", "MCParticle::get_mass(mch)")
            .Define('mcht', 'recoilBuilder()(truesqrts, mcz)')
            .Define("mcht_pt", "MCParticle::get_pt(mcht)")
            .Define("mcht_m", "MCParticle::get_mass(mcht)")

            .Define("muons", "sel_leps(13)(TightSelectedPandoraPFOs)")
            #.Filter("muons.size() >= 2")
            .Define("muon_pt", "ReconstructedParticle::get_pt(muons)")
            .Define("z", "resonanceBuilder(91)(muons)")
            .Define("z_pt", "ReconstructedParticle::get_pt(z)")
            .Define("z_p", "ReconstructedParticle::get_p(z)")
            .Define("z_m", "ReconstructedParticle::get_mass(z)")
            .Define('h', 'recoilBuilder()(240, z)')
            .Define("h_pt", "ReconstructedParticle::get_pt(h)")
            .Define("h_m", "ReconstructedParticle::get_mass(h)")
            .Define('ht', 'recoilBuilder()(truesqrts, z)')
            .Define("ht_pt", "ReconstructedParticle::get_pt(ht)")
            .Define("ht_m", "ReconstructedParticle::get_mass(ht)")
            )
        return df2


    def output():
        return [
            'eventNumber',
            'mcisrgam',
            'mcisrgamz1',
            'mcisrgamz2',
            'x1',
            'x2',
            'beta',
            'truesqrts0',
            'truesqrts',
            'mcmuo',
            'mcgam',
            'mcz',
            'mcz_p',
            'mcz_pt',
            'mcz_m',
            'mch',
            'mch_pt',
            'mch_m',
            'mcht',
            'mcht_pt',
            'mcht_m',

            'muons',
            'muon_pt',
            'z',
            'z_p',
            'z_pt',
            'z_m',
            'h',
            'h_pt',
            'h_m',
            'ht',
            'ht_pt',
            'ht_m',
            ]


