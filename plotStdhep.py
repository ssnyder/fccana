#!/usr/bin/env python

import sys
from pyLCIO.drivers.Driver import Driver
import ROOT


class Dumper( Driver ):
    def startOfData (self):
        self.n = 0
        self.totet = ROOT.TH1F ('totet', 'totet', 50, 0, 50)
        self.phiet = ROOT.TH1F ('phiet', 'phiet', 50, -3.5, 3.5)
        self.totetcut = ROOT.TH1F ('totetcut', 'totetcut', 50, 0, 50)
        self.phietcut = ROOT.TH1F ('phietcut', 'phietcut', 50, -3.5, 3.5)
    def processEvent (self, event):
        if self.n%100 == 0: print ('---', self.n)
        self.n = self.n + 1
        mcParticles = event.getMcParticles()
        tot = ROOT.TLorentzVector()
        totcut = ROOT.TLorentzVector()
        for p in mcParticles:
            stat = p.getGeneratorStatus()
            if stat == 1:
                v = p.getLorentzVec()
                tot += v
                if abs(v.Eta()) < 4:
                    totcut += v
        self.totet.Fill (tot.Pt())
        self.phiet.Fill (tot.Phi())
        self.totetcut.Fill (totcut.Pt())
        self.phietcut.Fill (totcut.Phi())
        return
    def endOfData (self):
        f = ROOT.TFile ('plotStdhep.root', 'RECREATE')
        self.totet.Write()
        self.phiet.Write()
        self.totetcut.Write()
        self.phietcut.Write()
        f.Close()
        return


def loop (infile, n = -1, skip = 0):
    from pyLCIO.io.EventLoop import EventLoop
    eventLoop = EventLoop()
    eventLoop.addFile( infile )

    dumper = Dumper()
    eventLoop.add( dumper )

    eventLoop.skipEvents( skip )
    eventLoop.loop( n )
    return


import sys
infile = sys.argv[1]
#infile = '../data/whizard/zh_nneenn.1011_500/zh_nneenn.1011_500.stdhep'
loop (infile)

