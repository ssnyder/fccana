#include "DDRec/DetectorData.h"
#include "DD4hep/DetElement.h"
#include "detectorSegmentations/FCCSWGridModuleThetaMerged_k4geo.h"

const dd4hep::rec::LayeredCalorimeterStruct* getLayerInfo (const dd4hep::DetElement& de)
{
  return de.extension<dd4hep::rec::LayeredCalorimeterData>();
}


double theta (const dd4hep::DDSegmentation::Segmentation& seg_in,
              const dd4hep::DDSegmentation::CellID& cid)
{
  const auto& seg = dynamic_cast<const dd4hep::DDSegmentation::FCCSWGridModuleThetaMerged_k4geo&> (seg_in);
  return seg.theta (cid);
}

