import ROOT

texGeV = r'Ge}\!\!\;\text{V'

class Label:
    def __init__ (self, x = 0.2, y = 0.87, size = 0.045, ystep = 0.06, xstep = 0.11, intlum = None):
        self.x = x
        self.y = y
        self.ystep = ystep
        self.tex = ROOT.TLatex()
        self.tex.SetNDC()
        self.tex.SetTextFont(72)
        self.tex.SetTextSize(size)
        self.tex.DrawLatex (x, y ,"FCC-ee")
        self.tex.SetTextFont(42)
        self.tex.SetTextSize(size)
        #self.tex.DrawLatex (x + xstep, y, "Internal")
        text = r'\sqrt{s} = \text{240 '+texGeV
        if intlum is not None:
            text = text + ', '+str(intlum)+' ab}^{-1}'
        else:
            text = text + '}'
        self.addline (text)
        return

    def addline (self, line, yadj = 0):
        self.y -= self.ystep + yadj
        return self.tex.DrawLatex (self.x, self.y, line)
