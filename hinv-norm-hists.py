import ROOT
import os


# In fb
fb = 1
pb = 1000*fb
intlum = 10800/fb


class Samp:
    def __init__ (self, name, xs, br=1):
        self.name = name
        self.hname = name.replace ('-', '_')
        self.xsbr = xs * br
        return


    def readhists (self):
        f = ROOT.TFile (f'{self.name}.root')
        ROOT.gROOT.cd()
        self.ee0_hists = self.readhistf (f, 'ee0')
        self.ee_hists = self.readhistf (f, 'ee')
        self.mm0_hists = self.readhistf (f, 'mm0')
        self.mm_hists = self.readhistf (f, 'mm')
        self.qq_hists = self.readhistf (f, 'qq')
        f.Close()
        return


    def writehists (self):
        for i in range(2):
            self.ee0_hists[i].Write()
            self.ee_hists[i].Write()
            self.mm0_hists[i].Write()
            self.mm_hists[i].Write()
            self.qq_hists[i].Write()
        return


    def readhistf (self, f, chan):
        return [self.readhist(f, chan), self.readhist(f, chan, True)]

    def readhist (self, f, chan, fine=False):
        ff = 'fine' if fine else ''
        var = 'visrecoil0_m' if chan.endswith('0') else 'visrecoil_m'
        var = var + ff
        n = getattr (f, f'prefilt_{var}').GetEntries()
        h = getattr (f, f'{chan}_{var}')
        scale = intlum * self.xsbr / n
        hnew = h.Clone (f'{chan}_{self.hname}_recoil' + ff)
        hnew.SetTitle (hnew.GetName())
        hnew.Sumw2 (True)
        hnew.Scale (scale)
        return hnew

# Ratio seen between fastsim (pythia8) and whizard.

zz_adjust = 1.44
samps = [
    Samp ('hinv-ee',   7.16*fb,  1.11e-3 ),
    Samp ('hinv-mm',   6.78*fb,  1.09e-3 ),
    Samp ('hinv-qq',  136.3*fb,  1.09e-3 ),
    Samp ('zh_nneenn', 46.1*fb,  2.68e-3 ),
    Samp ('zh_nnmmnn', 46.2*fb,  2.68e-3 ),
    Samp ('zh_nnqqnn', 46.0*fb,  6.88e-3 ),
    Samp ('zh_nnqq',   46.0*fb,  0.603 ),
    Samp ('zz_eenn',   11.1*fb * zz_adjust ),
    Samp ('zz_mmnn',   11.1*fb * zz_adjust  ),
    Samp ('zz_qqnn',  224.2*fb * zz_adjust  ),
    Samp ('zz_eeqq',   37.9*fb * zz_adjust  ),
    Samp ('zz_mmqq',   37.9*fb * zz_adjust  ),
    Samp ('zz_qqqq',  358.4*fb * zz_adjust  ),
    Samp ('wzp6_ee_WW_lvqq_ecm240',  604*fb ),
    Samp ('wzp6_ee_WW_munumunu_ecm240',  201*fb ),
    Samp ('wzp6_ee_WW_enuenu_ecm240',  201*fb ),

    Samp ('wzp6_ee_WW_munumunu_ecm240_p6decay',   16.4385*pb, 0.1063**2 ),
    Samp ('wzp6_ee_WW_enuenu_ecm240_p6decay',     16.4385*pb, 0.1071**2 ),
    Samp ('wzp6_ee_WW_taunutaunu_ecm240_p6decay', 16.4385*pb, 0.1138**2 ),
    Samp ('wzp6_ee_WW_lvqq_ecm240_p6decay',       16.4385*pb, 0.441 ),

    Samp ('wzp6_ee_tautau_ecm240', 4.668*pb ),
]


class Process:
    def __init__ (self, name, *samples):
        self.name = name
        self.samps = [self.getsamp (s) for s in samples]
        return

    def getsamp (self, name):
        for s in samps:
            if s.name == name: return s
        assert 0, name


    def mergehists (self):
        self.ee0_hists = self.mergehistf ('ee0')
        self.ee_hists = self.mergehistf ('ee')
        self.mm0_hists = self.mergehistf ('mm0')
        self.mm_hists = self.mergehistf ('mm')
        self.qq_hists = self.mergehistf ('qq')
        return


    def writehists (self):
        for i in range(2):
            self.ee0_hists[i].Write()
            self.ee_hists[i].Write()
            self.mm0_hists[i].Write()
            self.mm_hists[i].Write()
            self.qq_hists[i].Write()
        return


    def mergehistf (self, chan):
        return [self.mergehist(chan), self.mergehist(chan, True)]

    def mergehist (self, chan, fine = False):
        ifine = 1 if fine else 0
        ff = 'fine' if fine else ''
        h = getattr (self.samps[0], f'{chan}_hists')[ifine].Clone (f'{chan}_{self.name}_recoil{ff}')
        h.SetTitle (h.GetName())
        for s in self.samps[1:]:
            h.Add (getattr (s, f'{chan}_hists')[ifine])
        return h


procs = [
    Process ('signal', 'hinv-ee', 'hinv-mm', 'hinv-qq'),
    Process ('zh', 'zh_nneenn', 'zh_nnmmnn', 'zh_nnqqnn' ,'zh_nnqq'),
    Process ('zz', 'zz_eenn', 'zz_mmnn', 'zz_qqnn',
                   'zz_eeqq', 'zz_mmqq', 'zz_qqqq',),
    Process ('ww_whizard', 'wzp6_ee_WW_lvqq_ecm240',
                           'wzp6_ee_WW_munumunu_ecm240',
                           'wzp6_ee_WW_enuenu_ecm240',),
    Process ('ww', 'wzp6_ee_WW_lvqq_ecm240_p6decay',
                   'wzp6_ee_WW_enuenu_ecm240_p6decay',
                   'wzp6_ee_WW_munumunu_ecm240_p6decay',
                   'wzp6_ee_WW_taunutaunu_ecm240_p6decay',),
    Process ('wwll', 'wzp6_ee_WW_enuenu_ecm240_p6decay',
                     'wzp6_ee_WW_munumunu_ecm240_p6decay',
                     'wzp6_ee_WW_taunutaunu_ecm240_p6decay',),
    Process ('ztautau', 'wzp6_ee_tautau_ecm240'),
]


def makehists():
    for s in samps:
        s.readhists()
    for p in procs:
        p.mergehists()
    fout = ROOT.TFile ('hinv-norm.root', 'RECREATE')
    for s in samps:
        s.writehists()
    for p in procs:
        p.writehists()
    fout.Close()
    return
makehists()
    

from printpdf import gettime
thistime = gettime()
def movepdf (nameroot):
    if os.path.exists (f'{nameroot}.pdf'):
        os.rename (f'{nameroot}.pdf', f'{thistime}-{nameroot}.pdf')
    return

for s in samps:
    movepdf (s.name)

