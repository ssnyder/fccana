#include "FCCAnalyses/ReconstructedParticle.h"
#include "FCCAnalyses/defines.h"
#include "FCCAnalyses/MCParticle.h"
#include "edm4hep/TrackData.h"
#include "edm4hep/TrackState.h"
#include "podio/ObjectID.h"
// /cvmfs/sw.hsf.org/spackages7/fccanalyses/master/x86_64-centos7-gcc11.2.0-opt/illut/include/FCCAnalyses

using FCCAnalyses::Vec_rp;
using FCCAnalyses::Vec_mc;
using FCCAnalyses::Vec_f;
using Vec_cl = ROOT::VecOps::RVec<edm4hep::ClusterData>;
using Vec_tr = ROOT::VecOps::RVec<edm4hep::TrackData>;
using Vec_ts = ROOT::VecOps::RVec<edm4hep::TrackState>;
using Vec_oid = ROOT::VecOps::RVec<podio::ObjectID>;

template <class T>
TLorentzVector tlv (const T& p)
{
  TLorentzVector v;
  v.SetXYZM(p.momentum.x, p.momentum.y, p.momentum.z, p.mass);
  return v;
}



struct sel_absType {
  int m_id;
  sel_absType (int id) : m_id(id) {}
  ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData> operator() (ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData> in)
  {
    ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData> result;
    result.reserve(in.size());
    for (size_t i = 0; i < in.size(); ++i) {
      auto & p = in[i];
      if (std::abs(p.type) == m_id) {
        result.emplace_back(p);
      }
    }
    return result;
  }
};


std::vector<float> get_rp_clustE (const Vec_rp& rps, const Vec_cl& clusts)
{
  std::vector<float> out;
  out.reserve (rps.size());
  for (const auto& rp : rps) {
    float clustE = 0;
    if (rp.clusters_begin != rp.clusters_end) {
      clustE = clusts.at(rp.clusters_begin).energy;
    }
    out.push_back (clustE);
  }
  return out;
}
                                  

std::vector<float> get_rp_clustEerror (const Vec_rp& rps, const Vec_cl& clusts)
{
  std::vector<float> out;
  out.reserve (rps.size());
  for (const auto& rp : rps) {
    float clustE = 0;
    if (rp.clusters_begin != rp.clusters_end) {
      clustE = clusts.at(rp.clusters_begin).energyError;
    }
    out.push_back (clustE);
  }
  return out;
}
                                  

std::vector<float> get_fastEerror (const std::vector<float>& Evec)
{
  auto errfunc = [](float E) {
    return sqrt(E*E*0.005*0.005 + 0.002*0.002 + E*0.03*0.03);
  };
  std::vector<float> out;
  std::transform (Evec.begin(), Evec.end(), std::back_inserter(out), errfunc);
  return out;
}
                                  

float get_totE (const Vec_rp& rps)
{
  float out = 0;
  for (const auto& rp : rps) {
    TLorentzVector v = tlv (rp);
    out += v.E();
  }
  return out;
}
                                  

struct MatchRel
{
  MatchRel (int mc, int rp) : mcndx(mc), rpndx(rp) {}
  int mcndx;
  int rpndx;
};


int matchOne (const edm4hep::MCParticleData& mc,
              const ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData>& rps)
{
  int matched = -1;
  float mindr = 0.1;
  TLorentzVector mc_tlv;
  mc_tlv.SetXYZM (mc.momentum.x, mc.momentum.y, mc.momentum.z, mc.mass);
  for (size_t i  = 0; i < rps.size(); i++) {
    const auto& rp = rps[i];
    TLorentzVector rp_tlv;
    rp_tlv.SetXYZM (rp.momentum.x, rp.momentum.y, rp.momentum.z, rp.mass);
    float dr = mc_tlv.DeltaR (rp_tlv);
    if (dr < mindr) {
      mindr = dr;
      matched = i;
    }
  }
  return matched;
}


std::vector<MatchRel>
match (const ROOT::VecOps::RVec<edm4hep::MCParticleData>& mcs,
       const ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData>& rps)
{
  std::vector<MatchRel> ret;
  ret.reserve (mcs.size());
  for (size_t i = 0; i < mcs.size(); i++) {
    const auto& mc = mcs[i];
    int matched = matchOne (mc, rps);
    if (matched >= 0)
      ret.emplace_back (i, matched);
  }
  return ret;
}


std::vector<int> get_matchrel_mcndx (const std::vector<MatchRel>& mv)
{
  std::vector<int> out;
  out.reserve (mv.size());
  for (const MatchRel& m : mv) {
    out.push_back (m.mcndx);
  }
  return out;
}


std::vector<int> get_matchrel_rpndx (const std::vector<MatchRel>& mv)
{
  std::vector<int> out;
  out.reserve (mv.size());
  for (const MatchRel& m : mv) {
    out.push_back (m.rpndx);
  }
  return out;
}



ROOT::VecOps::RVec<edm4hep::MCParticleData> 
sel_matched (const ROOT::VecOps::RVec<edm4hep::MCParticleData>& mcs,
             const std::vector<MatchRel>& matchrel)
{
  ROOT::VecOps::RVec<edm4hep::MCParticleData> out;
  for (const MatchRel& r : matchrel) {
    out.push_back (mcs[r.mcndx]);
  }
  return out;
}


ROOT::VecOps::RVec<edm4hep::MCParticleData> 
sel_unmatched (const ROOT::VecOps::RVec<edm4hep::MCParticleData>& mcs,
             const std::vector<MatchRel>& matchrel)
{
  ROOT::VecOps::RVec<edm4hep::MCParticleData> out;
  for (size_t i = 0; i < mcs.size(); i++) {
    bool found = false;
    for (const MatchRel& r : matchrel) {
      if (i == r.mcndx) {
        found = true;
        break;
      }
    }
    if (!found) {
      out.push_back (mcs[i]);
    }
  }
  return out;
}


ROOT::VecOps::RVec<float> 
calc_diff (const std::vector<MatchRel>& matchrel,
          const ROOT::VecOps::RVec<edm4hep::MCParticleData>& mcs,
          const ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData>& rps)
{
  ROOT::VecOps::RVec<float> out;
  for (const MatchRel& r : matchrel) {
    float rppt = hypot (rps[r.rpndx].momentum.x, rps[r.rpndx].momentum.y);
    float mcpt = hypot (mcs[r.mcndx].momentum.x, mcs[r.mcndx].momentum.y);
    if (mcpt > 0) {
      out.push_back (rppt - mcpt);
    }
  }
  return out;
}


ROOT::VecOps::RVec<float> 
calc_res (const std::vector<MatchRel>& matchrel,
          const ROOT::VecOps::RVec<edm4hep::MCParticleData>& mcs,
          const ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData>& rps)
{
  ROOT::VecOps::RVec<float> out;
  for (const MatchRel& r : matchrel) {
    float rppt = hypot (rps[r.rpndx].momentum.x, rps[r.rpndx].momentum.y);
    float mcpt = hypot (mcs[r.mcndx].momentum.x, mcs[r.mcndx].momentum.y);
    if (mcpt > 0) {
      out.push_back ((rppt - mcpt) / mcpt);
    }
  }
  return out;
}


std::vector<float> 
calc_clustRes (const std::vector<MatchRel>& matchrel,
               const Vec_mc& mcs,
               const std::vector<float>& clustE)
{
  std::vector<float> out;
  for (const MatchRel& r : matchrel) {
    TLorentzVector vmc = tlv (mcs.at(r.mcndx));
    float mcE = vmc.E();
    float rpE = clustE.at(r.rpndx);
    if (mcE > 0) {
      out.push_back ((rpE - mcE) / mcE);
    }
  }
  return out;
}


ROOT::VecOps::RVec<float> 
calc_ires (const std::vector<MatchRel>& matchrel,
           const ROOT::VecOps::RVec<edm4hep::MCParticleData>& mcs,
           const ROOT::VecOps::RVec<edm4hep::ReconstructedParticleData>& rps)
{
  ROOT::VecOps::RVec<float> out;
  for (const MatchRel& r : matchrel) {
    float rppt = hypot (rps[r.rpndx].momentum.x, rps[r.rpndx].momentum.y);
    float mcpt = hypot (mcs[r.mcndx].momentum.x, mcs[r.mcndx].momentum.y);
    if (mcpt > 0 && rppt > 0) {
      out.push_back ((1/rppt - 1/mcpt) * mcpt);
    }
  }
  return out;
}




struct resonanceBuilder {
  float m_resonance_mass;
  resonanceBuilder(float arg_resonance_mass);
  template <class PART>
  ROOT::VecOps::RVec<PART> operator()(const ROOT::VecOps::RVec<PART>& legs);
};


resonanceBuilder::resonanceBuilder(float arg_resonance_mass) {m_resonance_mass = arg_resonance_mass;}

template <class PART>
ROOT::VecOps::RVec<PART> resonanceBuilder::operator()(const ROOT::VecOps::RVec<PART>& legs) {
  ROOT::VecOps::RVec<PART> result;
  int n = legs.size();
  if (n >1) {
    ROOT::VecOps::RVec<bool> v(n);
    std::fill(v.end() - 2, v.end(), true);
    do {
      PART reso;
      TLorentzVector reso_lv;
      for (int i = 0; i < n; ++i) {
          if (v[i]) {
            reso.charge += legs[i].charge;
            TLorentzVector leg_lv;
            leg_lv.SetXYZM(legs[i].momentum.x, legs[i].momentum.y, legs[i].momentum.z, legs[i].mass);
            reso_lv += leg_lv;
          }
      }
      reso.momentum.x = reso_lv.Px();
      reso.momentum.y = reso_lv.Py();
      reso.momentum.z = reso_lv.Pz();
      reso.mass = reso_lv.M();
      result.emplace_back(reso);
    } while (std::next_permutation(v.begin(), v.end()));
  }
  if (result.size() > 1) {
    auto resonancesort = [&] (PART i ,PART j) { return (abs( m_resonance_mass -i.mass)<abs(m_resonance_mass-j.mass)); };
    std::sort(result.begin(), result.end(), resonancesort);
    auto first = result.begin();
    auto last = result.begin() + 1;
    ROOT::VecOps::RVec<PART> onlyBestReso(first, last);
    return onlyBestReso;
  } else {
    return result;
  }
}



/////////////////////////////////////////////////////////////////////////////
// Track information.


std::vector<int> get_rp_tsindex (const Vec_rp& rps,
                                 const Vec_tr& tracks,
                                 const Vec_oid& trackIdx)
{
  std::vector<int> out;
  out.reserve (rps.size());
  for (const auto& rp : rps) {
    int ts_index = -1;
    if (rp.tracks_begin != rp.tracks_end) {
      int ndx = trackIdx.at(rp.tracks_begin).index;
      const edm4hep::TrackData& track = tracks.at(ndx);
      if (track.trackStates_begin != track.trackStates_end) {
        ts_index = track.trackStates_begin;
      }
    }
    out.push_back (ts_index);
  }
  return out;
}


std::vector<TLorentzVector> get_trackmom (const std::vector<int>& tsindexvec,
                                          const Vec_ts& trackStates)
{
  std::vector<TLorentzVector> out;
  out.reserve (tsindexvec.size());
  for (int tsindex : tsindexvec) {
    TLorentzVector v;
    if (tsindex >= 0) {
      const edm4hep::TrackState& ts = trackStates.at(tsindex);
      const float B = 2; // magnetic field
      const float C = -500 * ts.omega;
      const float pt = B * 0.2998 / abs(2*C);
      const float pz = pt * ts.tanLambda;
      const float E = hypot (pt, pz);
      v.SetXYZM (pt*cos(ts.phi), pt*sin(ts.phi), pz, 0);
    }
    out.push_back (v);
  }
  return out;
}


std::vector<float> get_trackSigP (const std::vector<int>& tsindexvec,
                                  const std::vector<TLorentzVector>& pvec,
                                  const Vec_ts& trackStates)
{
  std::vector<float> out;
  out.reserve (tsindexvec.size());
  for (size_t i = 0; i < tsindexvec.size(); ++i) {
    float sigP = 0;
    int tsindex = tsindexvec.at(i);
    if (tsindex >= 0) {
      const TLorentzVector v = pvec.at(i);
      const float mom = v.P();
      const edm4hep::TrackState& ts = trackStates.at(tsindex);
      const float sigOmega = sqrt(ts.covMatrix[5]);
      sigP = mom * sigOmega / ts.omega;
    }
    out.push_back (sigP);
  }
  return out;
}


std::vector<float> get_tlv_p (const std::vector<TLorentzVector>& vv)
{
  std::vector<float> out;
  out.reserve (vv.size());
  for (const TLorentzVector& v : vv) {
    out.push_back (v.P());
  }
  return out;
}


std::vector<float> get_tlv_pt (const std::vector<TLorentzVector>& vv)
{
  std::vector<float> out;
  out.reserve (vv.size());
  for (const TLorentzVector& v : vv) {
    out.push_back (v.Pt());
  }
  return out;
}


std::vector<float> get_tlv_theta (const std::vector<TLorentzVector>& vv)
{
  std::vector<float> out;
  out.reserve (vv.size());
  for (const TLorentzVector& v : vv) {
    out.push_back (v.Theta());
  }
  return out;
}


std::vector<float> get_tlv_phi (const std::vector<TLorentzVector>& vv)
{
  std::vector<float> out;
  out.reserve (vv.size());
  for (const TLorentzVector& v : vv) {
    out.push_back (v.Phi());
  }
  return out;
}


std::vector<float> get_tlv_eta (const std::vector<TLorentzVector>& vv)
{
  std::vector<float> out;
  out.reserve (vv.size());
  for (const TLorentzVector& v : vv) {
    out.push_back (v.Eta());
  }
  return out;
}


TLorentzVector sum (const Vec_rp& v)
{
  TLorentzVector ret;
  for (const auto& p : v) {
    ret += tlv(p);
  }
  return ret;
}


TLorentzVector sum_cut (const Vec_rp& v, float cut)
{
  TLorentzVector ret;
  for (const auto& p : v) {
    auto v = tlv(p);
    if (v.P() > cut) {
      ret += v;
    }
  }
  return ret;
}
